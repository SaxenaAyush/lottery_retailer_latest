import { TestBed } from '@angular/core/testing';

import { LotteryHttpService } from './lottery-http.service';

describe('LotteryHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LotteryHttpService = TestBed.get(LotteryHttpService);
    expect(service).toBeTruthy();
  });
});
