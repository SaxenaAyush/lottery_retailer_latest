import 'reflect-metadata';
import '../polyfills';
import { Component } from '@angular/core';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalComponent } from './modal.component';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ElectronService } from './providers/electron.service';
import { WebviewDirective } from './directives/webview.directive';
import { AppComponent } from './app.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { WalletDetailsComponent } from './components/client/wallet-details/wallet-details.component';
import { WalletModalComponent } from './components/client/wallet-details/wallet-modal.component';
import { SharedModule } from './common/sharaed/sharaed.module';
import { HttpModule } from '@angular/http';
import { AddDetailsComponent } from './components/client/add-details/add-details.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { ResultViewComponent } from './components/client/result-view/result-view.component';
import { UserProfileComponent } from './components/client/user-profile/user-profile.component';
import { SettingComponent } from './components/client/setting/setting.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QRCodeModule } from 'angularx-qrcode';
import { DatePipe } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ResultReportComponent } from './components/client/result-report/result-report.component';
import { MatIconModule } from '@angular/material';
import { CollapsingComponent } from './components/collapsing/collapsing.component';
import { AsideComponent } from './components/aside/aside.component';
import { AboutUsComponent } from './components/aboutUs/aboutUs.component';
import { CertificateComponent } from './components/certificate/certificate.component';
import { TermsComponent } from './components/login/terms-modal.component';
import { from } from 'rxjs';
const { remote } = require('electron');
import { BookingModalComponent } from './components/client/booking/booking-modalcomponent';
import { HistoryModalComponent } from './components/client/claim-transaction/history-modal.component';
import { TicketpreviewComponent } from './ticketpreview/ticketpreview.component';
import { ModalTicketComponent } from './components/client/booking/booking-ticketcomponents';
import { NgxBarcodeModule } from 'ngx-barcode';
import { ShowErrorsComponent } from './validator/show-error';
import { ReprintticketComponent } from './reprintticket/reprintticket.component';
import {NgxChildProcessModule} from 'ngx-childprocess';
import {NgxFsModule} from 'ngx-fs';
import { PrintresultComponent } from './printresult/printresult.component';
import {HotkeyModule} from 'angular2-hotkeys';
import { TicketHistoryComponent } from './components/client/ticket-history/ticket-history.component';
import { DailyReportComponent } from './components/client/daily-report/daily-report.component';
import { ReportPrintComponent } from './report-print/report-print.component';
import { CalimReprintticketComponent } from './claim-reprint/claim-reprintticket.component';
import { ClaimTransactionComponent } from './components/client/claim-transaction/claim-transaction.component';
import { BookingComponent } from './components/client/booking/booking.component';
import { ClientWalletHistoryComponent } from './components/client/client-wallet-history/client-wallet-history.component';
import { ClientWalletDetailsHistoryComponent } from './components/client/client-wallet-details-history/client-wallet-details-history.component';
import { ClientVsBookingComponent } from './components/client/client-vs-booking/client-vs-booking.component';
import { ClaimVsBookingDetailsComponent } from './components/client/claim-vs-booking-details/claim-vs-booking-details.component';
import { PrintClaimVsBookingComponent } from './print-claim-vs-booking/print-claim-vs-booking.component';
import { FirstClaimBookingPrintComponent } from './first-claim-booking-print/first-claim-booking-print.component';
import { DurationReportPrintComponent } from './duration-report-print/duration-report-print.component';
// import { PdfmakeModule } from 'ng-pdf-make';

// import { CountdownModule } from 'ngx-countdown';
// import { CountdownModule } from 'ngx-countdown';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
// function countdownConfigFactory(): CountdownGlobalConfig {
//   return { format: `mm:ss` };
// }


@NgModule({

  declarations: [
    AppComponent,
    ShowErrorsComponent,
    AsideComponent,   
    WebviewDirective,
    ModalComponent,
    HistoryModalComponent,
    WalletModalComponent,
    BookingModalComponent,
    TermsComponent,
    SideNavComponent,
    BookingComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    WalletDetailsComponent,
    ClaimTransactionComponent,
    AddDetailsComponent,
    ResultViewComponent,
    ResultReportComponent,
    UserProfileComponent,
    SettingComponent,
    HeaderComponent,
    CollapsingComponent,
    AboutUsComponent,
    CertificateComponent,
    TicketpreviewComponent,
    ModalTicketComponent,
    ShowErrorsComponent,
    ReprintticketComponent,
    PrintresultComponent,
    TicketHistoryComponent,
    DailyReportComponent,
    ReportPrintComponent,
    CalimReprintticketComponent,

    ClientWalletHistoryComponent,
    ClientWalletDetailsHistoryComponent,
    ClientVsBookingComponent,
    ClaimVsBookingDetailsComponent,
    PrintClaimVsBookingComponent,
    FirstClaimBookingPrintComponent,
    DurationReportPrintComponent
  ],
  imports: [
    SharedModule.forRoot(),
    BrowserModule,
    // PdfmakeModule,
    FormsModule,    
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    NgxSelectModule,
    NgxQRCodeModule,
    QRCodeModule,
    NgxBarcodeModule,
    NgxChildProcessModule,
    NgxFsModule,
    HotkeyModule.forRoot(),
    NgxDaterangepickerMd.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],


  providers: [ElectronService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}