import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketpreviewComponent } from './ticketpreview.component';

describe('TicketpreviewComponent', () => {
  let component: TicketpreviewComponent;
  let fixture: ComponentFixture<TicketpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
