import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
const jsonfile = require('jsonfile');
var jarfile = require("jarfile");
import ptp from "pdf-to-printer";
@Component({
  selector: 'app-ticket-history',
  templateUrl: './ticket-history.component.html',
  styleUrls: ['./ticket-history.component.scss']
})
export class TicketHistoryComponent implements OnInit {
  scannerValue: any;
  searchText: any;
  searchText2: any;
  searchText3: any;
  searchText4: any;
  searchText5: any;
  // today: number = Date.now();
  isDisabled = false;
  tempList: any = [];
  hideClaim = false;
  mprice: any;
  homeClass = "active";
  transactionClass = "inactive";
  dispatchClass = "inactive";
  homeActive = "btn-btn-primary";
  @ViewChild('modal5') modal5: any;
  @ViewChild('modal1') modal1: any;
  @ViewChild('modal4') modal4: any;
  code: any;
  historyList: any[] = [];
  ticketHistoryList: any = [];
  status: String;
  historyListByDate: any;
  page = 1;
  pageSize = 8;
  page2 = 1;
  pageSize2 = 8;
  requestedId = this.storageService.get('currentUser').user.roleMaster.id
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  property = false;
  roleId = this.userD.user.roleMaster.id;
  filteredhistoryList: any[]=[];
  historyListPrice:any [] =[];
  ticketDeatils: any;
  number: any;
  gher: any;
  amount: any;
  totalprice: any;
  bookingdate: any;
  slottime: any;
  slots: any;
  previewModal: any = [];
  TicketpreviewModal:any=[];
  PreviewticketDeatils:any;
  ticketBookingDate:any;
  ticketSlotTime:any;
  ticketTotalPrice:any;
  totaltickets: any = [];
  ticketcount: any = [];
  bac: any;
  backup: any = [];
  navModel: any = {
    "ticketHistory": 2,
    "canceledTicket": 1,
   
  }
  navbar(input: any) {
    //console.log(input);
    if (input == 1) {
      this.navModel.ticketHistory = 2;
      this.navModel.canceledTicket = 1;
   
    }
    else if (input == 2) {
      this.navModel.ticketHistory = 1;
      this.navModel.canceledTicket = 2;
    }

  }  constructor( private datePipe: DatePipe, private sweetAlert: SweetAlertService, private router: Router, private storageService: LotteryStorageService, private lotteryHttpService: LotteryHttpService, private alertService: SweetAlertService, ) {
  }




  
  ngOnInit() {
    this.openHistoryForm();
    setInterval(() => {
      this.checktime(); 
      }, 1000);
  }

  flag=0;
 match : any;
  checktime(){
    this.today;
    let h :any;
    let m :any;
    let s:any;
    var d = new Date();
   h = d.getHours();
   m = d.getMinutes()
   s= d.getSeconds();
  let min: string;
  if(m<10){
    min= "0"+m;
  }else{
min =m
  }
   this.match= h+":"+min;
let time="21:30";

if(this.match>=time ){
  this.flag=1;
this.isDisabled=true;

}

    }

  today = Date();
  TotalAmt:any;
  historyListDate:any;
  tickethistory = [];
  openHistoryForm() {
    $('.history-Form').show();
    $('.CanceledTicket-Form').hide();
     let reqMap = {
      bookingDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
    
        id: this.storageService.get('currentUser').user.id
 
    }
   // console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'ticketHistoryByDate', reqMap).subscribe((res) => {
      this.historyList = res.content.BookingDetail;
      this.historyListDate =this.datePipe.transform(this.today, "dd-MM-yyyy"),
      this.TotalAmt = res.content.TotalAmt;

     // this.filteredhistoryList = this.historyList;
     this.tickethistory=[];
    for(let his of this.historyList){
      this.tickethistory.push({
        "id": his.id,
        "retailerId": his.retailerId,
        "totalTicket":his.totalTicket,
        "createDate":his.createDate,
        "ticketId":his.ticketId,
        "slot":his.slot,
        "totalPrice":his.totalPrice,
        "status":his.status,
        "cancalable": his.cancalable,
        "cancel" : 1
      })
    }
    this.filteredhistoryList=this.tickethistory;
 // console.log('tickethistory',this.tickethistory);
  //  console.log('res.content.BookingDetail',res.content.BookingDetail);

     // console.log('Price dhklo',this.filteredhistoryList);

    });
  }
  openHistoryByDate(){
    let reqMap = {
      bookingDate :this.datePipe.transform(this.historyListByDate, "dd-MM-yyyy") ,

  
        id: this.storageService.get('currentUser').user.id,
      
    }
  //  console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'ticketHistoryByDate', reqMap).subscribe((res) => {
      this.historyList = res.content.BookingDetail;
      this.TotalAmt = res.content.TotalAmt;
      this.historyListDate = this.datePipe.transform(this.historyListByDate, "dd-MM-yyyy") ;

      this.filteredhistoryList = this.historyList;
     // console.log('Price dhklo',this.historyList);

    });
  
  }
  CancelhistoryList :any[]=[];
  filteredCancelhistoryList:any[]=[];
  CancelTotalAmt:any;
  cancelHistoryListByDate:any;
  CancelhistoryListDate:any;
 openCanceledTicketForm(){
  $('.CanceledTicket-Form').show();
  $('.history-Form').hide();
  let reqMap = {
    bookingDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
  
    retailerId: this.storageService.get('currentUser').user.id

  }
 // console.log(reqMap);
  this.lotteryHttpService.makeRequestApi('post', 'cancelHistoryByDate', reqMap).subscribe((res) => {
    this.CancelhistoryList = res.content.Tickets;
    this.CancelTotalAmt = res.content.TotalAmount;
    this.CancelhistoryListDate = this.datePipe.transform(this.today, "dd-MM-yyyy"),
    this.filteredCancelhistoryList = this.CancelhistoryList;
    // console.log('Price dhklo',this.filteredCancelhistoryList);

  });
 }
 openCanceledHistoryByDate(){
  let reqMap = {
    bookingDate: this.datePipe.transform(this.cancelHistoryListByDate, "dd-MM-yyyy"),
  
    retailerId: this.storageService.get('currentUser').user.id

  }
//  console.log(reqMap);
  this.lotteryHttpService.makeRequestApi('post', 'cancelHistoryByDate', reqMap).subscribe((res) => {
    this.CancelhistoryList = res.content.Tickets;
    this.CancelTotalAmt = res.content.TotalAmount;
    this.CancelhistoryListDate =this.datePipe.transform(this.cancelHistoryListByDate, "dd-MM-yyyy");

    this.filteredCancelhistoryList = this.CancelhistoryList;
    // console.log('Price dhklo',this.filteredCancelhistoryList);

  });
 }
  
 
 

  cancleTicket(emp) {
   // console.log('emp', emp)
    Swal.fire({
      html: '<h3 class="mt-5 mb-4 mx-2">Are you sure you want to Cancle this Ticket?</h3>',
      animation: true,
      width: 548,
      padding: '-1.9em',
      showCancelButton: true,
      focusConfirm: false,
      cancelButtonText: 'No',
      cancelButtonColor: '#17A2B8',
      cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
      confirmButtonColor: '#DD6B55',
      confirmButtonClass: 'btn btn-danger btn-pill px-4',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.isDisabled=true;
        this.lotteryHttpService.makeRequestApi('post', 'ticketCancle', {
          user: {
            id: this.storageService.get('currentUser').user.id,
          },
          bookingId: emp
        }).subscribe(res => {
          if (res.code == 206) {
            this.sweetAlert.swalSuccess('Ticket Cancled');
for(let can of this.filteredhistoryList){
  if(emp==can.id){
    can.cancel=2
  }
}
            this.isDisabled=false;
          }
          else {
            this.sweetAlert.swalError(res.message);
            this.isDisabled=false;
          }
          if (!res.isError) {
            this.openHistoryForm();
          }
          this.code = res.code;
        });
      //  this.isDisabled=false;
      }
    });
   // this.isDisabled=false;
  }


 
  searchDataActivate: any[] = [];
  onSearchFilterActivate(searchData) {
      if (searchData) {
        this.filteredhistoryList = this.historyList.filter(item => {
          const filter = Object.keys(item);
          return filter.some(
            key => {  
              if ((key === 'ticketId' || key === 'createDate' ) && item[key] != null) {
                return item[key].toLowerCase().trim().indexOf(searchData.toLowerCase().trim()) !== -1;
              }
              else if ((key === 'totalPrice' || key === 'totalTicket' ) && item[key] != null) {
                return item[key].toString().trim().indexOf(searchData.toString().trim()) !== -1;
              }
            }
          );
        });
      } else {
        this.filteredhistoryList = this.historyList;
      }
    }

    previewticketModal(userId) {
      let reqMap = {
        bookingDetail: {
          id: userId,
        }
      }
  
     // console.log(reqMap);
      this.lotteryHttpService.makeRequestApi('post', 'bookingId', reqMap).subscribe((res) => {
        if (res.code == 206) {
          console.log(res, 'res');
          this.PreviewticketDeatils = res.content[0];
          this.ticketBookingDate = this.PreviewticketDeatils.bookingDetails.bookingDate;
          this.ticketSlotTime = this.PreviewticketDeatils.bookingDetails.slot.drawTime;
          this.ticketTotalPrice = this.PreviewticketDeatils.bookingDetails.totalPrice;
     //     console.log('PreviewticketDeatils', this.PreviewticketDeatils);    
  
         }      
         for (let demo of this.PreviewticketDeatils) {
        
          this.TicketpreviewModal.push({
            "gherno": demo.gherno,
            "ticketNo": demo.bookingDetail.totalTicket,
            "totalticketAmt": demo.bookingDetail.totalPrice,
            "numbersSelected": demo.numbersSelected,
  
          })
          
  
        }
        for (let bill of this.PreviewticketDeatils) {
          
          this.ticketcount.push({
            "id": bill.bookingDetails.id,
            "ticketId": parseInt(bill.bookingDetails.ticketId)
          });
    
          this.totaltickets.push({
            "id": bill.bookingDetails.id,
            "retailerId": bill.bookingDetails.retailerId,
            "bookingDate": bill.bookingDetails.bookingDate,
            "createDate": bill.bookingDetails.createDate,
            "totalPrice": bill.bookingDetails.totalPrice,
            "slot": bill.bookingDetails.slot.drawTimeAm,
            "totalTicket": bill.bookingDetails.totalTicket,
            "ticketNumbers": bill.ticketNumbers,
            "ticketId": parseInt(bill.bookingDetails.ticketId)
          })
          //   }
          // }
    
        }
  
          this.modal5.show();
        
      });
    }
    ticketModal(userId) {
      let reqMap = {
        bookingDetail: {
          id: userId,
        }
      }
  
    //  console.log(reqMap);
      this.lotteryHttpService.makeRequestApi('post', 'bookingId', reqMap).subscribe((res) => {
        if (res.code == 206) {
          let ticket = res;
          this.ticketDeatils = res.content;
          this.ticketPrint();
          // console.log('ticketDeatils......', this.ticketDeatils);
          this.storageService.set('reprint', ticket);
        //  this.router.navigate(['/reprint-ticket']);
          this.previewModal = [];
         
          for (let demo of this.ticketDeatils) {
         
            this.previewModal.push({
              "gherno": demo.gherno,
              "ticketNo": demo.ticketNo,
              "totalticketAmt": demo.totalticketAmt,
              "numbersSelected": demo.numbersSelected,
  
            })
            // for(let i of this.previewModal.numbersSelected){
  
            //}
  
          }
          for (let p of this.previewModal) {
       //     console.log('abc', p.numbersSelected);
            this.bac = p.numbersSelected;
            //let a= this.splt(this.bac);
            //console.log(a);
          }
  
        }
  
            this.openHistoryForm();
  
  
     //   console.log('houseno', this.previewModal);
      });
    }

    ticketPrint(){
      const file = '/home/foo.json'
    const obj = { name: 'JP' }
     console.log(this.ticketDeatils,'this.allresult' );
    jsonfile.writeFile(file, this.ticketDeatils, function (err) {
      if (err) console.error(err)
    })
    
    setTimeout(() => {
      this.calljar();
    }, 3000);
    
    }
    calljar(){
      var exec = require('child_process').exec, child;
      child = exec('java -jar /home/result.jar',
      function (error, stdout, stderr){
      console.log('stdout: ' + stdout);
      console.log('stderr: ' + stderr);
      if(error !== null){
        console.log('exec error: ' + error);
      }
      });
      setTimeout(() => {
        this.printPdf();
      }, 2000);
      }
      printPdf(){
        ptp
        .print("/home/my_doc.pdf")
        .then(console.log)
        .catch(console.error);
      }
}
