import {Component, OnInit} from '@angular/core';
import {ViewChild} from '@angular/core';
import {ElectronService} from '../../../providers/electron.service';
import {TallyConnectorService} from '../../../providers/tallyconnector.service';
import {timingSafeEqual} from 'crypto';
import {Observable} from 'rxjs';
import {errorHandler} from '@angular/platform-browser/src/browser';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';
import {NgForm, FormBuilder} from '@angular/forms';
import {LotteryHttpService} from '../../../services/lottery-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LotteryStorageService} from '../../../services/lottery-storage.service';
import {SweetAlertService} from '../../../common/sharaed/sweetalert2.service';
import {FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import * as $ from '../../../../../node_modules/jquery';
import {element} from '@angular/core/src/render3';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';
import {version} from '../../../../../package.json';
import { DatePipe } from '@angular/common';
import { interval } from 'rxjs';
import { BrowserWindow } from 'electron';
import getMAC from 'getmac';
import {HotkeyModule, HotkeysService, Hotkey} from 'angular2-hotkeys';
import { ThrowStmt } from '@angular/compiler';
import * as arrowKeyNavigation from 'arrow-key-navigation'
import {setNavigableClassName} from "arrow-key-nav" 

import ptp from "pdf-to-printer";
import ArrowKeysReact from 'arrow-keys-react';

 //const React = require('react');
import { ArrowNavigation, useArrowNavigation } from 'react-arrow-navigation'
import { KeyboardNavModule } from 'angular-keyboard-nav';

// import { map } from 'rxjs/operators'
// import {SimpleTimer} from 'ng2-simple-timer';
// import { UUID } from 'angular2-uuid';
// import { NgxTimerModule } from 'ngx-timer';
// import { Observable } from 'rxjs/Observable'
// import 'rxjs/time/observable/timer'
// import 'rxjs/add/operator/map'
// import 'rxjs/add/operator/take'
// import { Pipe, PipeTransform } from '@angular/core';

// import { timer } from 'rxjs';
// import { Observable } from 'rxjs/Observable'
// import 'rxjs/add/observable/timer'
// import 'rxjs/add/operator/map'
// import 'rxjs/add/operator/take'
// import { Pipe, PipeTransform } from '@angular/core';

const jsonfile = require('jsonfile');
var jarfile = require("jarfile");
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent  implements OnInit {

  public loginForm: FormGroup;
  loading: boolean;
  @ViewChild('modal1') modal1: any;
  @ViewChild('modal2') modal2: any;
  @ViewChild('modal3') modal3: any;
  @ViewChild('modal4') modal4: any;
  @ViewChild('modal5') modal5: any;
  @ViewChild('modal6') modal6: any;
  @ViewChild('modal8') modal8: any;

  @ViewChild('webview') webview: any;

  apiUrl = "";
  flag = 0;
  isDisabled= true;
  profile: any = {
    "userId": 0
  };
//   url:any = 'http://env-0661550.mj.milesweb.cloud/wallet/';
  
//    balanceInfo:any ={
//     "id": 7,
//     "userName": "123",
//     "email": "1@email.com",
//     "userId": null,
//     "dateCreated": "2019-11-02",
//     "balance": 600
// };
  examDuration = 1000 * 180;
  leftSelectedNumber: any;
  searchModel: any
  gridApi: any;
  gridColumnApi: any;
  numbers: number[];
  inputarray: number[];
  today: number = Date.now();
  localStorage: any;
  // userDe:any;
  // userDe=JSON.stringify(this.localStorage.get('currentUser'));
  
//bookingslot=this.storageService.get('slotlist');
  //console.log('vvvvvvvvvvvvvvvvvvvvvv',this.bookingslot);

singleprice=  this.storageService.get('ticketPrice');
lastid=  this.storageService.get('lasticket');
  userD = this.storageService.get('currentUser');
previoustrans = this.storageService.get('lastamt');
// amtvalue = this.balance.content.amount;
amtvalue : any;
textValue:any;
  userName = this.userD.user.userName;
  retailerID = this.userD.user.id;
  phoneNumber = this.userD.user.phoneNumber;
  Number = "(" + this.phoneNumber + ")";
randomnumber: any;
  countupTimerService: any;
  appVersion:any =' '+ version;
  state: { content: string; };
  constructor(private _hotkeysService: HotkeysService, private httpClient: HttpClient, private electronService: ElectronService, private storageService: LotteryStorageService,
              private lotteryHttpService: LotteryHttpService, private fb: FormBuilder,
              private router: Router, private route: ActivatedRoute, private alertService: SweetAlertService, private datePipe:DatePipe,private http:HttpClient) {
              
                //  super(props);
                 
              
              //  setNavigableClassName("my-navigable-element");
                // if (/KAIOS/.test(navigator.userAgent)) {
                //   import('arrow-key-navigation').then(arrowKeyNavigation => {
                //     arrowKeyNavigation.register()
                //   })
                // }

                $(function () {
                  $("#allgher").focus();
                });     

                


    setInterval(() => {
      this.today = Date.now()
    }, 1);
    // setInterval(() => {
    //   this.refresh()
    // }, 900000);
    // this.userDetails=this.localStorage.getItem.currentUser;
    // console.log('dsvsdv',this.userDetails);
    // console.log(this.localStorage.getItem.currentUser);

    this._hotkeysService.add(new Hotkey('ctrl+p', (_event: KeyboardEvent): boolean => {
    //  console.log(_event.key);

      if(this.billModel==""){
        this.alertService.swalError("Please choose Application");
     //   console.log('billModel is empty');
      }else{
        this.saverepo();
      }
      return false;
    }));

    this._hotkeysService.add(new Hotkey('ctrl+h', (_event: KeyboardEvent): boolean => {
    //  console.log(_event.key);

      this.focusgher();
      return false;
    }));
    this._hotkeysService.add(new Hotkey('ctrl+f', (_event: KeyboardEvent): boolean => {
    //  console.log(_event.key);

      this.focusfront();
      return false;
    }));
    this._hotkeysService.add(new Hotkey('ctrl+b', (_event: KeyboardEvent): boolean => {
   //   console.log(_event.key);

      this.focusback();
      return false;
    }));
    this._hotkeysService.add(new Hotkey('ctrl+1', (_event: KeyboardEvent): boolean => {
    //  console.log(_event.key);

      this.focusgherrow();
      return false;
    }));

    this._hotkeysService.add(new Hotkey('ctrl+n', (_event: KeyboardEvent): boolean => {
  //    console.log(_event.key);
      this.focusNumber();
      
      return false;
    }));

    this._hotkeysService.add(new Hotkey('ctrl+d', (_event: KeyboardEvent): boolean => {
  //    console.log(_event.key);
     this.reset();
     this.alertService.swalSuccess("Reset is done");
      return false;
    }, undefined, 'Send a secret message to the console.'));

    this._hotkeysService.add(new Hotkey('ctrl+a', (_event: KeyboardEvent): boolean => {
    //  console.log(_event.key);
     this.togleamount();
     //this.alertService.swalSuccess("Reset is done");
      return false;
    }));


    this._hotkeysService.add(new Hotkey('ctrl+c', (_event: KeyboardEvent): boolean => {
  //    console.log(_event.key);
     this.cancel();
     //this.alertService.swalSuccess("Reset is done");
      return false;
    }));


    this._hotkeysService.add(new Hotkey('ctrl+t', (_event: KeyboardEvent): boolean => {
    //  console.log(_event.key);
     this.preview();
     //this.alertService.swalSuccess("Reset is done");
      return false;
    }));

    
   


    // this._hotkeysService.add(new Hotkey('ctrl+p, ctrl+s', (_event: KeyboardEvent): boolean => {
    
  
 // console.log('f1');
  //     return false; // Prevent bubbling
  // }));

  }

 t= 22500000;
  //countdown--------------

  focusgher(){
    this.focus=1;
    $(function () {
      $("#allgher").focus();
    });  

    if(this.focus==1){
      setNavigableClassName("my-navigable-element");
    }else if(this.focus==2){
      setNavigableClassName("all_gher_single_row");
     
    }else if(this.focus==3){
      setNavigableClassName(" single_number");
    }else if(this.focus==4){
      setNavigableClassName(" front_number_row");
    }else if(this.focus==5){
      setNavigableClassName(" back_number_row");
    }

  }
  focusfront(){
    this.focus=4;
    $(function () {
      $("#front_number_id").focus();
    });  

    if(this.focus==1){
      setNavigableClassName("my-navigable-element");
    }else if(this.focus==2){
      setNavigableClassName("all_gher_single_row");
     
    }else if(this.focus==3){
      setNavigableClassName("single_number");
    }else if(this.focus==4){
      setNavigableClassName("front_number_row");
    }else if(this.focus==5){
      setNavigableClassName("back_number_row");
    }

  }
  focusback(){
    this.focus=5;
    $(function () {
      $("#back_number").focus();
    });  

    if(this.focus==1){
      setNavigableClassName("my-navigable-element");
    }else if(this.focus==2){
      setNavigableClassName("all_gher_single_row");
     
    }else if(this.focus==3){
      setNavigableClassName(" single_number");
    }else if(this.focus==4){
      setNavigableClassName(" front_number_row");
    }else if(this.focus==5){
      setNavigableClassName(" back_number_row");
    }

  }
  focus=1;
  focusgherrow(){
    this.focus=2;
    $(function () {
      $("#btn5").focus();
     
    });  
    if(this.focus==1){
      setNavigableClassName("my-navigable-element");
    }else if(this.focus==2){
      setNavigableClassName("all_gher_single_row");
     // arrowKeyNavigation.register();
     
    }else if(this.focus==3){
      setNavigableClassName(" single_number");
    }else if(this.focus==4){
      setNavigableClassName(" front_number_row");
    }else if(this.focus==5){
      setNavigableClassName(" back_number_row");
    }
    
   
  }
  
  


  focusNumber(){
    this.focus=3;
    $(function () {
      $("#numb").focus();
    }); 
    
    if(this.focus==1){
      setNavigableClassName("my-navigable-element");
    }else if(this.focus==2){
      setNavigableClassName("all_gher_single_row");
     // arrowKeyNavigation.register();
     
    }else if(this.focus==3){
      setNavigableClassName(" single_number");
     // arrowKeyNavigation.register();
    }else if(this.focus==4){
      setNavigableClassName(" front_number_row");
    }else if(this.focus==5){
      setNavigableClassName(" back_number_row");
    }
  }
countdown(){
  // var deadline = new Date("Oct 30, 2019 15:37:25").getTime(); 
  // console.log('deadline',deadline);
  // var x = setInterval(function() { 
  // var now = new Date().getTime(); 
  // console.log('now',now);
  //var t = deadline - now; 
 // var t= 22500000-1000;
 // console.log('t---------', this.t);
  var days = Math.floor(this.t / (1000 * 60 * 60 * 24)); 
  var hours = Math.floor((this.t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
  var minutes = Math.floor((this.t % (1000 * 60 * 60)) / (1000 * 60)); 
//  console.log('mints', minutes);

  var seconds = Math.floor((this.t % (1000 * 60)) / 1000); 
//  console.log('second', seconds);
  document.getElementById("demo").innerHTML =  minutes + "m " + seconds + "s "; 
      if (this.t < 0) { 
         // clearInterval(x); 
          document.getElementById("demo").innerHTML = "EXPIRED";
          this.refresh(); 
      } 
    this.t= this.t-1000;
  // }, 1000); 
}
  
  //----------------------

//   constructor(private storageService: LotteryStorageService,
//     private lotteryHttpService: LotteryHttpService, private fb: FormBuilder,
//     private router: Router, private route: ActivatedRoute, private alertService: SweetAlertService) {
// }

  modelFirst: any = []
  singleModel: any = []
  siglModel: any = {
    "id": 0,
    "value": 0,
    "price": 1,
  }
  modelSecond: any = []

  changeModel: any = {
    "change": 0,
    "maxlength" :3,
  }
  max = 2;

  // bookingslot = [
  // ]

  buttonsList = [
    {
      "typeid": 1,
      "type": "even",
      "numbersSelected": [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": 5,
      "name": "E",
      "count" : 50,
    },
    {
      "typeid": 2,
      "type": "odd",
      "numbersSelected": [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": 7,
      "name": "O",
      "count" : 50,
    },
    {
      "typeid": 3,
      "type": "family",
      "numbersSelected": [],
      "status": "inactive",
      "ticketNo": 0,
      "totalticketAmt": 0,
      "familyNumber": '',
      "name": "F",
    },
    {
      "typeid": 4,
      "type": "right",
      "numbersSelected": [9, 18, 27, 36, 45, 54, 63, 72, 81, 90],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": '',
      "name": "R",
      "count" : 10,

    },
    {
      "typeid": 5,
      "type": "left",
      "numbersSelected": [0, 11, 22, 33, 44, 55, 66, 77, 88, 99],
      "status": "inactive",
      "ticketNo": '',
      "totalticketAmt": 7,
      "name": "L",
      "count" : 10,

    },
    {
      "typeid": 6,
      "type": "single",
      "numbersSelected": [24],
      "status": "inactive",
      "ticketNo": '',
      "totalticketAmt": 7,
      "count" : 1,


    },
    {
      "typeid": 9,
      "type": "all",
      "numbersSelected": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,  44,  45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,  91, 92, 93, 94, 95, 96, 97, 98, 99],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": 5,
      "name": "A",
      "count" : 100,
    },
  ]
  addslot: any = {
    "bookingDetail": {
      "distributorId": 2,
      "slotId": 0,
      "retailerId": 3,
      "totalPrice": 124
    },
    "tickets": []
  }


  billModel: any = [
    // {
    //   "gherno": 7,
    //   "values": [
    //     {
    //       "typeid": 6,
    //       "type": "single",
    //       "ticketNo": 4,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [26]
    //     },
    //     {
    //       "typeid": 1,
    //       "type": "even",
    //       "ticketNo": 4,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [0, 2, 4, 6, 10]
    //     },
    //     {
    //       "typeid": 2,
    //       "type": "odd",
    //       "ticketNo": 4,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [1, 3, 5, 7, 9]
    //     },
    //     {
    //       "typeid": 3,
    //       "type": "right",
    //       "ticketNo": 5,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [9, 18, 27, 36, 90]
    //     },
    //     {
    //       "typeid": 4,
    //       "type": "left",
    //       "ticketNo": 7,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [0, 11, 22, 33, 99]
    //     },
    //     {
    //       "typeid": 5,
    //       "type": "family",
    //       "ticketNo": 2,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [0, 2, 4, 6, 10],
    //       "roomno": -1,
    //     },

    //   ]
    // }
  ];
  tempModel = {
    "typeid": 0,
    "type": "",
    "ticketNo": 0,
    "totalticketAmt": 0,
    "numbersSelected": [],
    "roomno": -1,
    "no": -1,
    "time": "",
    "count" :1,
  }

  refreshTempModel() {
    this.tempModel = {
      "typeid": 0,
      "type": "",
      "ticketNo": 0,
      "totalticketAmt": 0,
      "roomno": -1,
      "numbersSelected": [],
      "no": -1,
      "time": "",
      "count" :1,

    }
  }

  buttonModel = []
  BackModel = [];
  FstModel = [];
  selectedNumbergher(number: { value: any; }) {
 
    // this.gherarray=[];
    // this.ctrlModel.number =false;
                                                                                                                                                                                                                                                                                                                                                                                                   // if (number.value < 0) {
//   alert('select your gher');
// }
// this.getluckyNumber(number.value);
//console.log('number selected number', number);
this.evenModel.value = number.value;
this.refreshButtonListAndValues(number);
this.evenModel.value = number;
this.selectedNumber1(number.value);
for (let element of this.modelFirst) {
for (let element1 of element) {
element1.status = 'inactive';
if (element1.value == number.value) {
element1.status = 'active';
}
for (let element2 of this.billModel) {

if (element1.value == number.value) {

 element1.status = 'active';
 this.showModel.room = number.value;
// console.log(this.showModel.room);
}
else if (element2.gherno == element1.value) {
 element1.status = 'gray';
}
}
}
}
}


backnumbers : any = [
  {
    "start": 0,
    "end" :9,
    "numbersSelected": 0

  },
  {
    "start": 10,
    "end" :19,
    "numbersSelected": 
      1
    
  },
  {
    "start": 20,
    "end" :29,
    "numbersSelected": 
      2
    
  },
  {
    "start": 30,
    "end" :39,
    "numbersSelected": 
    3
    
  },
  {
    "start": 40,
    "end" :49,
    "numbersSelected": 
      4
    
  },
  {
    "start": 50,
    "end" :59,
    "numbersSelected": 
      5
    
  },
  {
    "start": 60,
    "end" :69,
    "numbersSelected": 
      6
    
  },
  {
    "start": 70,
    "end" :79,
    "numbersSelected": 
      7
    
  },
  {
    "start": 80,
    "end" :89,
    "numbersSelected": 
      8
    
  },
  {
    "start": 90,
    "end" :99,
    "numbersSelected": 
      9
    
  },
]

backluckgher=[];
backlucknumber(gherno:any){
  if(gherno!=-1){
let temp2: string | number;
for(let gh of this.backnumbers){
  if(gherno.value>=gh.start && gherno.value<=gh.end){
    temp2=gh.numbersSelected;
  }
}
this.backluckgher=[];
for (let i = 0; i <= this.modelFirst.length - 1; i++) {
  for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
    if (this.modelFirst[temp2][j].value == this.modelFirst[i][j].value) {
     // this.modelFirst[i][j].status = 'active';
     // this.evenModel.value.value=this.modelFirst[i][j].value;
      this.backluckgher.push({
        "gherno": this.modelFirst[i][j].value,
        "status": 1,
      });
    }
  }
}
//console.log(' this.backluckgher', this.backluckgher);
this.getluckygher=[];
this.getluckygher=this.backluckgher;
//console.log(' this.getluckygher', this.getluckygher);
this.getluckNumbers();
//console.log(' gherno.luck.value', temp2);
  }
}

getbackluckNumbers(){

  //console.log('getluckygher',this.getluckygher);
  //console.log('resultList',this.resultListdummy);
  let mn =1;
  let els =2;
let flag=0;
  for(let luck of this.backluckgher){
    for(let get of this.getLuckyArray){
         if(luck.gherno==get.gherno){
              flag=1;
          }
    }
  }  
  if(flag==1){
    this.mainarray=[];
    this.elsearray=[];
    for(let luck of this.backluckgher){
      for(let get of this.getLuckyArray){
           if(luck.gherno==get.gherno){
            this.mainarray.push({
              "gherno" : get.gherno,
               "luckyno" : get.luckyno,
               "status" : mn,
              
            }); 
            }
      }
    }
    //console.log('mainarray',this.mainarray);
    let temp=[];
    let f=0;
// for(let lk of this.getLuckyArray){
//   f=0;
//   for(let get of this.getluckygher){
//     if(lk.gherno==get.gherno){
// f=1
// break;
//     }
//   }
//   if(f==0){
//     temp.push({
//       "gherno" : lk.gherno,
//       "status": 1
//     })
//   }
// }

// this.getluckygher=[];
// this.getluckygher=temp;
//       for(let get of this.getluckygher){
   
//         for(let luck of this.getLuckyArray ){
//              if(get.gherno==luck.gherno){
//               this.elsearray.push({
//                 "gherno" : luck.gherno,
//                  "luckyno" : luck.luckyno,
//                  "status" : els,
              
//               });  
            
//              }
//             }
         
//           }
//           console.log('elsearray',this.elsearray);
//       this.getLuckyArray=[];
//       for(let el of this.elsearray){
//         this.getLuckyArray.push({
//           "gherno" : el.gherno,
//            "luckyno" : el.luckyno,
//            "status" : el.status,
        
//         }); 
//       }
    //console.log('elsearray getLuckyArray',this.getLuckyArray);
    this.getLuckyArray=[];
    for(let main of this.mainarray){
      this.getLuckyArray.push({
        "gherno" : main.gherno,
         "luckyno" : main.luckyno,
         "status" : main.status,
        
      }); 
    }
    //console.log('mainarray getLuckyArray',this.getLuckyArray);
  }



  if(flag==0){
    this.mainarray=[];
    this.elsearray=[];
    for(let res of this.getLuckyArray){
      if(res.status==1)
      {
        res.status=2;
      }
      }

      let temp=[];
      temp=this.getLuckyArray;
      this.getLuckyArray=[];
    for(let luck of this.backluckgher)
    { 
    for(let res of this.resultList){
      if(luck.gherno==res.gherNo){
      this.getLuckyArray.push({
        "gherno" : res.gherNo,
         "luckyno" : res.luckyNumber,
         "status" : mn,
        
      });
      }
   }
  }
  for(let tp of temp){
    this.getLuckyArray.push({
      "gherno" : tp.gherno,
       "luckyno" : tp.luckyno,
       "status" : tp.status,
      
    });
  }
  temp=[];
  }
  //console.log('getLuckyArray',this.getLuckyArray);
  this.getluckygher=[];
  this.getluck=1;
  this.getreverseLuckyArray=this.getLuckyArray;
//this.getreverse();
}
  selectedNumber(number: { value: any; }) {
    
 if( this.ctrlcheck==1){
  this.gherarray=[];
  this.ctrlModel.number =false;
 }
              
      this.getluck=1;                                                                                                                                                                                                                                                                                                                                                                                                      // if (number.value < 0) {
    //   alert('select your gher');
    // }
    this.backlucknumber(number);
    // this.getluckyNumber(number);
    //console.log('number selected number', number);
    this.evenModel.value = number.value;
    this.refreshButtonListAndValues(number);
    this.evenModel.value = number;
    this.selectedNumber1(number.value);
    for (let element of this.modelFirst) {
      for (let element1 of element) {
        element1.status = 'inactive';
        if (element1.value == number.value) {
          element1.status = 'active';
        }
        for (let element2 of this.billModel) {

          if (element1.value == number.value) {

            element1.status = 'active';
            this.showModel.room = number.value;
            console.log(this.showModel.room);
          }
          else if (element2.gherno == element1.value) {
            element1.status = 'gray';
          }
        }
      }
    }
  }

  secondModelValues = [];
  secondModelFirstValues = [];

  selectedNumber1(number: { value: any; }) {
    // this.familyModel.value = number.value;
    // console.log('selectedNumber1',this.familyModel.value);
    this.BackModel= [];
    this.FstModel = [];
    for (let element of this.modelSecond) {
      for (let element1 of element) {
        element1.status = 'inactive';
        this.secondModelValues[element1.value] = '';
        for (let element2 of this.billModel) {
          if (element2.gherno == number) {
            for (let selectedValues of element2.values) {
              if(selectedValues.typeid==3){

              }else{
                this.buttonModel[selectedValues.typeid] = selectedValues.ticketNo;
                this.buttonModel[selectedValues.roomno] = selectedValues.roomno;
              }
              
              // this.modelSecond[selectedValues.roomno].status='yellow';
              if (element1.value == selectedValues.roomno) {
                element1.status = 'dark';
              }
              for (let valuesraw of selectedValues.numbersSelected) {
                if (element1.value == valuesraw) {
                  if (selectedValues.type == "even") {
                    element1.status = 'active';
                  }
                  else if (selectedValues.type == "odd") {
                    element1.status = 'change';
                  }
                  else if (selectedValues.type == "family") {
                    element1.status = 'yellow';

                  }
                  else if (selectedValues.type == "right") {
                    element1.status = 'danger';
                  }
                  else if (selectedValues.type == "left") {
                    element1.status = 'dark';
                  }
                  else {
                    element1.status = 'active';
                  }

                  if (selectedValues.type == "single") {
                    this.secondModelValues[element1.value] = selectedValues.ticketNo;
                  }
                  if (selectedValues.type == "back") {
                    this.BackModel[element1.value] = selectedValues.ticketNo;
                  }
                   if (selectedValues.type == "front") {
                    this.FstModel[element1.value] = selectedValues.ticketNo;
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  refreshButtonListAndValues(number: { value: any; }) {

    for (let button in this.buttonModel) {
      this.buttonModel[button] = 0;
      for (let element2 of this.billModel) {
        if (element2.gherno == number) {
          for (let selectedValues of element2.values) {
            if (selectedValues.typeid == selectedValues.typeid)
            {
              if(selectedValues.typeid==3){
                this.buttonModel[selectedValues.typeid] = " ";
              }else{
                this.buttonModel[selectedValues.typeid] = selectedValues.ticketNo;
              }
            }
           
              
          }
        }
      }
    }
  }


  // ------------------------ eventicket Copy ----------------------
//   eventicketNumber(event: any, _model_id: number) {
// if(this.ctrlModel.number == true){
//   let chamt =  this.buttonModel[event];
// if(chamt<=0){
//   this.alertService.swalError("Ticket No should not be less than One!!");
// }else{
//   for(let button of this.buttonsList){
//   for (let ctr of this.gherarray) {
//     if (ctr.status == 1) {
//       if (button.type == 'even')
//       {
//         button.ticketNo = +this.buttonModel[event];
//         button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
//         this.billModel.push({
//           "gherno": ctr.gherno,
//           "values": [button],
//         });
//         this.isDisabled=false;
//         let temp: { name: any; status: string; value: any; };
//         for(let gh of this.gherarray){
//            temp = {
//             name: gh.gherno,
//             status: "active",
//           value:gh.gherno
//            }
//         }
//         this.gettotal();
//     this.selectedNumbergher(temp);
//       }else if(button.type == 'odd'){
//         button.ticketNo = +this.buttonModel[event];
//         button['totalticketAmt'] = this.getPriceModel.oddPrice * this.buttonModel[event];
//         this.billModel.push({
//           "gherno": ctr.gherno,
//           "values": [button],

//         });
//         this.isDisabled=false;
//         let temp: { name: any; status: string; value: any; };
//         for(let gh of this.gherarray){
//            temp = {
//             name: gh.gherno,
//             status: "active",
//           value:gh.gherno
//            }
//         }
//     this.selectedNumbergher(temp);
//     this.gettotal();
//       }else if(button.type == 'left'){
//         button.ticketNo = +this.buttonModel[event];
//         button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
//         this.billModel.push({
//           "gherno": ctr.gherno,
//           "values": [button],

//         });
//         let temp: { name: any; status: string; value: any; };
//         for(let gh of this.gherarray){
//            temp = {
//             name: gh.gherno,
//             status: "active",
//           value:gh.gherno
//            }
//         }
//         this.gettotal();
//     this.selectedNumbergher(temp);
//         this.isDisabled=false;
//       }else if(button.type == 'right'){
//         button.ticketNo = +this.buttonModel[event];
//         button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
//         this.billModel.push({
//           "gherno": ctr.gherno,
//           "values": [button],

//         });
//         let temp: { name: any; status: string; value: any; };
//         for(let gh of this.gherarray){
//            temp = {
//             name: gh.gherno,
//             status: "active",
//           value:gh.gherno
//            }
//         }
//     this.selectedNumbergher(temp);
//         this.isDisabled=false;
//         this.gettotal();
//       }
//       else if(button.type == 'all'){
//         button.ticketNo = +this.buttonModel[event];
//         button['totalticketAmt'] = this.getPriceModel.allPrice * this.buttonModel[event];
//         this.billModel.push({
//           "gherno": ctr.gherno,
//           "values": [button],
//         });
//         let temp: { name: any; status: string; value: any; };
//         for(let gh of this.gherarray){
//            temp = {
//             name: gh.gherno,
//             status: "active",
//           value:gh.gherno
//            }
//         }
//     this.selectedNumbergher(temp);
//         this.isDisabled=false;
//         this.gettotal();
//       }
//       else if(button.type == 'family'){
//         this.familyModel.value = this.familyModel.rooms;
//         for(let ar of this.array){
//           if(this.familyModel.value==ar.value)
//           {
//            this.tempModel.numbersSelected = ar.numbersSelected;
//            this.tempModel.count=ar.count;
//           }
        
//         }        
//       this.tempModel.typeid = 3;
//       this.tempModel.ticketNo=this.storageService.get('ticketamount');
//       //this.tempModel.ticketNo = this.buttonModel[event];
//       this.tempModel.totalticketAmt = this.tempModel.count * this.getPriceModel.singlePrice *  this.tempModel.ticketNo;
   
//       this.billModel.push({
//         "gherno": ctr.gherno,
//         "values": [this.tempModel]
//       })
//       console.log(this.billModel);
//       let temp: { name: any; status: string; value: any; };
//       for(let gh of this.gherarray){
//          temp = {
//           name: gh.gherno,
//           status: "active",
//         value:gh.gherno
//          }
//       }
//       this.gettotal();
//   this.selectedNumbergher(temp);
//       this.isDisabled=false;
//      this.refreshTempModel();
//       }
//     }
//   }
// }
//   this.refreshTempModel();
//   // this.gherarray = [];
//   // this.ctrlModel.number = false;
//   console.log(this.billModel);
//   let temp: { name: any; status: string; value: any; };
//       for(let gh of this.gherarray){
//          temp = {
//           name: gh.gherno,
//           status: "active",
//         value:gh.gherno
//          }
//       }
//   this.selectedNumbergher(temp);
//   this.gettotal();
//   // this.selectedNumber(this.evenModel.value);
//     }
//   }else if(this.ctrlModel.number == false){
//     this.gettotal();
//     let added = false;
//     let fam = 0;
//     let check=0;
//     if (this.evenModel.value == -1) {
//       this.alertService.swalError("Please Select Your Gher!!");
//     }else{
//       let chamt =  this.buttonModel[event];
//       if(chamt<=0){
//         this.alertService.swalError("Ticket No should not be less than One!!");
//       }else{
//         for (let element of this.billModel) {
//           if (element.gherno == this.evenModel.value.value) {
//             check=1;
//             for (let element1 of element.values) {
//               if (element1.typeid == event) {
//                 if (element1.type == 'single') {
//                 } else if (element1.type == 'family') {
//                   if (element1.roomno != this.familyModel.value) {
//                     this.alertService.swalError('you can not select another family on same gher');
//                     fam = 1;
//                   }else if (element1.roomno == this.familyModel.value) {
//                     for(let button of this.buttonsList){
//                       button.ticketNo = this.buttonModel[event];
      
//                       button['totalticketAmt'] = this.getPriceModel.familyPrice * this.buttonModel[event];
                      
//                       element.values.push(
//                         button
//                       )
//                       added = true;
//                       this.isDisabled=false;
//                       console.log(this.billModel);
//                       this.selectedNumber(this.evenModel.value);
//                     }
                  
//                   }
//                 } else if (element1.type == 'even'){
//                   element1.ticketNo = this.buttonModel[event];
//                   element1.totalticketAmt= this.getPriceModel.evenPrice * element1.ticketNo;
//                   added = true;
//                   this.isDisabled=false;
//                   this.selectedNumber(this.evenModel.value);
//                 }
//                 else if (element1.type == 'odd'){
//                   element1.ticketNo = this.buttonModel[event];
//                   element1.totalticketAmt= this.getPriceModel.oddPrice * element1.ticketNo;
//                   added = true;
//                   this.isDisabled=false;
//                   this.selectedNumber(this.evenModel.value);
//                 }
//                 else if (element1.type == 'left'){
//                   element1.ticketNo = this.buttonModel[event];
//                   element1.totalticketAmt= this.getPriceModel.leftPrice * element1.ticketNo;
//                   added = true;
//                   this.isDisabled=false;
//                   this.selectedNumber(this.evenModel.value);
//                 }
//                 else if (element1.type == 'right'){
//                   element1.ticketNo = this.buttonModel[event];
//                   element1.totalticketAmt= this.getPriceModel.rightPrice * element1.ticketNo;
//                   added = true;
//                   this.isDisabled=false;
//                   this.selectedNumber(this.evenModel.value);
//                 }
//                 else if (element1.type == 'all'){
//                   element1.ticketNo = this.buttonModel[event];
//                   element1.totalticketAmt= this.getPriceModel.allPrice * element1.ticketNo;
//                   added = true;
//                   this.isDisabled=false;
//                   this.selectedNumber(this.evenModel.value);
//                 }
                
//                 else {
//                   element1.ticketNo = this.buttonModel[event];
//                   element1.totalticketAmt= this.getPriceModel.familyPrice * element1.ticketNo;
//                   added = true;
//                   this.isDisabled=false;
//                   this.selectedNumber(this.evenModel.value);
//                 }
//               }
//             }
//           }
//         }

//         //start
//         if(check==0) {
//           for(let button of this.buttonsList){
//             if(button.type == 'right'){
//               button.ticketNo = +this.buttonModel[event];
//               button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
//               this.billModel.push({
//                 "gherno": +this.evenModel.value.value,
//                 "values": [button],
   
//               });
//               console.log(this.billModel);
//               this.isDisabled=false;
//               this.selectedNumber(this.evenModel.value);
//             }else if(button.type == 'left'){
//               button.ticketNo = +this.buttonModel[event];
//               button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
//               this.billModel.push({
//                 "gherno": +this.evenModel.value.value,
//                 "values": [button],
   
//               });
//               console.log(this.billModel);
//               this.isDisabled=false;
//               this.selectedNumber(this.evenModel.value);
//             }else if(button.type == 'even'){
//               button.ticketNo = +this.buttonModel[event];
//               button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
//               this.billModel.push({
//                 "gherno": +this.evenModel.value.value,
//                 "values": [button],
   
//               });
//               console.log(this.billModel);
//               this.isDisabled=false;
//               this.selectedNumber(this.evenModel.value);
//             }else if(button.type == 'odd'){
//               button.ticketNo = +this.buttonModel[event];
//               button['totalticketAmt'] = this.getPriceModel.oddPrice  * this.buttonModel[event];
//               this.billModel.push({
//                 "gherno": +this.evenModel.value.value,
//                 "values": [button],
   
//               });
//               console.log(this.billModel);
//               this.isDisabled=false;
//               this.selectedNumber(this.evenModel.value);
//             }
//             else if(button.type == 'all'){
//               button.ticketNo = +this.buttonModel[event];
//               button['totalticketAmt'] = this.getPriceModel.allPrice  * this.buttonModel[event];
//               this.billModel.push({
//                 "gherno": +this.evenModel.value.value,
//                 "values": [button],
   
//               });
//               console.log(this.billModel);
//               this.isDisabled=false;
//               this.selectedNumber(this.evenModel.value);
//             }else if(button.type='family'){
              
//             if (this.familyModel.value == -1) {
//               this.alertService.swalError("Please Select Your Number!!");
//             } else {

//                  for(let ar of this.array){
//                    if(this.familyModel.value==ar.value)
//                    {
//                     this.tempModel.numbersSelected = ar.numbersSelected;
//                     this.tempModel.count=ar.count;
//                    }
                 
//                  }        
//                this.tempModel.typeid = 3;
             
//                this.tempModel.ticketNo = this.buttonModel[event];
//                this.tempModel.totalticketAmt =this.tempModel.count*  this.getPriceModel.singlePrice *  this.tempModel.ticketNo;
            
//                this.billModel.push({
//                  "gherno": this.evenModel.value.value,
//                  "values": [this.tempModel]
//                })
//                console.log(this.billModel);
//                this.isDisabled=false;
//                this.familyModel.value =-1;
//                this.selectedNumber(this.evenModel.value);
//               this.refreshTempModel();
//             }
//           }
//         }
        

         
//        }
//       }
//     }
//     }
//     // this.gettotal();
//     // let added = false;
//     // let fam = 0;
//   //   if (this.evenModel.value == -1) {
//   //  //   this.alertService.swalError("Please Select Your Gher!!");
//   //   } else {
// //let chamt =  this.buttonModel[event];
// // if(chamt<=0){
// //  // this.alertService.swalError("Ticket No should not be less than One!!");
// // }else {
//   // for (let element of this.billModel) {
//   //   if (element.gherno == this.evenModel.value.value) {
//   //     for (let element1 of element.values) {
//   //       if (element1.typeid == event) {
//   //         if (element1.type == 'single') {
//   //         } else if (element1.type == 'family') {
//   //           if (element1.roomno != this.familyModel.value) {
//   //             alert('you can not select another family on same gher');
//   //             fam = 1;
//   //           }

//   //           // element1.ticketNo = this.buttonModel[event];
//   //           // added = true;
//   //           // this.selectedNumber(this.evenModel.value);
//   //         } else if (element1.type == 'even'){
//   //           element1.ticketNo = this.buttonModel[event];
//   //           element1.totalticketAmt= this.getPriceModel.evenPrice * element1.ticketNo;
//   //           added = true;
//   //           this.isDisabled=false;
//   //           this.selectedNumber(this.evenModel.value);
//   //         }
//   //         else if (element1.type == 'odd'){
//   //           element1.ticketNo = this.buttonModel[event];
//   //           element1.totalticketAmt= this.getPriceModel.oddPrice * element1.ticketNo;
//   //           added = true;
//   //           this.isDisabled=false;
//   //           this.selectedNumber(this.evenModel.value);
//   //         }
//   //         else if (element1.type == 'left'){
//   //           element1.ticketNo = this.buttonModel[event];
//   //           element1.totalticketAmt= this.getPriceModel.leftPrice * element1.ticketNo;
//   //           added = true;
//   //           this.isDisabled=false;
//   //           this.selectedNumber(this.evenModel.value);
//   //         }
//   //         else if (element1.type == 'right'){
//   //           element1.ticketNo = this.buttonModel[event];
//   //           element1.totalticketAmt= this.getPriceModel.rightPrice * element1.ticketNo;
//   //           added = true;
//   //           this.isDisabled=false;
//   //           this.selectedNumber(this.evenModel.value);
//   //         }
//   //         else if (element1.type == 'all'){
//   //           element1.ticketNo = this.buttonModel[event];
//   //           element1.totalticketAmt= this.getPriceModel.allPrice * element1.ticketNo;
//   //           added = true;
//   //           this.isDisabled=false;
//   //           this.selectedNumber(this.evenModel.value);
//   //         }
          
//   //         else {
//   //           element1.ticketNo = this.buttonModel[event];
//   //           element1.totalticketAmt= this.getPriceModel.familyPrice * element1.ticketNo;
//   //           added = true;
//   //           this.isDisabled=false;
//   //           this.selectedNumber(this.evenModel.value);
//   //         }
//   //       }
//   //     }
//   //     if (added == false) {
//   //       this.refreshTempModel();
//   //       for (let button of this.buttonsList) {
//   //         if (button.typeid == event) {
//   //           if (button.type == 'single') {

//   //           } else {
//   //             if (button.type == 'family') {


//   //               button.ticketNo = this.buttonModel[event];

//   //               button['totalticketAmt'] = this.getPriceModel.familyPrice * this.buttonModel[event];
                
//   //               element.values.push(
//   //                 button
//   //               )
//   //               added = true;
//   //               this.isDisabled=false;
//   //               console.log(this.billModel);
//   //               this.selectedNumber(this.evenModel.value);
//   //             }
//   //           }
//   //         }
//   //       }
//   //     }
//   //   }
//   // }
//   // if (added == false) {
//   //   this.refreshTempModel();
//   //   for (let button of this.buttonsList) {
//   //     if (button.typeid == event) {
//   //       if (button.type == 'single') {
//   //       }
//   //       else if (button.type == 'family') {

//   //         if (this.ctrlModel.number == true) {

//   //           this.storageService.set('ticketamount',this.buttonModel[event]);
//   //           for (let ctr of this.gherarray) {
//   //             if (ctr.status == 1) {
//   //               this.familyModel.value = this.familyModel.rooms;
//   //             //  this.FamilysingleNumber();
//   //               let Fflag = false;
//   //               for (let check of this.billModel) {
//   //                 if (check.gherno == this.evenModel.value.value) {
//   //                   if (check.roomno != this.familyModel.value) {
//   //                     alert('you can not select another family on same gher');
//   //                     Fflag = true;
//   //                     fam=1;
//   //                   }
//   //                 }
//   //               }
//   //               if (fam == 0) {
//   //                 for(let ar of this.array){
//   //                   if(this.familyModel.value==ar.value)
//   //                   {
//   //                    this.tempModel.numbersSelected = ar.numbersSelected;
//   //                    this.tempModel.count=ar.count;
//   //                   }
                  
//   //                 }        


//   //               this.tempModel.typeid = 3;
//   //               this.tempModel.ticketNo=this.storageService.get('ticketamount');
//   //               //this.tempModel.ticketNo = this.buttonModel[event];
//   //               this.tempModel.totalticketAmt = this.tempModel.count * this.getPriceModel.singlePrice *  this.tempModel.ticketNo;
             
//   //               this.billModel.push({
//   //                 "gherno": ctr.gherno,
//   //                 "values": [this.tempModel]
//   //               })
//   //               console.log(this.billModel);
//   //               this.selectedNumber(this.evenModel.value);
//   //               this.isDisabled=false;
//   //              this.refreshTempModel();
//   //               }

//   //             }


//   //           }

//   //           this.refreshTempModel();
//   //           this.gherarray = [];
//   //           this.ctrlModel.number = false;
//   //         }
//   //         else {
//   //           let Fflag = false;
//   //           for (let check of this.billModel) {
//   //             if (check.gherno == this.evenModel.value.value) {
//   //               if (check.roomno != this.familyModel.value) {
//   //                 alert('you can not select another family on same gher');
//   //                 Fflag = true;
//   //               }
//   //             }
//   //           }
//   //           if (fam == 0) {
//   //             if (this.familyModel.value == -1) {
//   //               this.alertService.swalError("Please Select Your Number!!");
//   //             } else {

//   //                  for(let ar of this.array){
//   //                    if(this.familyModel.value==ar.value)
//   //                    {
//   //                     this.tempModel.numbersSelected = ar.numbersSelected;
//   //                     this.tempModel.count=ar.count;
//   //                    }
                   
//   //                  }        
//   //                this.tempModel.typeid = 3;
               
//   //                this.tempModel.ticketNo = this.buttonModel[event];
//   //                this.tempModel.totalticketAmt =this.tempModel.count*  this.getPriceModel.singlePrice *  this.tempModel.ticketNo;
              
//   //                this.billModel.push({
//   //                  "gherno": this.evenModel.value.value,
//   //                  "values": [this.tempModel]
//   //                })
//   //                console.log(this.billModel);
//   //                this.isDisabled=false;
//   //                this.familyModel.value =-1;
//   //                this.selectedNumber(this.evenModel.value);
//   //               this.refreshTempModel();
              
             
//   //              }


//   //           }
//   //         }


//   //       }
//   //       else {

//   //         if (this.ctrlModel.number == true) {
//   //           // for (let ctr of this.gherarray) {
//   //           //   if (ctr.status == 1) {
//   //           //     if (button.type == 'even')
//   //           //     {
//   //           //       button.ticketNo = +this.buttonModel[event];
//   //           //       button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
//   //           //       this.billModel.push({
//   //           //         "gherno": ctr.gherno,
//   //           //         "values": [button],
//   //           //       });
//   //           //       this.isDisabled=false;
//   //           //     }else if(button.type == 'odd'){
//   //           //       button.ticketNo = +this.buttonModel[event];
//   //           //       button['totalticketAmt'] = this.getPriceModel.oddPrice * this.buttonModel[event];
//   //           //       this.billModel.push({
//   //           //         "gherno": ctr.gherno,
//   //           //         "values": [button],

//   //           //       });
//   //           //       this.isDisabled=false;
//   //           //     }else if(button.type == 'left'){
//   //           //       button.ticketNo = +this.buttonModel[event];
//   //           //       button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
//   //           //       this.billModel.push({
//   //           //         "gherno": ctr.gherno,
//   //           //         "values": [button],

//   //           //       });
//   //           //       this.isDisabled=false;
//   //           //     }else if(button.type == 'right'){
//   //           //       button.ticketNo = +this.buttonModel[event];
//   //           //       button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
//   //           //       this.billModel.push({
//   //           //         "gherno": ctr.gherno,
//   //           //         "values": [button],

//   //           //       });
//   //           //       this.isDisabled=false;
//   //           //     }
//   //           //     else if(button.type == 'all'){
//   //           //       button.ticketNo = +this.buttonModel[event];
//   //           //       button['totalticketAmt'] = this.getPriceModel.allPrice * this.buttonModel[event];
//   //           //       this.billModel.push({
//   //           //         "gherno": ctr.gherno,
//   //           //         "values": [button],

//   //           //       });
//   //           //       this.isDisabled=false;
//   //           //     }
//   //           //   }
//   //           // }
//   //           // this.refreshTempModel();
//   //           // this.gherarray = [];
//   //           // this.ctrlModel.number = false;
//   //           // console.log(this.billModel);
//   //           // this.selectedNumber(this.evenModel.value);
//   //         }
//   //         else {
//   //            if(button.type == 'right'){
//   //             button.ticketNo = +this.buttonModel[event];
//   //             button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
//   //             this.billModel.push({
//   //               "gherno": +this.evenModel.value.value,
//   //               "values": [button],

//   //             });
//   //             console.log(this.billModel);
//   //             this.isDisabled=false;
//   //             this.selectedNumber(this.evenModel.value);
//   //           }else if(button.type == 'left'){
//   //             button.ticketNo = +this.buttonModel[event];
//   //             button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
//   //             this.billModel.push({
//   //               "gherno": +this.evenModel.value.value,
//   //               "values": [button],

//   //             });
//   //             console.log(this.billModel);
//   //             this.isDisabled=false;
//   //             this.selectedNumber(this.evenModel.value);
//   //           }else if(button.type == 'even'){
//   //             button.ticketNo = +this.buttonModel[event];
//   //             button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
//   //             this.billModel.push({
//   //               "gherno": +this.evenModel.value.value,
//   //               "values": [button],

//   //             });
//   //             console.log(this.billModel);
//   //             this.isDisabled=false;
//   //             this.selectedNumber(this.evenModel.value);
//   //           }else if(button.type == 'odd'){
//   //             button.ticketNo = +this.buttonModel[event];
//   //             button['totalticketAmt'] = this.getPriceModel.oddPrice  * this.buttonModel[event];
//   //             this.billModel.push({
//   //               "gherno": +this.evenModel.value.value,
//   //               "values": [button],

//   //             });
//   //             console.log(this.billModel);
//   //             this.isDisabled=false;
//   //             this.selectedNumber(this.evenModel.value);
//   //           }
//   //           else if(button.type == 'all'){
//   //             button.ticketNo = +this.buttonModel[event];
//   //             button['totalticketAmt'] = this.getPriceModel.allPrice  * this.buttonModel[event];
//   //             this.billModel.push({
//   //               "gherno": +this.evenModel.value.value,
//   //               "values": [button],

//   //             });
//   //             console.log(this.billModel);
//   //             this.isDisabled=false;
//   //             this.selectedNumber(this.evenModel.value);
//   //           }

            
//   //         }

//   //       }
//   //     }
//   //   }

//   // }
// // }

// //     }
 

//   }

  // ------------------End Copy -----------------------------------


 // ------------------------ eventicket Original ----------------------
 eventicketNumber(event: any, _model_id: number) {
  if(this.ctrlModel.number == true){
    let temp: { name: any; status: string; value: any; };
    for(let gh of this.gherarray){
       temp = {
        name: gh.gherno,
        status: "active",
      value:gh.gherno
       }
    }
    this.evenModel.value=temp;
  }
  this.gettotal();
  let added = false;
  let fam = 0;
  if (this.evenModel.value == -1) {
    this.alertService.swalError("Please Select Your Customer Choosable!!");
  } else {
let chamt =  this.buttonModel[event];
if(chamt<=0){
this.alertService.swalError("Form No should not be less than One!!");
}else {

for (let element of this.billModel) {
  if (element.gherno == this.evenModel.value.value) {
    for (let element1 of element.values) {
      if (element1.typeid == event) {
        if (element1.type == 'single') {
        } else if (element1.type == 'family') {
          if (element1.roomno == this.familyModel.value) {

            this.alertService.swalError('Please edit Form no from Application form ');
            fam = 1;
          }

          // element1.ticketNo = this.buttonModel[event];
          // added = true;
          // this.selectedNumber(this.evenModel.value);
        } else if (element1.type == 'even'){
          element1.ticketNo = this.buttonModel[event];
          element1.totalticketAmt= this.getPriceModel.evenPrice * element1.ticketNo;
          added = true;
          this.isDisabled=false;
          this.selectedNumber(this.evenModel.value);
        }
        else if (element1.type == 'odd'){
          element1.ticketNo = this.buttonModel[event];
          element1.totalticketAmt= this.getPriceModel.oddPrice * element1.ticketNo;
          added = true;
          this.isDisabled=false;
          this.selectedNumber(this.evenModel.value);
        }
        else if (element1.type == 'left'){
          element1.ticketNo = this.buttonModel[event];
          element1.totalticketAmt= this.getPriceModel.leftPrice * element1.ticketNo;
          added = true;
          this.isDisabled=false;
          this.selectedNumber(this.evenModel.value);
        }
        else if (element1.type == 'right'){
          element1.ticketNo = this.buttonModel[event];
          element1.totalticketAmt= this.getPriceModel.rightPrice * element1.ticketNo;
          added = true;
          this.isDisabled=false;
          this.selectedNumber(this.evenModel.value);
        }
        else if (element1.type == 'all'){
          element1.ticketNo = this.buttonModel[event];
          element1.totalticketAmt= this.getPriceModel.allPrice * element1.ticketNo;
          added = true;
          this.isDisabled=false;
          this.selectedNumber(this.evenModel.value);
        }
        
        else {
          element1.ticketNo = this.buttonModel[event];
          element1.totalticketAmt= this.getPriceModel.familyPrice * element1.ticketNo;
          added = true;
          this.isDisabled=false;
          this.selectedNumber(this.evenModel.value);
        }
      }
    }
    if (added == false) {
      this.refreshTempModel();
      for (let button of this.buttonsList) {
        if (button.typeid == event) {
          if (button.type == 'single') {

          } else {
            if (button.type == 'family') {
              if(fam==0){
                // button.ticketNo = this.buttonModel[event];
                // button.type="family";
  
                // button['totalticketAmt'] = this.getPriceModel.familyPrice * this.buttonModel[event];
                
                // element.values.push(
                //   button
                // )
                // added = true;
                // this.isDisabled=false;
                // console.log(this.billModel);
                // this.selectedNumber(this.evenModel.value);
              }
             
            }
          }
        }
      }
    }
  }
}
if (added == false) {
  this.refreshTempModel();
  for (let button of this.buttonsList) {
    if (button.typeid == event) {
      if (button.type == 'single') {
      }
      else if (button.type == 'family') {

        if (this.ctrlModel.number == true) {

          this.storageService.set('ticketamount',this.buttonModel[event]);
          for (let ctr of this.gherarray) {
            if (ctr.status == 1) {
              this.familyModel.value = this.familyModel.rooms;
            //  this.FamilysingleNumber();



              let Fflag = false;
              for (let check of this.billModel) {
                if (check.gherno == this.evenModel.value.value) {
                  if (check.roomno == this.familyModel.value) {
                    this.alertService.swalError('Please edit From no volumn from Application from !');
                    Fflag = true;
                    fam=1;
                  }
                }
              }
              if (fam == 0) {

                for(let ar of this.array){
                  if(this.familyModel.value==ar.value)
                  {
                   this.tempModel.numbersSelected = ar.numbersSelected;
                   this.tempModel.count=ar.count;
                   this.tempModel.roomno=this.familyModel.value;
                  }
                
                }        


              this.tempModel.typeid = 3;
              this.tempModel.type="family";
              this.tempModel.ticketNo=this.storageService.get('ticketamount');
              //this.tempModel.ticketNo = this.buttonModel[event];
              this.tempModel.totalticketAmt = this.tempModel.count * this.getPriceModel.singlePrice *  this.tempModel.ticketNo;
           
              this.billModel.push({
                "gherno": ctr.gherno,
                "values": [this.tempModel]
              })
          //    console.log(this.billModel);
              let temp: { name: any; status: string; value: any; };
                      for(let gh of this.gherarray){
                         temp = {
                          name: gh.gherno,
                          status: "active",
                        value:gh.gherno
                         }
                      }
                      this.gettotal();
                  this.selectedNumbergher(temp);
              this.isDisabled=false;
             this.refreshTempModel();
              }

            }


          }

          this.refreshTempModel();
          // this.gherarray = [];
          // this.ctrlModel.number = false;
        }
        else {
          let Fflag = false;
          for (let check of this.billModel) {
            if (check.gherno == this.evenModel.value.value) {
              if (check.roomno == this.familyModel.value) {
                this.alertService.swalError('Please edit From no from Application Form !!');
                Fflag = true;
              }
            }
          }
          if (fam == 0) {
            if (this.familyModel.value == -1) {
              this.alertService.swalError("Please Select Your Application Number!!");
            } else {

                 for(let ar of this.array){
                   if(this.familyModel.value==ar.value)
                   {
                    this.tempModel.numbersSelected = ar.numbersSelected;
                    this.tempModel.count=ar.count;
                    this.tempModel.roomno=this.familyModel.value;
                   }
                 
                 }        
               this.tempModel.typeid = 3;
             this.tempModel.type="family";
               this.tempModel.ticketNo = this.buttonModel[event];
               this.tempModel.totalticketAmt =this.tempModel.count*  this.getPriceModel.singlePrice *  this.tempModel.ticketNo;
            
               this.billModel.push({
                 "gherno": this.evenModel.value.value,
                 "values": [this.tempModel]
               })
           //    console.log(this.billModel);
               this.isDisabled=false;
               this.familyModel.value =-1;
               this.selectedNumber(this.evenModel.value);
              this.refreshTempModel();
            
           
             }


          }
        }


      }
      // if(button.type !='single'){
      else {

        if (this.ctrlModel.number == true) {
          for (let ctr of this.gherarray) {
            if (ctr.status == 1) {
              if (button.type == 'even')
              {
                button.ticketNo = +this.buttonModel[event];
                button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
                this.billModel.push({
                  "gherno": ctr.gherno,
                  "values": [button],

                });
                this.isDisabled=false;
              }else if(button.type == 'odd'){
                button.ticketNo = +this.buttonModel[event];
                button['totalticketAmt'] = this.getPriceModel.oddPrice * this.buttonModel[event];
                this.billModel.push({
                  "gherno": ctr.gherno,
                  "values": [button],

                });
                this.isDisabled=false;
              }else if(button.type == 'left'){
                button.ticketNo = +this.buttonModel[event];
                button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
                this.billModel.push({
                  "gherno": ctr.gherno,
                  "values": [button],

                });
                this.isDisabled=false;
              }else if(button.type == 'right'){
                button.ticketNo = +this.buttonModel[event];
                button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
                this.billModel.push({
                  "gherno": ctr.gherno,
                  "values": [button],

                });
                this.isDisabled=false;
              }
              else if(button.type == 'all'){
                button.ticketNo = +this.buttonModel[event];
                button['totalticketAmt'] = this.getPriceModel.allPrice * this.buttonModel[event];
                this.billModel.push({
                  "gherno": ctr.gherno,
                  "values": [button],

                });
                this.isDisabled=false;
              }
            }
          }
          this.refreshTempModel();
          // this.gherarray = [];
          // this.ctrlModel.number = false;
       //   console.log(this.billModel);
          let temp: { name: any; status: string; value: any; };
        for(let gh of this.gherarray){
           temp = {
            name: gh.gherno,
            status: "active",
          value:gh.gherno
           }
        }
        this.gettotal();
    this.selectedNumbergher(temp);
        }
        else {
           if(button.type == 'right'){
            button.ticketNo = +this.buttonModel[event];
            button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
            this.billModel.push({
              "gherno": +this.evenModel.value.value,
              "values": [button],

            });
        //    console.log(this.billModel);
            this.isDisabled=false;
            this.selectedNumber(this.evenModel.value);
          }else if(button.type == 'left'){
            button.ticketNo = +this.buttonModel[event];
            button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
            this.billModel.push({
              "gherno": +this.evenModel.value.value,
              "values": [button],

            });
         //   console.log(this.billModel);
            this.isDisabled=false;
            this.selectedNumber(this.evenModel.value);
          }else if(button.type == 'even'){
            button.ticketNo = +this.buttonModel[event];
            button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
            this.billModel.push({
              "gherno": +this.evenModel.value.value,
              "values": [button],

            });
        //    console.log(this.billModel);
            this.isDisabled=false;
            this.selectedNumber(this.evenModel.value);
          }else if(button.type == 'odd'){
            button.ticketNo = +this.buttonModel[event];
            button['totalticketAmt'] = this.getPriceModel.oddPrice  * this.buttonModel[event];
            this.billModel.push({
              "gherno": +this.evenModel.value.value,
              "values": [button],

            });
        //    console.log(this.billModel);
            this.isDisabled=false;
            this.selectedNumber(this.evenModel.value);
          }
          else if(button.type == 'all'){
            button.ticketNo = +this.buttonModel[event];
            button['totalticketAmt'] = this.getPriceModel.allPrice  * this.buttonModel[event];
            this.billModel.push({
              "gherno": +this.evenModel.value.value,
              "values": [button],

            });
         //   console.log(this.billModel);
            this.isDisabled=false;
            this.selectedNumber(this.evenModel.value);
          }

          
        }

      }
    }
  }

}
}

  }
  this.gettotal();

}

  // ------------------End Original  -----------------------------------
 







  FNumber(_number: any) {
  //  console.log('family number',_number);
    this.familyModel.value = _number;
    this.familyModel.rooms = _number;

  }



//SingleSelection------------------Duplicate------------------------------

singleSelection(input: any) {
  let ticketno=this.secondModelValues[input];
  if(ticketno<=0){
    this.alertService.swalError("Application No should not be less than One!!");
  }else{
  if(this.ctrlModel.number == true){
    this.gettotal();
    let add = false;
    this.refreshTempModel();

    
   
    for (let ctr of this.gherarray) {
      if (ctr.status == 1) {

        for(let book of this.billModel){
          if(ctr.gherno==book.gherno){
            for (let element1 of book.values) {
              if (element1.type == 'single') {
                if (input == element1.numbersSelected[0]) {
                  element1.ticketNo = this.secondModelValues[input];
                  element1.totalticketAmt= this.getPriceModel.singlePrice * element1.ticketNo;
                  add = true;
                }
              }
            }
          }
        }
        if(add==false){
          this.tempModel.typeid = 6;
          this.tempModel.numbersSelected = [input];
          this.tempModel.ticketNo = this.secondModelValues[input];
          this.tempModel.type = 'single';
          this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
          this.billModel.push({
            "gherno": ctr.gherno,
            "values": [this.tempModel]
          })
        }
       
      }
    }
    this.gettotal();
     let temp: { name: any; status: string; value: any; };
      for(let gh of this.gherarray){
         temp = {
          name: gh.gherno,
          status: "active",
        value:gh.gherno
         }
      }
      // this.evenModel.value=temp;
      this.selectedNumbergher(temp);
   // this.selectedNumber(temp);
    this.isDisabled=false;
    this.refreshTempModel();
   // this.gherarray = [];
    add = true;
   // this.ctrlModel.number = false;
  }
  else if( this.ctrlModel.number == false){
   if(this.evenModel.value==-1 || this.evenModel.value== ""){
    this.alertService.swalError("Please Select Your Customer Choosable!!");
  }
    else{
      if(this.gherarray==""){

      }
      // let temp: { name: any; status: string; value: any; };
      // for(let gh of this.gherarray){
      //    temp = {
      //     name: gh.gherno,
      //     status: "active",
      //   value:gh.gherno
      //    }
      // }
      // this.evenModel.value=temp;
      this.gettotal();
    let add = false;
    this.refreshTempModel();
    for (let element of this.billModel) {
      if (element.gherno == this.evenModel.value.value) {

        for (let element1 of element.values) {

          if (element1.type == 'single') {
            if (input == element1.numbersSelected[0]) {
              element1.ticketNo = this.secondModelValues[input];
              element1.totalticketAmt= this.getPriceModel.singlePrice * element1.ticketNo;
              add = true;
            }
          }
        }
        if (add == false) {
          this.tempModel.typeid = 6;
          this.tempModel.numbersSelected = [input];
          this.tempModel.ticketNo = this.secondModelValues[input];
          this.tempModel.type = 'single';
          this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
          this.tempModel.count=1;
          element.values.push(
            this.tempModel
          )
          this.refreshTempModel();
          this.selectedNumber(this.evenModel.value);
          this.isDisabled=false;
          add = true;
        }
      }
    }
    if (add == false) {
        this.tempModel.typeid = 6;
        this.tempModel.numbersSelected = [input];
        this.tempModel.ticketNo = this.secondModelValues[input];
        this.tempModel.type = 'single';
        this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
        this.billModel.push({
          "gherno": this.evenModel.value.value,
          "values": [this.tempModel]
        })
        this.selectedNumber(this.evenModel.value);
        this.refreshTempModel();
        this.isDisabled=false;
        add = true;
    }
    this.gettotal();
    }
  } 
} 
  }
 



//------------------------------------End-------------------------------



//SingleSelection------------------Original------------------------------
  // singleSelection(input: any) {

  //   if(this.ctrlModel.number == true){
  //     let temp: { name: any; status: string; value: any; };
  //     for(let gh of this.gherarray){
  //        temp = {
  //         name: gh.gherno,
  //         status: "active",
  //       value:gh.gherno
  //        }
  //     }
  //     this.evenModel.value=temp;
  //   }
  //   if(this.evenModel.value==-1 || this.evenModel.value== ""){
  //     this.alertService.swalError("Please Select Your Gher!!");

  //     //
  //     // Swal.fire({
  //     //   html: '<h3 class="mt-5 mb-4 mx-2">Please Select Your Gher First</h3>',
  //     //   animation: true,
  //     //   width: 548,
  //     //   padding: '-1.9em',
  //     //   // showCancelButton: true,
  //     //   focusConfirm: false,
  //     //   // cancelButtonText: 'No',
  //     //   // cancelButtonColor: '#17A2B8',
  //     //   // cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
  //     //   confirmButtonColor: '#DD6B55',
  //     //   confirmButtonClass: 'btn btn-danger btn-pill px-4',
  //     //   confirmButtonText: 'OK'
  //     // })

  //     //
  //    // alert('Select your gher first');
  //   }else {
  //     let ticketno=this.secondModelValues[input];
  //     if(ticketno<=0){
  //       this.alertService.swalError("Ticket No should not be less than One!!");
  //     }else{
  //       this.gettotal();
  //     let add = false;
  //     this.refreshTempModel();
  
  //     for (let element of this.billModel) {
  //       if (element.gherno == this.evenModel.value.value) {
  
  //         for (let element1 of element.values) {
  
  //           if (element1.type == 'single') {
  //             if (input == element1.numbersSelected[0]) {
  //               element1.ticketNo = this.secondModelValues[input];
  //               element1.totalticketAmt= this.getPriceModel.singlePrice * element1.ticketNo;
  //               add = true;
  //             }
  //           }
  
  //         }
  //         if (add == false) {
  //           this.tempModel.typeid = 6;
  //           this.tempModel.numbersSelected = [input];
  //           this.tempModel.ticketNo = this.secondModelValues[input];
  //           this.tempModel.type = 'single';
  //           this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
  //           this.tempModel.count=1;
  //           element.values.push(
  //             this.tempModel
  //           )
  //           this.refreshTempModel();
  //           this.selectedNumber(this.evenModel.value);
  //           this.isDisabled=false;
  //           add = true;

  //         }
  //       }
  //     }
  //     if (add == false) {
  //       if (this.ctrlModel.number == true) {
  //         for (let ctr of this.gherarray) {
  //           if (ctr.status == 1) {
  //             this.tempModel.typeid = 6;
  //             this.tempModel.numbersSelected = [input];
  //             this.tempModel.ticketNo = this.secondModelValues[input];
  //             this.tempModel.type = 'single';
  //             this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
  //             this.billModel.push({
  //               "gherno": ctr.gherno,
  //               "values": [this.tempModel]
  //             })
  //           }
  
  //         }
  //         this.selectedNumber(this.evenModel.value);
  //         this.isDisabled=false;
  //         this.refreshTempModel();
  //         this.gherarray = [];
  //         add = true;
  //         this.ctrlModel.number = false;
  //       }
  //       else {
  //         this.tempModel.typeid = 6;
  //         this.tempModel.numbersSelected = [input];
  //         this.tempModel.ticketNo = this.secondModelValues[input];
  //         this.tempModel.type = 'single';
  //         this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
  //         this.billModel.push({
  //           "gherno": this.evenModel.value.value,
  //           "values": [this.tempModel]
  //         })
  //         this.selectedNumber(this.evenModel.value);
  //         this.refreshTempModel();
  //         this.isDisabled=false;
  //         add = true;
  //       }
  
  //     }
  
  //     this.gettotal();
  //     }
      
  //   }
   
  // }


  //------------------------------------END-------------------------------
  // first(input: any){
  //   let add=false;
  //   this.refreshTempModel();

  //   for (let element of this.billModel) {
  //     if (element.gherno == this.evenModel.value.value) {

  //       for (let element1 of element.values) {

  //           if(element1.type == 'front' ){
  //            if(input==element1.numbersSelected[0]){
  //                 element1.ticketNo=this.secondModelFirstValues[input];
  //                 add=true;
  //            }
  //           }

  //       }
  //       if(add==false){
  //        this.tempModel.typeid=7;
  //       //  this.tempModel.numbersSelected=[input];
  //        this.tempModel.ticketNo=this.secondModelFirstValues[input];
  //        this.tempModel.type='front';
  //        this.tempModel.totalticketAmt=50;
  //        element.values.push(
  //         this.tempModel
  //        )
  //        this.refreshTempModel();
  //        add=true;
  //       }
  //     }
  //   }
  //     if(add==false){
  //       this.tempModel.typeid=6;
  //       this.tempModel.numbersSelected=[input];
  //       this.tempModel.ticketNo=this.secondModelFirstValues[input];
  //       this.tempModel.type='front';
  //       this.tempModel.totalticketAmt=50;
  //       this.billModel.push({
  //         "gherno" : this.evenModel.value.value,
  //        "values" : [this.tempModel]
  //       })
  //       this.refreshTempModel();
  //       add=true;
  //      }


  // }
  evenModel: any = {
    "value": -1,
    "ticket": 0,
    "ticketno": 0,
    "oddticketno": 0,
    "leftticketno": 0,
    "rightticketno": 0,
    "backPrice": 10,
    "ticketPrice": 50,
    "familyticketPrice": 8,
    "leftticketPrice": 10,
    "totalticketAmt": 0,
    "type": "",
    "backno": -1,
    "fno": "",
    "roomno": "",

  }
  multigherstore: any = [];
  multigherclonestore: any = [];
  multModel: any = {
    "gherno": -1,
    "showgherno": "",
    "flag": 0,
  }
  gherModel: any = {
    "gher": 0,
  }

  frontNumber: any = []
  Front: any = []
  timeTable: any = []
  frontModel: any = {
    "id": "",
  }

  numberModel: any = {
    "value": 0,
  }
  showModel: any = {
    "value": 0,
    "temp": "",
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
  }

  familyModel: any = {
    "single": 0,
    "value": -1,
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
    "ticketPrice": 8,
    "setvar": "",
    "first": "",
    "back": "",
    "ticketno": 0,
    "rooms": -1,
  }

  backModel: any = {
    "backno": -1,
    "ticket": 0,
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
    "ticketPrice": 8,
    "setvar": "",
    "ticketno": 0,
  }
  firstModel: any = {
    "firstno": -1,
    "ticket": 0,
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
    "ticketPrice": 8,
    "setvar": "",
    "ticketno": 0,
  }

  checkboxModel: any = {
    "value": '',
  }

  totalModel: any = {
    "total": 0,
    "tickettotal":0
  }
  len: { [s: string]: unknown; } | ArrayLike<unknown>;

  // billModel: any = {
  //    "id" : 0,
  //    "gher": "",
  //    "ticketno" : 0,
  //    "totalticketAmt" : this.evenModel.ticketno,
  // }

  IsVisible = true;


  familyArray: any = [];

  familyResultArray: any = [];


  model: any;

  // ---------------------------- Front Operation ------------------------

  firstNumber(_event: any) {
  //  console.log('first number', _event);
    this.firstModel.firstno = _event;
    // if(this.ctrlModel.number == true){
    //   let temp: { name: any; status: string; value: any; };
    //   for(let gh of this.gherarray){
    //      temp = {
    //       name: gh.gherno,
    //       status: "active",
    //     value:gh.gherno
    //      }
    //   }
    //   this.evenModel.value=temp;
    // }
    // if (this.evenModel.value != -1) {
    // } else {
    //  // alert('');
    //   this.alertService.swalError('Select Your Gher!');
    // }
  }


  //-----------------------------------------Front Numbers Copy ---------------------------

  first(_event: any) {
    this.gettotal();
    let flag = 0;
    let f: number;
    this.firstModel.ticket = _event.target.value;
    if( this.firstModel.ticket<=0){
      this.alertService.swalError("Application No should not be less than 0");
    }else{
    if (this.ctrlModel.number == true) {
          
      for (let ctr of this.gherarray) {
        if (ctr.status == 1) {
          f=0;
          for (let element of this.billModel) {
           
            if (ctr.gherno == element.gherno) {
    
              for (let element1 of element.values) {
                for (let element2 of element.values) {
                  if (element1.type == 'front') {
                    if (element1.no == this.firstModel.firstno) {
                      element2.ticketNo = this.firstModel.ticket;
                      element2.totalticketAmt= this.getPriceModel.frontPrice * element2.ticketNo;
    
                      f = 1;
                    }
                  }
                }
    
              }
            }
    
    
          }
if(f==0){
  this.tempModel.ticketNo = _event.target.value;
  this.tempModel.type = 'front';
  this.tempModel.typeid = 7;
  this.tempModel.totalticketAmt = this.getPriceModel.frontPrice *  this.tempModel.ticketNo;
  this.tempModel.no = this.firstModel.firstno;
  this.firstroom(this.firstModel.firstno);
  this.shownoF();
  var b = this.familyModel.first.split(',');
//  console.log('b', b);
  var c = b.map(Number);
//  console.log('c', c);
  this.tempModel.numbersSelected = c;
//  console.log(_event.target.value);
  // this.addLotteryNo();
  this.billModel.push({
    "gherno": +ctr.gherno,
    "values": [this.tempModel]
  });
//  console.log('billmodel', this.billModel);
  this.isDisabled=false;
  this.familyModel.first = "";
  this.firstModelno = [];
}
       
          // this.refreshTempModel();
        }

      }
      this.refreshTempModel();
     // this.gherarray = [];
     // this.ctrlModel.number = false;
  //    console.log(this.billModel);
      this.gettotal();
      let temp: { name: any; status: string; value: any; };
      for(let gh of this.gherarray){
         temp = {
          name: gh.gherno,
          status: "active",
        value:gh.gherno
         }
      }
      // this.evenModel.value=temp;
      this.selectedNumbergher(temp);
      // this.selectedNumber(this.evenModel.value);
    } else if(this.ctrlModel.number == false){
      if (this.evenModel.value != -1) {
        for (let element of this.billModel) {
          if (this.evenModel.value.value == element.gherno) {
  
            for (let element1 of element.values) {
              for (let element2 of element.values) {
                if (element1.type == 'front') {
                  if (element1.no == this.firstModel.firstno) {
                    element2.ticketNo = this.firstModel.ticket;
                    element2.totalticketAmt= this.getPriceModel.frontPrice * element2.ticketNo;
  
                    flag = 1;
                  }
                }
              }
  
            }
          }
  
  
        }
         if (flag != 1) {
            this.tempModel.ticketNo = _event.target.value;
            this.tempModel.type = 'front';
            this.tempModel.typeid = 7;
            this.tempModel.totalticketAmt =this.getPriceModel.frontPrice *  this.tempModel.ticketNo;
            this.tempModel.no = this.firstModel.firstno;
            this.firstroom(this.firstModel.firstno);
            this.shownoF();
            var b = this.familyModel.first.split(',');
    //        console.log('b', b);
            var c = b.map(Number);
      //      console.log('c', c);
            this.tempModel.numbersSelected = c;
        //    console.log(_event.target.value);
            // this.addLotteryNo();
            this.billModel.push({
              "gherno": +this.evenModel.value.value,
              "values": [this.tempModel]
            });
            this.isDisabled=false;
          //  console.log('billmodel', this.billModel);
            this.familyModel.first = "";
            this.firstModelno = [];
            this.refreshTempModel();
          }
    
       
      } else {
        this.alertService.swalError('Select Your Customer Choosable!');
      }
    }
   
   
    this.gettotal();
  }
  }


  //----------------------------------------Front Number End -------------------------------
//-----------------------------------------Front Numbers Original  ---------------------------
  // first(_event: any) {
  //   this.gettotal();
  //   let flag = 0;
  //   this.firstModel.ticket = _event.target.value;
  //   if (this.evenModel.value != -1) {
  //     for (let element of this.billModel) {
  //       if (this.evenModel.value.value == element.gherno) {

  //         for (let element1 of element.values) {
  //           for (let element2 of element.values) {
  //             if (element1.type == 'front') {
  //               if (element1.no == this.firstModel.firstno) {
  //                 element2.ticketNo = this.firstModel.ticket;
  //                 element2.totalticketAmt= this.getPriceModel.frontPrice * element2.ticketNo;

  //                 flag = 1;
  //               }
  //             }
  //           }

  //         }
  //       }


  //     }
  //     if (flag != 1) {
  //       if (this.ctrlModel.number == true) {
          
  //         for (let ctr of this.gherarray) {
  //           if (ctr.status == 1) {
  //             this.tempModel.ticketNo = _event.target.value;
  //             this.tempModel.type = 'front';
  //             this.tempModel.typeid = 7;
  //             this.tempModel.totalticketAmt = this.getPriceModel.frontPrice *  this.tempModel.ticketNo;
  //             this.tempModel.no = this.firstModel.firstno;
  //             this.firstroom(this.firstModel.firstno);
  //             this.shownoF();
  //             var b = this.familyModel.first.split(',');
  //             console.log('b', b);
  //             var c = b.map(Number);
  //             console.log('c', c);
  //             this.tempModel.numbersSelected = c;
  //             console.log(_event.target.value);
  //             // this.addLotteryNo();
  //             this.billModel.push({
  //               "gherno": +ctr.gherno,
  //               "values": [this.tempModel]
  //             });
  //             console.log('billmodel', this.billModel);
  //             this.isDisabled=false;
  //             this.familyModel.first = "";
  //             this.firstModelno = [];
  //             // this.refreshTempModel();
  //           }

  //         }
  //         this.refreshTempModel();
  //         this.gherarray = [];
  //         this.ctrlModel.number = false;
  //         console.log(this.billModel);
  //         this.selectedNumber(this.evenModel.value);
  //       } else {
  //         this.tempModel.ticketNo = _event.target.value;
  //         this.tempModel.type = 'front';
  //         this.tempModel.typeid = 7;
  //         this.tempModel.totalticketAmt =this.getPriceModel.frontPrice *  this.tempModel.ticketNo;
  //         this.tempModel.no = this.firstModel.firstno;
  //         this.firstroom(this.firstModel.firstno);
  //         this.shownoF();
  //         var b = this.familyModel.first.split(',');
  //         console.log('b', b);
  //         var c = b.map(Number);
  //         console.log('c', c);
  //         this.tempModel.numbersSelected = c;
  //         console.log(_event.target.value);
  //         // this.addLotteryNo();
  //         this.billModel.push({
  //           "gherno": +this.evenModel.value.value,
  //           "values": [this.tempModel]
  //         });
  //         this.isDisabled=false;
  //         console.log('billmodel', this.billModel);
  //         this.familyModel.first = "";
  //         this.firstModelno = [];
  //         this.refreshTempModel();
  //       }


  //     }

  //   }
  //   else {
  //     this.alertService.swalError('Select Your Gher!');
  //   }
  //   this.gettotal();
  // }

  firstroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[_value][j].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'yellow';
          this.firstModelno.push({
            "value": this.modelSecond[i][j].value,
          });
        }

      }
    }
  }

  shownoF() {
   // console.log('firstModelno', this.firstModelno);
    for (let i = 0; i < this.firstModelno.length; i++) {

      if (this.firstModelno[this.firstModelno.length - 1].value == this.firstModelno[i].value) {
        this.familyModel.first += this.firstModelno[i].value;
      } else {
        this.familyModel.first += this.firstModelno[i].value + ",";
      }

    }
  //  console.log(this.familyModel.first);
  }


   //----------------------------------------Front Number End -------------------------------
  shownoB() {
  //  console.log('backModelno', this.backModelno);
    for (let i = 0; i < this.backModelno.length; i++) {

      if (this.backModelno[this.backModelno.length - 1].value == this.backModelno[i].value) {
        this.familyModel.back += this.backModelno[i].value;
      } else {
        this.familyModel.back += this.backModelno[i].value + ",";
      }

    }
 //   console.log(this.familyModel.back);
  }

  firstModelno: any = [];

  inactivefrontroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[_value][j].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'inactive';
        }

      }
    }
  }

  //-------------------------------End Front Operation---------------------------------------

  //-----------------------------Back Operation --------------------------

  backNumber(_event: any) {
    let temp: any;
    temp = _event;
    this.backModel.backno = temp;
   // console.log(this.backModel.backno);
    // if(this.ctrlModel.number == true){
    //   let temp: { name: any; status: string; value: any; };
    //   for(let gh of this.gherarray){
    //      temp = {
    //       name: gh.gherno,
    //       status: "active",
    //     value:gh.gherno
    //      }
    //   }
    //   this.evenModel.value=temp;
    // }
    // if (this.evenModel.value != -1) {
    // } else {
    //   this.alertService.swalError('Select Your Gher!');
    // }
  }
//---------------------------Back Number Copy  -----------------------------------
back(_event: any) {
  let flag = 0;
  let f: number;
  this.backModel.ticket = _event.target.value;
  if( this.backModel.ticket<=0){
    this.alertService.swalError('Application No SHould not be less than 0 !');

  }else{
  if (this.ctrlModel.number == true) {
    for (let ctr of this.gherarray) {
      if (ctr.status == 1) {
        f=0;
        for (let element of this.billModel) {
          if (this.evenModel.value.value == element.gherno) {
    
            for (let element1 of element.values) {
              for (let element2 of element.values) {
                if (element1.type == 'back') {
                  if (element1.no == this.backModel.backno) {
                    element2.ticketNo = this.backModel.ticket;
                    element2.totalticketAmt= this.getPriceModel.backPrice * element2.ticketNo;
                    f = 1;
                  }
                }
              }
    
            }
          }
    
    
        }



if(f==0){
  this.tempModel.ticketNo = _event.target.value;
  this.tempModel.type = 'back';
  this.tempModel.typeid = 8;
  this.tempModel.totalticketAmt = this.getPriceModel.backPrice *   this.tempModel.ticketNo;
  this.tempModel.no = this.backModel.backno;

  this.backroom(this.backModel.backno);
  this.shownoB();
  var b = this.familyModel.back.split(',');
//  console.log('b', b);
  var c = b.map(Number);
//  console.log('c', c);
  this.tempModel.numbersSelected = c;
//  console.log(_event.target.value);
  // this.addLotteryNo();
  this.billModel.push({
    "gherno": ctr.gherno,
    "values": [this.tempModel]
  });

  this.isDisabled=false;
  this.familyModel.back = "";
  this.backModelno = [];
}
        
      }

      // this.refreshTempModel();
    }
    this.refreshTempModel();
   // console.log(this.billModel);
    this.gettotal();
    let temp: { name: any; status: string; value: any; };
    for(let gh of this.gherarray){
       temp = {
        name: gh.gherno,
        status: "active",
      value:gh.gherno
       }
    }
    // this.evenModel.value=temp;
    this.selectedNumbergher(temp);
  }


  if (this.ctrlModel.number == false){
    if (this.evenModel.value != -1){
    for (let element of this.billModel) {
      if (this.evenModel.value.value == element.gherno) {

        for (let element1 of element.values) {
          for (let element2 of element.values) {
            if (element1.type == 'back') {
              if (element1.no == this.backModel.backno) {
                element2.ticketNo = this.backModel.ticket;
                element2.totalticketAmt= this.getPriceModel.backPrice * element2.ticketNo;
                flag = 1;
              }
            }
          }

        }
      }


    }
    if(flag!=1){
      this.tempModel.ticketNo = _event.target.value;
      this.tempModel.type = 'back';
      this.tempModel.typeid = 8;
      this.tempModel.totalticketAmt = this.getPriceModel.backPrice *   this.tempModel.ticketNo;
      this.tempModel.no = this.backModel.backno;

      this.backroom(this.backModel.backno);
      this.shownoB();
      var b = this.familyModel.back.split(',');
    //  console.log('b', b);
      var c = b.map(Number);
    //  console.log('c', c);
      this.tempModel.numbersSelected = c;
    ///  console.log(_event.target.value);
      // this.addLotteryNo();
      this.billModel.push({
        "gherno": +this.evenModel.value.value,
        "values": [this.tempModel]
      });

      this.isDisabled=false;
      this.familyModel.back = "";
      this.backModelno = [];
      this.refreshTempModel();
    }
  }else {
    this.alertService.swalError('Select Your Customer Choosable!');
  }
}
  this.gettotal();
}
}

//-------------------------------Back Number End ----------------------------------

// -------------------------------Back Number Original ---------------------------
  // back(_event: any) {
  //   let flag = 0;
  //   this.backModel.ticket = _event.target.value;
  //   if (this.evenModel.value != -1) {
  //     for (let element of this.billModel) {
  //       if (this.evenModel.value.value == element.gherno) {

  //         for (let element1 of element.values) {
  //           for (let element2 of element.values) {
  //             if (element1.type == 'back') {
  //               if (element1.no == this.backModel.backno) {
  //                 element2.ticketNo = this.backModel.ticket;
  //                 element2.totalticketAmt= this.getPriceModel.backPrice * element2.ticketNo;
  //                 flag = 1;
  //               }
  //             }
  //           }

  //         }
  //       }


  //     }
  //     if (flag != 1) {
  //       if (this.ctrlModel.number == true) {
  //         for (let ctr of this.gherarray) {
  //           if (ctr.status == 1) {
  //             this.tempModel.ticketNo = _event.target.value;
  //             this.tempModel.type = 'back';
  //             this.tempModel.typeid = 8;
  //             this.tempModel.totalticketAmt = this.getPriceModel.backPrice *   this.tempModel.ticketNo;
  //             this.tempModel.no = this.backModel.backno;

  //             this.backroom(this.backModel.backno);
  //             this.shownoB();
  //             var b = this.familyModel.back.split(',');
  //             console.log('b', b);
  //             var c = b.map(Number);
  //             console.log('c', c);
  //             this.tempModel.numbersSelected = c;
  //             console.log(_event.target.value);
  //             // this.addLotteryNo();
  //             this.billModel.push({
  //               "gherno": ctr.gherno,
  //               "values": [this.tempModel]
  //             });

  //             this.isDisabled=false;
  //             this.familyModel.back = "";
  //             this.backModelno = [];
  //           }

  //           // this.refreshTempModel();
  //         }
  //         this.refreshTempModel();
  //         this.gherarray = [];
  //         this.ctrlModel.number = false;
  //         console.log(this.billModel);
  //         this.selectedNumber(this.evenModel.value);
  //       }
  //       else {
  //         this.tempModel.ticketNo = _event.target.value;
  //         this.tempModel.type = 'back';
  //         this.tempModel.typeid = 8;
  //         this.tempModel.totalticketAmt = this.getPriceModel.backPrice *   this.tempModel.ticketNo;
  //         this.tempModel.no = this.backModel.backno;

  //         this.backroom(this.backModel.backno);
  //         this.shownoB();
  //         var b = this.familyModel.back.split(',');
  //         console.log('b', b);
  //         var c = b.map(Number);
  //         console.log('c', c);
  //         this.tempModel.numbersSelected = c;
  //         console.log(_event.target.value);
  //         // this.addLotteryNo();
  //         this.billModel.push({
  //           "gherno": +this.evenModel.value.value,
  //           "values": [this.tempModel]
  //         });

  //         this.isDisabled=false;
  //         this.familyModel.back = "";
  //         this.backModelno = [];
  //         this.refreshTempModel();
  //       }

  //     }
  //     this.gettotal();
  //   }
  //   else {
  //     this.alertService.swalError('Select Your Gher!');
  //   }
  //   this.gettotal();
  // }

  backroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][_value].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'yellow';
          this.backModelno.push({
            "value": this.modelSecond[i][j].value,
          });
        }

      }
    }
  }


  // ----------------------------------Back Number ENd -----------------------------
  backModelno: any = [];

  inactivebackroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][_value].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'inactive';
        }

      }
    }
  }

  //------------ Multi Gher-----------------------
  multighrclone() {
    for (let i = 0; i < this.billModel.length; i++) {
      let flag = 0;
      if (this.multModel.gherno == this.billModel[i].gherno) {
        this.multigherclonestore.push({
          "data": this.billModel[i],
        });
      }
    }
  // console.log('clone', this.multigherclonestore);
  }

  addclonedata() {
    this.multModel.flag = 0;
    this.gettotal();
    for (let i = 0; i < this.multigherstore.length; i++) {
      for (let j = 0; j < this.multigherclonestore.length; j++) {
        if (this.multigherclonestore[j].data.type = "even") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Even";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "odd") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Odd";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "front") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Front" + "(" + this.multigherstore[i].backno + ")";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "back") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Back" + "(" + this.multigherstore[i].backno + ")";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        }
        else if (this.multigherclonestore[j].data.type = "family") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "F" + "(" + this.multigherstore[i].roomno + ")" + ":-" + this.multigherstore[i].fno;
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "single") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "S" + "(" + this.multigherstore[i].roomno + ")";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        }

      }
    }
    this.gettotal();

  }


  //---------------------------------------------

  //---------------------------End Back Operation-------------------------

  show() {
  //  console.log(this.evenModel.value);
  }

  //  public showNotification(type: string, message: string): void {
  //   this.notifier.notify(type, message);
  // }
  // public hideAllNotifications(): void {
  //   this.notifier.hideAll();
  // }


  // selectedNumber(number: { value: any; }){
  //   for(let element of this.modelFirst){
  //     for(let element1 of element){
  //       if (element1.value == number.value){
  //         element1.status = 'active';
  //         this.evenModel.value =number.value;
  //         this.showModel.value =this.evenModel.value;
  //         this.show();
  //         this.inactive();
  //         this.inactiveOdd();
  //         this.showModel.temp = "";
  //         this.evenModel.ticket = "";

  //       }
  //       else{

  //       }
  //     }
  //   }
  //   for(let i=0;i<this.modelFirst.length;i++)
  //     {
  //       for(let j=0;j<this.modelFirst[i].length;j++){

  //         for(let k=0;k<this.billModel.length;k++){
  //           if(this.modelFirst[i][j].value==this.billModel[k].gherno)
  //           {
  //             this.modelFirst[i][j].status="gray";
  //           }
  //         }
  //       }
  //     }
  //     this.createSecondModel();
  //   if(this.multModel.flag==1){
  //     this.multigherstore.push( {
  //       "gher": this.evenModel.value,
  //     });
  //     console.log(this.multigherstore);
  //     for(let i=0;i<this.modelFirst.length;i++)
  //     {
  //       for(let j=0;j<this.modelFirst[i].length;j++){
  //         if(this.evenModel.value==this.modelFirst[i][j].value)
  //         {
  //           this.modelFirst[i][j].status = 'gray';
  //         }
  //       }
  //     }

  //     console.log('multiclobe',this.multigherclonestore);

  //   }

  //   for(let i=0;i<this.modelFirst.length;i++)
  //   {
  //     for(let j=0;j<this.modelFirst[i].length;j++){
  //       if(this.modelFirst[i][j].status=="gray"){

  //       }
  //       else{
  //         if(this.evenModel.value==this.modelFirst[i][j].value)
  //         {
  //           continue;
  //         }
  //         else{
  //           this.modelFirst[i][j].status = 'inactive';
  //         }

  //       }

  //     }
  //   }
  // }


  oddticketNumber(event: any) {
    if (this.evenModel.value != -1) {
      this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Odd";
      this.evenModel.oddticketno = event.target.value;
      this.evenModel.ticketno = event.target.value;
      this.evenModel.totalticketAmt = this.evenModel.ticketPrice * this.evenModel.ticketno;
      this.evenModel.type = "odd";
      this.OddShow();


      this.gherchangeSingleColor(this.evenModel.value);
    }
    else {
      this.evenModel.oddticketno = 0;
      this.alertService.swalError('Select Your Customer Choosable First!');

    }

  }

  leftticketNumber(event: any) {
    if (this.evenModel.value != -1) {
      this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Left";
      this.evenModel.leftticketno = event.target.value;
      this.evenModel.ticketno = this.evenModel.leftticketno;
      this.evenModel.totalticketAmt = this.evenModel.leftticketPrice * this.evenModel.ticketno;
      this.double();
      //this.addLotteryNo();
    }
    else {
      this.evenModel.ticketno = 0;
      this.evenModel.leftticketno = 0;
      this.alertService.swalError('Select Your Customer choosable First!');

    }


  }

  rightticketNumber(event: any) {
    if (this.evenModel.value != -1) {
      this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Right";
      this.evenModel.rightticketno = event.target.value;
      this.evenModel.ticketno = this.evenModel.rightticketno;
      this.evenModel.totalticketAmt = this.evenModel.leftticketPrice * this.evenModel.ticketno;
      this.right();
      //this.addLotteryNo();
    }
    else {
      this.evenModel.ticketno = 0;
      this.evenModel.leftticketno = 0;
      this.alertService.swalError('Select Your Customer Choosable First!');

    }


  }

  totalTicketAmount() {

    this.showModel.totalAmount = this.showModel.totalAmount + this.evenModel.totalticketAmt;
  }

  createFirstModel() {
    let tempArray = [];
    for (var i = 0; i < 100; i++) {
      if (i <= 9) {
        let temp: string;
        tempArray.push({
          "value": i,
          "name": '0'+i,
          "status": "inactive"
        });
      }
      else {
        tempArray.push({
          "value": i,
          "name": i,
          "status": "inactive"
        });
      }
      if ((i + 1) % 10 == 0) {
        this.modelFirst.push(tempArray);
        tempArray = [];
      }
    }

  }

  //------------------Single---------------------

  gherchangeSingleColor(value: any) {
  //  console.log(value);
    for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
        if (this.modelSecond[i][j].value == value) {
          console.log(this.modelFirst[i][j].value);
          this.modelFirst[i][j].status = 'gray';

        }
      }
    }
  }

  changeSingleColor(value: any) {
  //  console.log(value);
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][j].value == value) {
       //   console.log(this.modelSecond[i][j].value);
          this.modelSecond[i][j].status = 'gray';

        }
      }
    }
  }

  SingleNumber(selectedItem: any) {
    this.siglModel.id = selectedItem;
  //  console.log('this.sigmodel.id', this.siglModel.id);
  //  console.log("Selected item Id: ", selectedItem);
    //this.billModel.splice(this.billModel.indexOf(selectedItem), 1);
  }

  singleNumberValue(_event: any) {
    let flag = 0;
    this.siglModel.value = _event.target.value;
    for (let i = 0; i < this.billModel.length; i++) {
      let temp = this.showModel.value + this.siglModel.id + " ";
      if (temp == this.billModel[i].gher) {
        this.billModel[i].ticketno = this.siglModel.value;
        this.billModel[i].totalticketAmt = this.siglModel.price * this.siglModel.value;
        flag = 1;
      }
    }
    if (this.showModel.value != 0) {
      if (flag != 1) {
        this.siglModel.value = _event.target.value;
     //   console.log(this.siglModel.value);
        this.evenModel.ticketno = this.siglModel.value;
        this.evenModel.totalticketAmt = this.siglModel.price * this.evenModel.ticketno;
        this.showModel.temp = this.showModel.value + this.siglModel.id + " ";
        this.evenModel.type = "single";
        this.evenModel.roomno = this.siglModel.id;
        // this.totalTicketAmount();
        this.addLotteryNo();
        this.changeSingleColor(this.siglModel.id);
        this.gherchangeSingleColor(this.showModel.value);

      }

    }
    else {
      this.alertService.swalError('Select Your Customer Choosable!');
    }

  }


  //--------------------End Single--------------------------
  //-------------------front ---------------------
  front() {
    // let tempArray =[];
    for (var i = 0; i < 10; i++) {

      this.frontNumber.push({
        "value": i,
        "name": i,

      });

    }

    

  }

  fst(){
    for (var i = 0; i < 10; i++) {
      this.Front.push({
        "value": i,
        "name": i,

      });

    }
  }
  time() {
    // let tempArray =[];
    let hr = 10;
    for (var i = 0; i < 11; i++) {
      let k = 0;

      let min = 15;
      for (let j = 0; j < 4; j++) {
        if (j == 3) {

          let time = (hr + 1) + ":" + "00";
          this.timeTable.push({
            "value": time,
            "id": k++,
            "status": 'inactive',
          });
          min = min + 15;
        }
        else {
          let time = hr + ":" + min;
          this.timeTable.push({
            "value": time,
            "id": k++,
            "status": 'inactive',
          });
          min = min + 15;
        }

      }
      hr = hr + 1;
    }

  }

  
 

  //---------------------------------------------

  // Family -------------------************FAMILY*************----------------------------
  showno() {
  //  console.log('setmodel1', this.setmodel1);
    for (let i = 0; i < this.setmodel1.length; i++) {

      if (this.setmodel1[this.setmodel1.length - 1].value == this.setmodel1[i].value) {
        this.familyModel.setvar += this.setmodel1[i].value;
      } else {
        this.familyModel.setvar += this.setmodel1[i].value + ",";
      }

    }
  //  console.log(this.familyModel.setvar);
  }

  FamilyticketNumber(_event: any) {

    // this.evenModel.ticketno = this.familyModel.ticketno;
    // this.evenModel.totalticketAmt = this.evenModel.familyticketPrice * this.evenModel.ticketno;
    // this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "F" + "(" + this.showModel.room + ")" + ":-" + this.familyModel.setvar;
    // this.evenModel.type = "family";
    // this.evenModel.roomno = this.showModel.room;
    // this.evenModel.fno = this.familyModel.setvar;
    // this.totalTicketAmount();
   // this.FamilysingleNumber();
    this.showno();
    // this.addLotteryNo();

  }

  changeColor(value: any) {
  //  console.log(value);
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][j].value == value) {
          console.log(this.modelSecond[i][j].value);
          this.modelSecond[i][j].status = 'yellow';

        }
      }
    }
  }

  // declare function overload(param: string);
  // FamilysingleNumber() {
  //   let flag = 0;
  //   if (this.evenModel.value != -1) {
  //     if (this.familyModel.value != -1) {
  //       if (this.familyModel.value <= 9) {
  //         this.showset(0);
  //       }
  //       console.log('combination', this.comb);
  //       for (let i = 0; i < this.comb.set.length; i++) {
  //         this.len = this.comb.set[i];
  //         let len1 = this.comb.set[i];
  //         console.log('len', this.len);
  //         console.log('len1', len1);
  //       }
  //       for (let j in this.len) {
  //         let temp = Object.values(this.len)[j];
  //         if (temp == this.familyModel.value) {
  //           while (this.familyModel.value > 0) {
  //             this.familyModel.single = this.familyModel.value % 10;
  //             this.familyModel.value = Math.floor(this.familyModel.value / 10);
  //             console.log(this.familyModel.single);
  //             this.showset(this.familyModel.single);
  //             flag = 1;
  //             this.familyModel.value = 0;
  //             break;

  //           }
  //         }
  //         console.log('pair', j, temp);
  //       }
  //       if (flag != 1) {

  //         while (this.familyModel.value > 0) {
  //           this.familyModel.single = this.familyModel.value % 10;
  //           this.familyModel.value = Math.floor(this.familyModel.value / 10);
  //           console.log(this.familyModel.single);
  //           this.showset(this.familyModel.single);
  //         }
  //       }

  //       this.show1();
  //     } else {
  //       alert('select your number first');
  //     }


  //   }
  //   else {
  //     this.alertService.swalError('Select Your Gher!');
  //   }
  // }

  setmodel1: any = [];

  pushdata(value1: any) {
    this.setmodel1.push({
      "value": value1,
    });
  }

  show1() {
  //  console.log('can you see?');
  //  console.log(this.setmodel);
    for (let i = 0; i < this.setmodel.length; i++) {
    //  console.log('--family---');
      let len: any;
      len = this.setmodel[i].set;
    //  console.log(len);
      //console.log(len.length);
    ///  console.log('i:-', i);
      for (let j in len) {
       // console.log(Object.keys(len)[j], Object.values(len)[j]);
        this.changeModel.change = Object.values(len)[j];
       // console.log('change color', this.changeModel.change);
        this.changeColor(this.changeModel.change);
        this.pushdata(this.changeModel.change);
      }
    }
    this.setmodel = [];

  }

  showset(_id: number) {
  //  console.log('showset');
    for (let i = 0; i < this.array.set.length; i++) {
      if (this.array.set[i].id == _id) {
        for (let j = 0; j < this.array.set[i].setlist.length; j++) {
    //      console.log('show one');
      //    console.log(this.array.set[i].setlist[j]);
          this.setmodel.push({
            "set": this.array.set[i].setlist[j],
          });

        }
      }
    }

  //  console.log(this.array);
  }


  setmodel: any = []


  // timetable = {
  //  "time" : [
  //    {
  //     0 : "10:15",
  //    }

  //  ]
  // }

  comb = {
    "set": [
      {
        0: "00",
        1: 11,
        2: 22,
        3: 33,
        4: 44,
        5: 66,
        7: 77,
        8: 88,
        9: 99,
        10: "05",
        11: 50,
        12: 16,
        13: 61,
        14: 27,
        15: 72,
        16: 38,
        17: 83,
        18: 49,
        19: 94,

      }
    ]
  }


  array : any = [
    {
      "value": 0,
      "count" :4,
      "numbersSelected": [
        "0",
        "5",
        "50",
        "55"
      ]
    },
    {
      "value": 1,
      "count" :8,
      "numbersSelected": [
        "1",
        "10",
        "15",
        "51",
        "6",
        "60",
        "65",
        "56",
      ]
    },
    {
      "value": 2,
      "count" :8,
      "numbersSelected": [
        "2",
        "20",
        "25",
        "52",
        "57",
        "75",
        "7",
        "70"
      ]
    },
    {
      "value": 3,
      "count" :8,
      "numbersSelected": [
        "3",
        "30",
        "35",
        "53",
        "58",
        "85",
        "08",
        "80",
      ]
    },
    {
      "value": 4,
      "count" :8,
      "numbersSelected": [
        "4",
        "40",
        "45",
        "54",
        "59",
        "95",
        "09",
        "90",
      ]
    },
    {
      "value": 5,
      "count" :4,
      "numbersSelected": [
        "0",
        "05",
        "50",
        "55",
      ]
    },
    {
      "value": 6,
      "count" :8,
      "numbersSelected": [
        "6",
        "60",
        "65",
        "56",
        "15",
        "51",
        "10",
        "1",
      ]
    },
    {
      "value": 7,
      "count" :8,
      "numbersSelected": [
        "7",
        "70",
        "75",
        "57",
         "25",
         "52",
         "20",
         "2"
      ]
    },
    {
      "value": 8,
      "count" :8,
      "numbersSelected": [
        "8",
        "80",
        "85",
        "58",
         "35",
         "53",
         "30",
         "3"
      ]
    },
    {
      "value": 9,
      "count" :8,
      "numbersSelected": [
         "9",
        "90",
        "95",
        "59",
         "45",
         "54",
         "40",
         "4"
      ]
    },{
      "value": 10,
      "count" :8,
      "numbersSelected": [
        "10",
        "1",
        "15",
        "51",
         "6",
         "60",
         "56",
         "65"
      ]
    },{
      "value": 11,
      "count" :4,
      "numbersSelected": [
        "11",
        "66",
        "16",
        "61",
      ]
    },{
      "value": 12,
      "count" :8,
      "numbersSelected": [
        "12",
        "21",
        "17",
        "71",
         "76",
         "67",
         "26",
         "62"
      ]
    },{
      "value": 13,
      "count" :8,
      "numbersSelected": [
        "13",
        "31",
        "18",
        "81",
         "86",
         "68",
         "63",
         "36"
      ]
    },{
      "value": 14,
      "count" :8,
      "numbersSelected": [
        "14",
        "41",
        "46",
        "64",
         "96",
         "69",
         "91",
         "19"
      ]
    },{
      "value": 15,
      "count" :8, // check
      "numbersSelected": [
        "15",
        "1",
        "10",
        "51",
        "6",
         "60",
         "56",
         "65",
         
      ]
    },{
      "value": 16,
      "count" :4,
      "numbersSelected": [
        "16",
        "11",
        "66",
        "61",
      ]
    },{
      "value": 17,
      "count" :8,
      "numbersSelected": [
        "17",
        "12",
        "21",
        "71",
         "76",
         "67",
         "26",
         "62"
      ]
    },{
      "value": 18,
      "count" :8,
      "numbersSelected": [
        "18",
        "81",
        "13",
        "31",
         "86",
         "68",
         "63",
         "36"
      ]
    },{
      "value": 19,
      "count" :8,
      "numbersSelected": [
        "19",
        "91",
        "14",
        "41",
         "46",
         "64",
         "96",
         "69"
      ]
    },{
      "value": 20,
      "count" :8,
      "numbersSelected": [
        "20",
        "2",
        "25",
        "52",
         "57",
         "75",
         "7",
         "70"
      ]
    },{
      "value": 21,
      "count" :8,
      "numbersSelected": [
        "21",
        "12",
        "17",
        "71",
         "76",
         "26",
         "62",
         "67"
      ]
    },{
      "value": 22,
      "count" :4,
      "numbersSelected": [
        "22",
        "27",
        "72",
        "77",
      ]
    },{
      "value": 23,
      "count" :8,
      "numbersSelected": [
        "23",
        "32",
        "37",
        "73",
         "78",
         "87",
         "28",
         "82"
      ]
    },{
      "value": 24,
      "count" :8,
      "numbersSelected": [
        "24",
        "42",
        "47",
        "74",
         "92",
         "29",
         "97",
         "79"
      ]
    },{
      "value": 25,
      "count" :8,
      "numbersSelected": [
        "25",
        "52",
        "20",
        "2",
         "57",
         "75",
         "7",
         "70"
      ]
    },{
      "value": 26,
      "count" :8,
      "numbersSelected": [
        "26",
        "62",
        "17",
        "71",
         "12",
         "21",
         "76",
         "67"
      ]
    },{
      "value": 27,
      "count" :4,
      "numbersSelected": [
        "27",
        "72",
        "22",
        "77",
      ]
    },{
      "value": 28,
      "count" :8,
      "numbersSelected": [
        "28",
        "82",
        "23",
        "32",
         "37",
         "73",
         "78",
         "87"
      ]
    },{
      "value": 29,
      "count" :8,
      "numbersSelected": [
        "29",
        "92",
        "24",
        "42",
         "47",
         "74",
         "97",
         "79"
      ]
    },{
      "value": 30,
      "count" :8,
      "numbersSelected": [
        "30",
        "3",
        "35",
        "53",
         "58",
         "85",
         "8",
         "80"
      ]
    },{
      "value": 31,
      "count" :8,
      "numbersSelected": [
        "31",
        "13",
        "18",
        "81",
         "86",
         "68",
         "63",
         "36"
      ]
    },{
      "value": 32,
      "count" :8,
      "numbersSelected": [
        "32",
        "23",
        "37",
        "73",
         "78",
         "87",
         "28",
         "82"
      ]
    },{
      "value": 33,
      "count" :4,
      "numbersSelected": [
        "33",
        "88",
        "83",
        "38",
      ]
    },{
      "value": 34,
      "count" :8,
      "numbersSelected": [
        "34",
        "43",
        "84",
        "48",
         "93",
         "98",
         "89",
         "39"
      ]
    },{
      "value": 35,
      "count" :8,
      "numbersSelected": [
        "35",
        "30",
        "3",
        "53",
         "58",
         "85",
         "8",
         "80"
      ]
    },{
      "value": 36,
      "count" :8,
      "numbersSelected": [
        "36",
        "63",
        "13",
        "31",
        "81",
         "18",
         "86",
         "68"
      ]
    },{
      "value": 37,
      "count" :8,
      "numbersSelected": [
        "37",
        "73",
        "32",
        "23",
         "78",
         "87",
         "28",
         "82"
      ]
    },{
      "value": 38,
      "count" :4,
      "numbersSelected": [
        "38",
        "83",
        "33",
        "88",
      ]
    },{
      "value": 39,
      "count" :8,
      "numbersSelected": [
        "39",
        "93",
        "34",
        "43",
         "84",
         "48",
         "98",
         "89"
      ]
    },{
      "value": 40,
      "count" :8,
      "numbersSelected": [
        "40",
        "4",
        "45",
        "54",
         "59",
         "95",
         "9",
         "90"
      ]
    },{
      "value": 41,
      "count" :8,
      "numbersSelected": [
        "41",
        "14",
        "46",
        "64",
         "96",
         "91",
         "19",
         "69"
      ]
    },{
      "value": 42,
      "count" :8,
      "numbersSelected": [
        "42",
        "24",
        "29",
        "92",
         "47",
         "74",
         "97",
         "79"
      ]
    },{
      "value": 43,
      "count" :8,
      "numbersSelected": [
        "43",
        "34",
        "39",
        "93",
         "84",
         "48",
         "89",
         "98"
      ]
    },{
      "value": 44,
      "count" :4,
      "numbersSelected": [
        "49",
        "94",
        "99",
        "44",
       
      ]
    },{
      "value": 45,
      "count" :8,
      "numbersSelected": [
        "45",
        "54",
        "40",
        "0",
         "59",
         "95",
         "9",
         "90"
      ]
    },{
      "value": 46,
      "count" :8,
      "numbersSelected": [
        "46",
        "64",
        "41",
        "14",
         "96",
         "69",
         "91",
         "19"
      ]
    },{
      "value": 47,
      "count" :8,
      "numbersSelected": [
        "47",
        "74",
        "42",
        "24",
         "29",
         "92",
         "97",
         "79"
      ]
    },{
      "value": 48,
      "count" :8,
      "numbersSelected": [
        "48",
        "84",
        "43",
        "34",
         "39",
         "93",
         "98",
         "89"
      ]
    },{
      "value": 49,
      "count" :4,
      "numbersSelected": [
        "49",
        "94",
        "44",
        "99",
      ]
    },
    {
      "value": 50,
      "count" :4,
      "numbersSelected": [
        "50",
        "5",
        "55",
        "0",
      ]
    }, {
      "value": 51,
      "count" :8,
      "numbersSelected": [
        "51",
        "15",
        "1",
        "15",
        "6",
        "60",
        "56",
        "65",
      ]
    },
    {
      "value": 52,
      "count" :8,
      "numbersSelected": [
        "52",
        "25",
        "20",
        "2",
        "57",
        "75",
        "7",
        "70",
      ]
    },
    {
      "value": 53,
      "count" :8,
      "numbersSelected": [
        "53",
        "35",
        "30",
        "0",
        "58",
        "85",
        "8",
        "80",
      ]
    },
    {
      "value": 54,
      "count" :8,
      "numbersSelected": [
        "54",
        "45",
        "40",
        "4",
        "59",
        "95",
        "9",
        "90",
      ]
    },
    {
      "value": 55,
      "count" :4,
      "numbersSelected": [
        "55",
        "50",
        "5",
        "0",
      
      ]
    },
    {
      "value": 56,
      "count" :8,
      "numbersSelected": [
        "56",
        "65",
        "51",
        "15",
        "1",
        "6",
        "10",
        "60",
      ]
    },
    {
      "value": 57,
      "count" :8,
      "numbersSelected": [
        "57",
        "75",
        "52",
        "25",
        "20",
        "2",
        "7",
        "70",
      ]
    },
    {
      "value": 58,
      "count" :8,
      "numbersSelected": [
        "58",
        "85",
        "3",
        "30",
        "53",
        "35",
        "80",
        "8",
      ]
    },
    {
      "value": 59,
      "count" :8,
      "numbersSelected": [
        "59",
        "54",
        "45",
        "40",
        "4",
        "95",
        "9",
        "90"
      ]
    },
    {
      "value": 60,
      "count" :8,
      "numbersSelected": [
        "60",
        "6",
        "15",
        "51",
        "65",
        "56",
        "1",
        "10"
      ]
    },
    {
      "value": 60,
      "count" :8,
      "numbersSelected": [
        "60",
        "56",
        "51",
        "15",
        "1",
        "15",
        "6",
        "65"
      ]
    },
    {
      "value":61,
      "count" :4,
      "numbersSelected": [
        "61",
        "16",
        "11",
        "66"
      ]
    },
    {
      "value": 62,
      "count" :8,
      "numbersSelected": [
        "62",
        "26",
        "17",
        "12",
        "21",
        "71",
        "76",
        "67"
      ]
    },
    {
      "value": 63,
      "count" :8,
      "numbersSelected": [
        "63",
        "36",
        "31",
        "13",
        "18",
        "81",
        "86",
        "68"
      ]
    },
    {
      "value": 64,
      "count" :8,
      "numbersSelected": [
        "64",
        "46",
        "41",
        "14",
        "96",
        "69",
        "91",
        "19"
      ]
    },
    {
      "value": 65,
      "count" :8,
      "numbersSelected": [
        "65",
        "60",
        "56",
        "51",
        "15",
        "1",
        "10",
        "6"
      ]
    },
    {
      "value": 66,
      "count" :4,
      "numbersSelected": [
        "66",
        "61",
        "16",
        "11"
      ]
    },
    {
      "value": 67,
      "count" :8,
      "numbersSelected": [
        "67",
        "62",
        "26",
        "17",
        "12",
        "21",
        "71",
        "76"
      ]
    },
    {
      "value": 68,
      "count" :8,
      "numbersSelected": [
        "68",
        "63",
        "36",
        "31",
        "13",
        "18",
        "81",
        "86"
      ]
    },
    {
      "value": 69,
      "count" :8,
      "numbersSelected": [
        "69",
        "64",
        "46",
        "41",
        "14",
        "96",
        "91",
        "19"
      ]
    },
    {
      "value": 70,
      "count" :8,
      "numbersSelected": [
        "70",
        "7",
        "75",
        "57",
        "25",
        "52",
        "20",
        "2"
      ]
    },
    {
      
      "value": 71,
      "count" :8,
      "numbersSelected": [
        "71",
        "17",
        "76",
        "67",
        "21",
        "12",
        "62",
        "26",
      ]
    }, {
      "value": 72,
      "count" :8,
      "numbersSelected": [
        "72",
        "27",
        "77",
        "22",
      ]
    }, {
      "value": 73,
      "count" :8,
      "numbersSelected": [
        "73",
        "37",
        "28",
        "82",
        "78",
        "87",
        "32",
        "23",
      ]
    }, {
      "value": 74,
      "count" :8,
      "numbersSelected": [
        "74",
        "47",
        "29",
        "92",
        "79",
        "97",
        "24",
        "42"
      ]
    }, {
      "value": 75,
      "count" :8,
      "numbersSelected": [
        "75",
        "57",
        "20",
        "2",
        "70",
        "7",
        "25",
        "52"
      ]
    }, {
      "value": 76,
      "count" :8,
      "numbersSelected": [
        "76",
        "67",
        "12",
        "21",
        "71",
        "17",
        "26",
        "62",
      ]
    },
    {
      "value": 77,
      "count" :4,
      "numbersSelected": [
        "77",
        "22",
        "27",
        "72",
      ]
    },
    {
      "value": 78,
      "count" :8,
      "numbersSelected": [
        "78",
        "87",
        "23",
        "32",
        "73",
        "37",
        "82",
        "28",
      ]
    },
    {
      "value": 79,
      "count" :8,
      "numbersSelected": [
        "79",
        "97",
        "24",
        "42",
        "74",
        "47",
        "29",
        "92",
      ]
    },
    {
      "value": 80,
      "count" :8,
      "numbersSelected": [
        "80",
        "8",
        "35",
        "53",
        "85",
        "58",
        "30",
        "3",
      ]
    },
    {
      "value": 81,
      "count" :8,
      "numbersSelected": [
        "81",
        "18",
        "36",
        "36",
        "86",
        "68",
        "31",
        "13",
      ]
    },
    {
      "value": 82,
      "count" :8,
      "numbersSelected": [
        "82",
        "28",
        "37",
        "73",
        "23",
        "32",
        "87",
        "78",
      ]
    },
    {
      "value": 83,
      "count" :4,
      "numbersSelected": [
        "88",
        "33",
        "38",
        "83",
       
      ]
    },
    {
      "value": 84,
      "count" :8,
      "numbersSelected": [
        "84",
        "48",
        "39",
        "93",
        "89",
        "98",
        "43",
        "34",
      ]
    },
    {
      "value": 85,
      "count" :8,
      "numbersSelected": [
        "85",
        "58",
        "30",
        "3",
        "35",
        "53",
        "80",
        "8",
      ]
    },
    {
      "value": 86,
      "count" :8,
      "numbersSelected": [
        "86",
        "68",
        "31",
        "13",
        "18",
        "81",
        "63",
        "36",
      ]
    },
    {
      "value": 87,
      "count" :8,
      "numbersSelected": [
        "87",
        "78",
        "32",
        "23",
        "83",
        "28",
        "73",
        "37",
      ]
    },
    {
      "value": 88,
      "count" :4,
      "numbersSelected": [
        "88",
        "33",
        "38",
        "83",
      ]
    },
    {
      "value": 89,
      "count" :8,
      "numbersSelected": [
        "89",
        "98",
        "34",
        "43",
        "84",
        "48",
        "39",
        "93",
      ]
    },
    {
      "value": 90,
      "count" :8,
      "numbersSelected": [
        "90",
        "9",
        "45",
        "54",
        "40",
        "4",
        "95",
        "59",
      ]
    },
    {
      "value": 91,
      "count" :8,
      "numbersSelected": [
        "91",
        "19",
        "46",
        "64",
        "96",
        "69",
        "41",
        "14",
      ]
    },
    {
      "value": 92,
      "count" :8,
      "numbersSelected": [
        "92",
        "29",
        "47",
        "74",
        "97",
        "79",
        "42",
        "24",
      ]
    },
    {
      "value": 93,
      "count" :8,
      "numbersSelected": [
        "93",
        "39",
        "48",
        "84",
        "98",
        "89",
        "34",
        "43",
      ]
    },
    {
      "value": 94,
      "count" :4,
      "numbersSelected": [
        "94",
        "49",
        "44",
        "99",
      ]
    },
    {
      "value": 95,
      "count" :8,
      "numbersSelected": [
        "95",
        "59",
        "40",
        "4",
        "90",
        "9",
        "45",
        "54",
      ]
    },
    {
      "value": 96,
      "count" :8,
      "numbersSelected": [
        "96",
        "69",
        "14",
        "41",
        "19",
        "91",
        "46",
        "64",
      ]
    },
    {
      "value": 97,
      "count" :8,
      "numbersSelected": [
        "97",
        "79",
        "42",
        "24",
        "92",
        "29",
        "47",
        "74",
      ]
    },
    {
      "value": 98,
      "count" :8,
      "numbersSelected": [
        "98",
        "89",
        "43",
        "34",
        "93",
        "39",
        "48",
        "84",
      ]
    },
    {
      "value": 99,
      "count" :4,
      "numbersSelected": [
        "99",
        "44",
        "49",
        "94",
      
      ]
    },
  ] 
    
  

  // family end --------------************************-----------------------------------

  createSecondModel() {
    this.modelSecond = [];
    let tempArray1 = [];
    for (var i = 0; i < 100; i++) {
      if (i <= 9) {
        let temp: string;
        tempArray1.push({
          "value": i,
          "name": '0' + i,
          "status": "inactive"
        });
      }
      else {
        tempArray1.push({
          "value": i,
          "name": i,
          "status": "inactive"
        });
      }
      if ((i + 1) % 10 == 0) {
        this.modelSecond.push(tempArray1);
        tempArray1 = [];
      }
    }

  }


  disablegher(_value: any) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
        if (this.gherModel.gher == this.modelFirst[i][j].value) {
          this.modelFirst[i][j].status = 'inactive';
        }

      }
    }
  }

  inactiveall() {
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        this.modelSecond[i][j].status = 'inactive';
      }
    }
    for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
        this.modelFirst[i][j].status = 'inactive';
      }
    }
  }

  inactive() {
    for (let element of  this.modelFirst) {
      element.status='inactive';
      
    }
  }

  inactiveOdd() {
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][j].value % 2 != 0) {
          this.modelSecond[i][j].status = 'inactive';
        }
      }
    }
  }

  evenShow() {
    this.flag = 0;
    if (this.evenModel.value != -1) {
      for (let i = 0; i < this.billModel.length; i++) {
        let temp = "Gher" + "(" + this.showModel.value + ")" + "Even";
        if (temp == this.billModel[i].gher) {
          this.evenModel.ticketno = 0;
          alert('you have already selected');
          this.flag = 1;
        }
      }
      if (this.flag != 1) {
        for (let i = 0; i <= this.modelSecond.length - 1; i++) {
          for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
            if (this.modelSecond[i][j].value % 2 == 0) {
         //     console.log(this.modelSecond[i][j].value);
              this.modelSecond[i][j].status = 'active';
              this.evenModel.ticket++;
              this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Even";
              this.evenModel.type = "even";
            }
          }
        }
        this.addLotteryNo();

      }
   //   console.log(this.evenModel.ticket);
    }
    else {
      this.alertService.swalError('Select Your Customer choosable!');
    }
  }

  // odd--------------------********************************************---------------------------------------------------

  OddShow() {
    this.flag = 0;
    if (this.evenModel.value != -1) {
      for (let i = 0; i < this.billModel.length; i++) {
        let temp = "Gher" + "(" + this.evenModel.value + ")" + "Odd";
        if (temp == this.billModel[i].gher) {
          this.evenModel.oddticketno = 0;
          alert('you have already selected');
          this.flag = 1;
        }
      }
      if (this.flag != 1) {
        for (let i = 0; i <= this.modelSecond.length - 1; i++) {
          for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
            if (this.modelSecond[i][j].value % 2 != 0) {
              console.log(this.modelSecond[i][j].value);
              this.modelSecond[i][j].status = 'active';
              this.evenModel.ticket++;
              this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Odd";
            }
          }
        }
        this.addLotteryNo();
      }
 //     console.log(this.evenModel.ticket);
    }
    else {
      alert('select your first box Value');
    }
  }

  //------------------------********************************************---------------------------------------------------

  //** Double  **/
  //----------------Right-----------------------

  right() {
    this.flag = 0;
    let eo = 0;
    let even = 0;
    let odd = 0;
    if (this.evenModel.value != -1) {
      let temp = "Gher" + "(" + this.evenModel.value + ")" + "Right";
      let temp1 = "Gher" + "(" + this.evenModel.value + ")" + "Odd";
      let temp2 = "Gher" + "(" + this.evenModel.value + ")" + "Even";
      for (let i = 0; i < this.billModel.length; i++) {
        if (temp == this.billModel[i].gher) {
          alert('you have already selected');
          this.evenModel.leftticketno = 0;
          this.flag = 1;
        }
        //   for(let j=0;j<this.billModel.length;j++)
        // {
        //   //odd
        //   if(temp1 == this.billModel[j].gher)
        //     {
        //       for(let k=0;k<this.billModel.length;k++)
        //     {
        //       if(temp2 == this.billModel[k].gher)
        //     {
        //       //even
        //         alert('even & odd already selected in this gher');
        //         this.flag=1;
        //         even=1;
        //         this.evenModel.ticketno=0
        //         this.evenModel.leftticketno=0;
        //     }

        //     }
        //     //function
        //     }
        //   }
        //   for(let j=0;j<this.billModel.length;j++)
        // {
        //   if(temp2 == this.billModel[j].gher)
        //     {
        //       //even
        //       for(let k=0;i<this.billModel.length;k++)
        //     {
        //       if(temp1 == this.billModel[k].gher)
        //     {
        //       //odd
        //         alert('even & odd already selected in this gher');
        //         this.flag=1;
        //         this.evenModel.ticketno=0;
        //         this.evenModel.leftticketno=0;
        //     }
        //     }
        //     }
        //   }
      }
      if (this.flag != 1) {
        let nine = 9;
        let nine1: number;
        for (let n = 1; n <= 10; n++) {
          nine1 = nine * n;

          for (let i = 0; i <= this.modelSecond.length - 1; i++) {
            for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {

              if (nine1 == this.modelSecond[i][j].value) {
             //   console.log(this.modelSecond[i][j].value);
                this.modelSecond[i][j].status = 'active';
                // this.evenModel.ticket++;
                this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Right";
                this.evenModel.type = "right"
              }
            }
          }
        }
        this.addLotteryNo();
        this.gherchangeSingleColor(this.evenModel.value);
      }

    }
    else {
      alert('select your  first box value');
    }
  }


  //--------------------------------------------
  double() {
    this.flag = 0;
    let eo = 0;
    let even = 0;
    let odd = 0;
    if (this.evenModel.value != -1) {
      let temp = "Gher" + "(" + this.evenModel.value + ")" + "Left";
      let temp1 = "Gher" + "(" + this.evenModel.value + ")" + "Odd";
      let temp2 = "Gher" + "(" + this.evenModel.value + ")" + "Even";
      for (let i = 0; i < this.billModel.length; i++) {
        if (temp == this.billModel[i].gher) {
          alert('you have already selected');
          this.evenModel.leftticketno = 0;
          this.flag = 1;
        }

      }
      if (this.flag != 1) {
        for (let i = 0; i <= this.modelSecond.length - 1; i++) {
          for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
            if (this.modelSecond[i] == this.modelSecond[j]) {
            //  console.log(this.modelSecond[i][j].value);
              this.modelSecond[i][j].status = 'active';
              // this.evenModel.ticket++;
              this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Left";
              this.evenModel.type = "left"
            }
          }
        }
        this.addLotteryNo();
        this.gherchangeSingleColor(this.evenModel.value);
      }

    }
    else {
      alert('select your  first box value');
    }
  }

  evendouble() {
    // if(even==1)
    //       {
    for (let m = 0; m <= this.modelSecond.length - 1; m++) {
      for (let n = 0; n <= this.modelSecond[m].length - 1; n++) {
        if (this.modelSecond[m][n].value % 2 != 0) {
          if (this.modelSecond[m] == this.modelSecond[n]) {
         //   console.log(this.modelSecond[m][n].value);
            this.modelSecond[m][n].status = 'change';
          }
        }
      }
    }
    // }
  }

  numberArray = [
    {
      "gher": 0,
      "room": 0,
      "value": 0,
      "family": 0
    }
  ]

  selectThisNumber(input: any) {

    this.billModel.push({
      "gherno": input,
      "values": []
    });
  }

  addLotteryNo() {
    this.billModel.push({
      "gherno": this.evenModel.value,
      "gher": this.showModel.temp,
      "ticketno": this.evenModel.ticketno,
      "totalticketAmt": this.evenModel.totalticketAmt,
      "type": this.evenModel.type,
      "backno": this.evenModel.backno,
      "fno": this.evenModel.fno,
      "roomno": this.evenModel.roomno,
    });
    this.evenModel.ticketno = 0;
    this.showModel.temp = "";
    this.evenModel.totalticketAmt = 0;
    this.IsVisible = false;
    this.totalTicketAmount();
    this.familyModel.setvar = "";
    this.setmodel1 = [];
    this.showModel.temp = "";
    this.familyModel.ticketno = 0;
    this.evenModel.oddticketno = 0;
    this.evenModel.leftticketno = 0;
    this.evenModel.rightticketno = 0;
    this.evenModel.type = "";
    //let temp = this.billModel[this.billModel-1];
    let temp = this.billModel[this.billModel.length - 1];
   // console.log('temp', temp);
    this.multModel.gherno = temp.gherno;
    this.multModel.showgherno = temp.gherno;
    this.multModel.flag = 0;
    this.gettotal();

  }

isdeletevalue = false;
truevalue(input: boolean){
  this.isdeletevalue = input;
}
  delete(_gherno: any, selectedItem: any, gh: any) {
    
 //   console.log('check_gherno',_gherno);
 //   console.log('check_gherno',selectedItem);
 //   console.log('check_gh',gh);
 //   console.log('check_gh.gherno',gh.gherno);
     let get = {
       "value" : gh.gherno,
     } 

     if( this.billModel[_gherno].values.length==0)
     {
     //   console.log('this.is empty');
     }
      let tempArray = [];
      for (let val in this.billModel[_gherno].values) {
        if (val != selectedItem) {
          tempArray.push(this.billModel[_gherno].values[val]);
        }
      }
      this.billModel[_gherno].values = tempArray;
      if( this.billModel[_gherno].values.length==0)
      {
        this.billModel.splice(_gherno, 1);
     //   console.log('this.is empty');
      }
      this.selectedNumber(get);
    // }
    // this.isdeletevalue = false;
    this.isdeletevalue = false;
    this.gettotal();
  //  console.log(this.billModel);

   // this.selectedNumber(this.evenModel.value.value);
    
  }

  minus(selectedItem: any) {
 //   console.log("Selected item Id: ", selectedItem.gher);
    // this.gettotal();
    if (selectedItem.ticketNo > 0) {
      if (selectedItem.type == "single") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice;
      }
      else if (selectedItem.type == "left") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "right") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "family") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 8;
      }
      else if (selectedItem.type == "back") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "front") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      } else if (selectedItem.type == "even") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 50;
      } else if (selectedItem.type == "odd") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 50;
      }

      this.gettotal();
    }
    else {
      alert('Balance is low');
    }
  }

  plus(selectedItem: any) {
  //  console.log("Selected item Id: ", selectedItem.gher);
    // this.gettotal();
    if (selectedItem.ticketNo > 0) {
      if (selectedItem.type == "single") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice;
      }
      else if (selectedItem.type == "left") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "right") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "family") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt +this.getPriceModel.singlePrice * 80;
      }
      else if (selectedItem.type == "back") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "front") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10
      } else if (selectedItem.type == "even") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 50;
      } else if (selectedItem.type == "odd") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 50;
      }

      this.gettotal();
    }
  }

  multigher(_value: any) {
    if (_value == true) {
    //  console.log('check mukti gher true');

    } else {
  //    console.log('check multi gher false');
      let temp = this.billModel[this.billModel.length - 1];
   //   console.log(temp);
      this.multModel.flag = 1;
      this.multighrclone();
    }
 //   console.log(this.checkboxModel.value);
  }

  // total Amount

  //
  slotid : any;
  getcurrentSlot(){
     this.checktime();
     let flag=0;
    for(let book of this.bookingslot)
    {
      
        if(this.matchtime==book.startTime)
        {
          for(let sl of this.slots){
            if(this.matchtime==sl.startTime){
flag=1;
            }
          }
          if(flag==0){
            // this.slots.push({
            //   "endTime": book.endTime,
            //   "startTime" : book.startTime,
            //    "id" : book.id,
            //    "status" : book.status,
            //    "drawTimeAm" : book.drawTimeAm,
            //    "drawTime" : book.drawTime
    
            //   })
            this.slots.push(
              book
            )
            this.slotid=book.id;
          }
         
        
      }
     
    }
    flag=0;
  //  console.log('ayushcurrent slot',this.matchtime);
  //  console.log('check currwnt slot',this.slots);
  }
  resultList: any=[];

  resultListdummy: any=[];
  getresult(){
 //   console.log('chek result')
    this.lotteryHttpService.makeRequestApi('post', 'getAllResult', {bookingDate: this.datePipe.transform(new Date(),"dd-MM-yyyy")}).subscribe(
      res => {
            this.resultListdummy=res;
            this.getcurrentSlot();
        //    console.log('getresult api slotid',this.slotid);
            let slotid=this.slotid-1;
        //    console.log('resultListdummy api',this.resultListdummy);
        this.resultList=[];
            for(let res of this.resultListdummy){
              if(res.slotMaster.id==slotid){
                this.resultList.push({
                  "gherNo":res.gherNo,
                  "id":res.id,
                  "luckyNumber":res.luckyNumber,
                  "slotid":res.slotMaster.id,
                  "slottime":res.slotMaster.drawTimeAm,
                })
              }
             
            }

           // this.resultList=this.resultListdummy;
        //   console.log('getresult api',this.resultList);
            //console.log('resultListdummy api',this.resultListdummy);
       });
  }

amount : any;

checkAmtModel : any = {
"endamt" :1,
"checkamt" : 2000,
}
  getAmount(){
 //   console.log('chek result')
    this.lotteryHttpService.makeRequestApi('post', 'getAmount', {requestBy: {
id : this.retailerID
    }  }).subscribe(
      res => {
            this.amount=res;
           
         //   console.log('getAmount',this.amount);
            this.amtvalue = this.amount.content.amount;
         //   console.log('getAmount',this.amtvalue);
            if(this.amtvalue<=this.checkAmtModel.checkamt && this.amtvalue>=this.checkAmtModel.endamt){
             // this.alertService.swalError("Your Balance is low Please Recharge!!");
              this.showamt=true;
            }else if(this.amtvalue<this.checkAmtModel.endamt){
              this.showamt=true;
              this.disableprint=true;
              Swal.fire({
                html: '<h3 class="mt-5 mb-4 mx-2"> Please recharge First to start your booking!!</h3>',
                animation: true,
                width: 548,
                padding: '-1.9em',
                // showCancelButton: true,
                focusConfirm: false,
                // cancelButtonText: 'No',
                // cancelButtonColor: '#17A2B8',
                // cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
                confirmButtonColor: '#DD6B55',
                confirmButtonClass: 'btn btn-danger btn-pill px-4',
                confirmButtonText: 'Try Again!'
              })
              this.router.navigate(['/client/wallet-Details']);
            }else{
              this.showamt=false;
              this.disableprint=false;
            }

       });
  }
  lunchbook(){
    let three: string | number; 
    let two: string | number;
  two="21:30";
  three="22:00";
   
    for(let book of this.lunchbookingslot){
     // this.bookingslot.push(book);
      if(book.startTime>=two && book.endTime<=three)
      {
      
    }else{
      this.bookingslot.push(book)
    }
  }
  
 // console.log('bookingslot slot ali', this.bookingslot);
  }
  bookingslot : any = [];
  lunchbookingslot : any = [];
  getslotlist(){
    this.lotteryHttpService.makeRequestApi('get', 'slotlist').subscribe(slot => {
      if (slot !=null) {
     //   console.log(slot, 'slot')
      this.lunchbookingslot= slot;
      this.lunchbook();
     // console.log('booking slot ali', this.bookingslot);
      this.getcurrentSlot();
      this.diable();
      }
      else{
      //  console.log('error');
      }
  
    });
  }
lunch: any;
lunchstatus: any;
  lunchtime(){

    this.lunch=0;
    this.today;
    let h :any;
    let m :any;
    var d = new Date();
    h = d.getHours();
    m = d.getMinutes()
    let min: string;
    if(m<10){
    min ="0"+m;
    }else{
    min=m
    }
    this.match= h+":"+m;
    let timetwo= "14:00";
    let timethree= "15:00";
    
    
    if(this.match>=timetwo && this.match<=timethree)
    {
    this.alertService.swalError("This is lunch time");
    this.lunchstatus=2;
    }else{
    this.lunchstatus=1;
    }
    
    }
  price(){
    this.lotteryHttpService.makeRequestApi('get', 'priceList').subscribe(price => {
      if (price !=null) {
      //  console.log(price);
        const pricecopy = price;
        this.storageService.set('ticketPrice', pricecopy);
      //  console.log('price',pricecopy);
      }
      else{
      //  console.log('error');
      }
  
    });
  }

  getPrice : any;
  getPriceModel : any ={
    "singlePrice" :0,
    "leftPrice"   :0,
    "rightprice"   :0,
    "evenPrice"   :0,
    "oddPrice"   :0,
    "frontPrice"   :0,
    "backPrice"   :0,
    "familyPrice" :0,
    "allPrice" :0,
  }
  fetchprice(){
    this.getPrice=  this.storageService.get('ticketPrice');

    for(let pr of this.getPrice){
        if(pr.name=='single')
        {
          this.getPriceModel.singlePrice= pr.price;
        }
    }
    this.getPriceModel.leftPrice  =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.rightPrice =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.evenPrice  =  this.getPriceModel.singlePrice * 50; 
    this.getPriceModel.oddPrice   =  this.getPriceModel.singlePrice * 50;
    this.getPriceModel.frontPrice =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.backPrice  =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.familyPrice  =  this.getPriceModel.singlePrice * 8;
    this.getPriceModel.allPrice  =  this.getPriceModel.singlePrice * 100;
   // console.log('single price',this.getPriceModel.singlePrice);

    this.storageService.set('singleprice',this.getPriceModel.singlePrice);
  }
  // singprice= this.singleprice[0].price;
  // sinname = this.singleprice[0].name;

  
  callback() {
    let cdate = new Date();
    cdate.setHours(cdate.getHours()-2);
    this.countupTimerService.startTimer(cdate);
   // console.log('sdsd');
   // console.log( this.countupTimerService.startTimer(cdate));
  }




  counter: number = 0;
  timerId: string;
type = "password";
  loginModel : any = {
  "username" : "",
   "password" : "",
}
togleamount(){
  if(this.type=="password"){
    this.modal1.show();
  }else if(this.type=="text"){
    this.type="password";
  }
}
checkLogin(){
  this.lotteryHttpService.makeRequestApi('post', 'userLogin', {phoneNumber : this.phoneNumber, passwordHash : this.loginModel.password, licence : {
    machinekey: getMAC()
  }}
  ).subscribe(res => {
    if(res.code == 206){
      this.type="text";
      this.loginModel.username = "";
      this.loginModel.password = "";
      this.modal1.hide();
    }else{
      this.modal1.hide();
      this.loginModel.username = "";
      this.loginModel.password = "";
      this.alertService.swalError("Your Password is incorrect!!");
    //  this.modal3.show();
      // Swal.fire({
      //   html: '<h3 class="mt-5 mb-4 mx-2">Username or password is not correct</h3>',
      //   animation: true,
      //   width: 548,
      //   padding: '-1.9em',
      //   // showCancelButton: true,
      //   focusConfirm: false,
      //   // cancelButtonText: 'No',
      //   // cancelButtonColor: '#17A2B8',
      //   // cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
      //   confirmButtonColor: '#DD6B55',
      //   confirmButtonClass: 'btn btn-danger btn-pill px-4',
      //   confirmButtonText: 'Try Again!'
      // })
    }
  });
}
ngOnDestroy(){
  
  this._hotkeysService.reset();
}
cssclass="my-navigable-element";
//this.cssclass= "all_gher_single_row"
  ngOnInit() {
   
    setNavigableClassName("my-navigable-element");
    //setNavigableClassName("rowgherrowsingle");
   // this.backnumber();
    $(function () {
      $("#allgher").focus();
    });
   // arrowKeyNavigation.register(); // start listening for key inputs
   // arrowKeyNavigation.unregister() // stop listening
    this.getAmount();
    
this.minTime();
  // console.log('family array',this.array);
    // $(".toggle-password").click(function() {

    //   $(this).toggleClass("fa-eye fa-eye-slash");
    //   var input = $($(this).attr("toggle"));
    //   if (input.attr("type") == "password") {
    //     input.attr("type", "text");
    //   } else {
    //     input.attr("type", "password");
    //   }
    // });
   // this.callback();

    // this.startTimer();
    // $('#print').click(function(){
    //   $('.container').printThis();
    // })
    this.createFirstModel();
    this.createSecondModel();
    this.totalTicketAmount();
    this.show1();
    this.front();
    this.fst();
    this.time();
    this.singlegher();
   
   // console.log('userDetails',this.userD);
    // console.log('userDetails', this.userD);
    // console.log('name', this.sinname);
    // console.log('singpr', this.singprice);
    // console.log('prc', this.singleprice);
    this.gettotal();
     this.price();
    this.getslotlist();
   
    this.fetchprice();
    //this.captureScreen();
    this.checktime();
    this.diable();
    this.getresult();
    setInterval(() => {
      this.checktime(); 
      }, 1000);
    // this.getBalance();
    // this.checktime();
    // this.getcurrentSlot();
 

      // setInterval(() => {
      //   this.disableScreen(); 
      //   }, 1000);

      

    //   setInterval(() => {
    //     this.diable(); 
    //     }, 1000);

      //  setInterval(() => {
      //   this.lunchtime();
      //   }, 1000);

      
      

  }

  gettotal() {
    let total = 0;
    this.totalModel.total = 0;
    for (let _element of this.billModel) {
      for (let element1 of _element.values) {
        this.totalModel.total = this.totalModel.total + element1.totalticketAmt;
      }
    }

    let count=this.slots.length;
    this.grandtotal=this.totalModel.total*count;
   // console.log(total);

  }

  ctrlModel: any = {
    "number": false,
  }
  numb: any = false;
  gherarray: any = [
    // {
    //   "gherno": -1,
    //   "status": 2,
    // }
  ];

  slots : any = [
  ]


  existInSlots(input: { id: any; }){
    let exist = false;
    for(let slot of this.slots){
      if(slot.id == input.id ) exist = true; 
    }
    return exist;
  }

  // --- Add Slots ----------------- Orginal --------------------------
//   addtime(input: any) {
//     let flag=0;
//     for(let slot of this.slots)
//     {
//       if(input==slot)
//       {
//         if(input.id==this.slotid){
//           this.alertService.swalError("Current Slot Should no be deleted");
//           flag=1;
//         }else{
//              let tempArray= [];
//             for(let sl of this.slots){
//               if(input.id!=sl.id){
//                 tempArray.push(sl);
//               }
//             }
// this.slots = [];
// this.slots=tempArray;
//         //  this.slots.splice(input, 1);
//           flag=1;
//         }
      
//       }
//     }
//     if(flag==0){
//       this.slots.push(input);  
//     }
//      console.log(this.slots); 
//      console.log(this.slots); 
//      this.gettotal();
//         }


//  // --- Add Slots ----------------- Orginal  END--------------------------



 // --- Add Slots ----------------- Duplicate --------------------------

 addtime(input: any) {
  // console.log('lallalal',input);
  let flag=0;
  for(let slot of this.slots)
  {
    if(input==slot)
    {
      // if(input.id==this.slotid){
      //   this.alertService.swalError("Current Slot Should no be deleted");
      //   flag=1;
      // }
     // else{
           let tempArray= [];
          for(let sl of this.slots){
            if(input.id!=sl.id){
              tempArray.push(sl);
            }
          }
      this.slots = [];
      this.slots=tempArray;
      //  this.slots.splice(input, 1);
        flag=1;
     // }
    
    }
  }
  if(flag==0){
    this.slots.push(input);  
  }
  // console.log(this.slots); 
 //  console.log(this.slots); 
   this.gettotal();
      }


// ------------------------All Slot-------------------------------

listbachehue:any[]=[];
currentslot:any;
slStart:any;
allButton = 0;
tempArray1: any = [
  // {
  //   "gherno": -1,
  //   "status": 2,
  // }
];
allSlotTime(){

  if(this.tempArray1 == ""){
    this.allButton = 1;
    for(let sl of this.bookingslot){
    if(this.slotid<=sl.id){
   this.tempArray1.push(sl);
    }
    }
    this.slots = [];
    this.slots=this.tempArray1;
    this.gettotal();
  }
 else{
  this.allButton = 0;
  this.tempArray1 = [];
this.slots=[];
this.textValue="";
for(let sl of this.bookingslot){
if(sl.id==this.slotid){
  this.slots.push(sl)
}
}
 }
  }
  slotModel : any = {
  "slots":""
  }
  textVal : any;
  currslot:any= [];
  slotValue:any;
  valuechange(event) {
 this.textVal = event.target.value;
  this.currslot.push(this.textVal)
  this.slotValue = this.countslot - this.leftslot
 // console.log('textValue',);
if( this.textVal == ""){
  this.alertService.swalError('Value cannot be zero');
}
else if(this.textVal > this.slotValue){
  this.alertService.swalError('Value cannot greater  than total slots left ');
}
else if( this.textVal <= 0){
  this.alertService.swalError('Value cannot be less then zero');
}
else{
 // console.log('currentslotid',this.slotid);
let count=this.textVal;
let tamp = [];
for(let sl of this.bookingslot){
  if(this.slotid <= sl.id){
    if(count>0){
      tamp.push(sl);
      count--;
    }
      
  }
  }
 // console.log(tamp,'tamp');
  this.slots = [];
  this.slots=tamp;
  tamp=[];
  this.gettotal();

  this.alertService.swalSuccess('Future Slot is selected ')
}
this.textVal = "";
}

 //  // --- Add Slots ----------------- Duplicate  END--------------------------
        gherNumber: any =[]
        singlegher() {
          for (var i = 0; i < 10; i++) {
      
            this.gherNumber.push({
              "value": i,
              "name": i,
              "status" : 1,
            });
      
          }
        }
        allgherstatus=1;
  allgher(){
    // if(this.allgherstatus==1){
    //   this.allgherstatus=2;
    // }else if(this.allgherstatus==2){
    //   this.allgherstatus=1;
    // }
    if(this.gherarray==""){
      this.allgherstatus=2;
      this.ctrlModel.number = true;
      for(let all of this.modelFirst){
        for (let element1 of all) {
        this.gherarray.push({
          "gherno": element1.value,
          "status": 1,
        });
        let temp = {
          name: element1.value,
        status: "active",
       value:element1.value
        }
       this.evenModel.value=temp;
      }
    //  console.log('all gher all', all);
    //  console.log('all gherarray', this.gherarray);
    }
      for (let ctrl of this.gherarray) {
        for (let element of this.modelFirst) {
          for (let element1 of element) {
            if (ctrl.gherno == element1.value) {
              if (ctrl.status == 1)
                element1.status = 'active';
            }
          }
        }
      }
for(let all of this.gherNumber){
  all.status=2;

}

    }else{
      this.allgherstatus=1;
      for (let ctrl of this.gherarray) {
        for (let element of this.modelFirst) {
          for (let element1 of element) {
            if (ctrl.gherno == element1.value) {
              if (ctrl.status == 1)
                element1.status = 'inactive';
            }
          }
        }
      }
      this.gherarray=[];
      this.evenModel.value=-1;
      for(let all of this.gherNumber){
        all.status=1;
      
      }
    } 
  }

  ifrowgher=0;
  getluckygher = [];
  getluck=1;
        rowgher(number : any){
          let bk=0;
          let temp: { name: any; status: string; value: any; };
          for (let element of this.modelFirst) {
            for (let element1 of element) {
              if (this.evenModel.value.value == element1.value) {
                for(let bok of this.billModel){
                  if (this.evenModel.value.value == bok.gherno) {
                    element1.status = 'gray';
                    bk=1;
                    temp = {
                                  name: bok.gherno,
                                  status: "active",
                                value:bok.gherno
                                 }
                                 this.selectedNumbergher(temp);
                  }
                }
                if(bk==0){
                  element1.status = 'inactive';
                }
               
              }
            }
          }
        //  console.log(this.billModel);
          // let temp: { name: any; status: string; value: any; };
          //         for(let gh of this.gherarray){
          //            temp = {
          //             name: gh.gherno,
          //             status: "active",
          //           value:gh.gherno
          //            }
          //         }
          //         this.gettotal();
          //     this.selectedNumbergher(temp);

          this.evenModel.value=-1;
         // this.refreshgher();
         // this.selectedNumber( this.evenModel.value);
         // this.gherarray=[];
     // console.log('number...',number);
let flag=0;
for(let gh of this.gherNumber){
  if(gh.value==number){
    if(gh.status==1){
      gh.status=2;
      flag=1;
    }else if(gh.status==2){
      gh.status=1;
      flag=0;
    }  
  }
}
if(flag==1){
let test = [];
for(let gh of this.gherarray){
  test.push({
    "gherno": gh.gherno,
    "status": 1,
  })
}
this.gherarray= [];
let tpgh: number;
for(let tt of test){
  tpgh=0;
  for(let bk of  this.billModel){
    if(tt.gherno==bk.gherno){
      tpgh=1;
    }
  }
  if(tpgh==0){
    this.gherarray.push({
      "gherno": tt.gherno,
      "status": 1,
    })
  }
}


  this.ctrlModel.number = true;
  this.getluckygher=[];
  this.getluck=1;
  for (let i = 0; i <= this.modelFirst.length - 1; i++) {
    for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
      if (this.modelFirst[number][j].value == this.modelFirst[i][j].value) {
        this.modelFirst[i][j].status = 'active';
       // this.evenModel.value.value=this.modelFirst[i][j].value;
        this.gherarray.push({
          "gherno": this.modelFirst[i][j].value,
          "status": 1,
        });
        this.getluckygher.push({
          "gherno": this.modelFirst[i][j].value,
          "status": 1,
        });

//         let temp = {
//           name: this.modelFirst[i][j].value,
// status: "active",
// value:this.modelFirst[i][j].value
//         }
    //   this.evenModel.value=temp;
     
     
      }
  
    }
    this.getluck=2;
  }
}

if(flag==0){
  this.getluck=1;
  let temparray = [];
  let clonegherarray = [];
  for (let i = 0; i <= this.modelFirst.length - 1; i++) {
    for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
      if (this.modelFirst[number][j].value == this.modelFirst[i][j].value) {
        this.modelFirst[i][j].status = 'inactive';
        this.evenModel.value=-1;
        temparray.push({
          "gherno": this.modelFirst[i][j].value,
          "status": 1,
        });
      }
  
    }
  }
  for(let gher of this.gherarray){
    clonegherarray.push({
      "gherno": gher.gherno,
      "status": 1,
    });
  }
  //clonegherarray= this.gherarray;
  this.gherarray=[];
  // for(let temp of temparray){
  //   for(let cl  of  clonegherarray ){
  //     if(cl.gherno!=temp.gherno){
  //       this.gherarray.push({
  //         "gherno": cl.gherno,
  //         "status": 1,
  //       })
  //     }
  //   } 
  // }
let tpflag: number;
  for(let cl of clonegherarray ){
    tpflag=0;
    for(let tp of temparray ){
      if(cl.gherno==tp.gherno){
        tpflag=1;
      }
    }
    if(tpflag==0){
      this.gherarray.push({
        "gherno": cl.gherno,
        "status": 1,
      });

      temp = {
        name: cl.gherno,
        status: "active",
      value:cl.gherno
       }
    //   this.selectedNumbergher(temp);
    }
    
   
  }

}
//let  temp: { name?: any; status?: string; value: any; };
for(let cl of this.gherarray){
    temp = {
    name: cl.gherno,
    status: "active",
  value:cl.gherno
   }
 
}
//console.log('temp', temp);
this.refreshButtonListAndValues(temp);
for (let i = 0; i <= this.modelSecond.length - 1; i++) {
  for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
    this.modelSecond[i][j].status = 'inactive';
  }
}
if(this.getluck==2){
  this.getluckNumbers();
}
//this.selectedNumbergher(temp);
  }

  ctrlcheck=1;
  isKeyPressed(event: { ctrlKey: any; }, input: { value: any; }) {

    let flag = 0;
    if (event.ctrlKey) {
      this.ctrlModel.number = true;
      this.ctrlcheck=2;
      flag = 1;
    } else {
      flag = 2;
      this.ctrlcheck=1;
    }
    if (flag == 1) {
      let check = 0;
      for (let gh of this.gherarray) {
        if (gh.gherno == input.value) {
          if(gh.status==1){
            gh.status = 2;       
          }else if(gh.status==2){
            gh.status = 1;
          }
          check = 1;
        }
      }
      if (check == 0) {
        this.gherarray.push({
          "gherno": input.value,
          "status": 1,
        });
      }

      for (let ctrl of this.gherarray) {
        for (let element of this.modelFirst) {
          for (let element1 of element) {
            if (ctrl.gherno == element1.value) {
              if (ctrl.status == 2)
                element1.status = 'inactive';
            }
          }
        }
      }

    //  console.log(this.gherarray);
    //  console.log(this.ctrlModel.number);
    }
    for (let ctrl of this.gherarray) {
      for (let element of this.modelFirst) {
        for (let element1 of element) {
          if (ctrl.gherno == element1.value) {
            if (ctrl.status == 1)
              element1.status = 'active';
          }
        }
      }
    }

    /////--------------



      let temparray = [];
      let clonegherarray = [];
      // for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      //   for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
      //     if (this.modelFirst[number][j].value == this.modelFirst[i][j].value) {
      //       this.modelFirst[i][j].status = 'inactive';
      //       this.evenModel.value=-1;
      //       temparray.push({
      //         "gherno": this.modelFirst[i][j].value,
      //         "status": 1,
      //       });
      //     }
      
      //   }
      // }
for(let gh of this.gherarray){
  for(let book of this.billModel){
    if(gh.gherno==book.gherno){
      temparray.push({
        "gherno": gh.gherno,
        "status": gh.status,
      });
    }
  }
}
      for(let gher of this.gherarray){
        clonegherarray.push({
          "gherno": gher.gherno,
          "status": gher.status,
        });
      }
     
      this.gherarray=[];
  
    let tpflag: number;
      for(let cl of clonegherarray ){
        tpflag=0;
        for(let tp of temparray ){
          if(cl.gherno==tp.gherno){
            tpflag=1;
          }
        }
        if(tpflag==0){
          if(cl.status==1){
            this.gherarray.push({
              "gherno": cl.gherno,
              "status": cl.status,
            });
          }
         
        }
        
       
      }
    
  


    //--------------



   // console.log(this.gherarray);
    if (flag == 2) {
   //   console.log('try again');
    //  console.log('chgcgh', input);
    }
    let  temp: { name?: any; status?: string; value: any; };
    for(let cl of this.gherarray){
        temp = {
        name: cl.gherno,
        status: "active",
      value:cl.gherno
       }
     
    }
  //  console.log('temp', temp);
   // this.selectedNumbergher(temp);
  }

  logKey(e: any) {
 //   console.log(e.ctrlKey);
    let temp = e.ctrlKey;
    this.numb = temp;
 //   console.log('ctrltemp', temp);
//    console.log('ctrltemp123', this.numb);

    if (e.ctrlKey == true) {
      // this.ctrlModel.number=true;
      alert('ctrl key');
    }
    else {
      alert('not a ctrl key ');
    }
  }

  showkey(input: any) {
    if (input == true) {
  //    console.log('fgfc');
  //    console.log(this, this.ctrlModel.number);
  //    console.log(this, this.ctrlModel.number.value);
    }

  }

  refresh(): void {
    window.location.reload();
  }

  grandtotal : any;
// previoustrans =0;
  allresult: any;
  saverepo() {
   // window.print();
    this.isDisabled=true;
  //  this.getcurrentSlot();
    let input = {
      "slots": this.slots,
      "bookingDetail": {
        "distributorId": 0,
        "retailerId": this.retailerID,
        "totalPrice": this.totalModel.total,
      },
      "tickets": this.billModel
    }
let count=this.slots.length;
 //  console.log('slot count', count);
   
let subtotal: number;
    console.log(JSON.stringify(input), 'input booking ...');
    // let output=JSON.stringify(input);
    subtotal= this.totalModel.total*count;
if(this.amtvalue<this.totalModel.total){
  this.alertService.swalError("Total is less than your Amount Value");
}else{
  this.lotteryHttpService.makeRequestApi('post', 'booking', input).subscribe((res) => {
if(res.code==206){
//  console.log('json data');
//  console.log(JSON.stringify(res));
 
//  console.log('booking saved');
//  console.log(res);
  this.storageService.set('lastamt',this.totalModel.total);
  this.getAmount();
  this.allresult=res;
 

  // this.refresh();
//  console.log('check data.........',this.allresult.content);
  for(let last of this.allresult.content){

      
      this.lastid= last.bookingDetails.id;
      this.storageService.set('lasticket',this.lastid);
    
  }


//  this.ticketprint();
//this.ticpre();
this.bookingPrint();

  this.billModel=[];
  this.evenModel.value=-1;
 

  this.totalModel.total=0;
  this.slots=[];
// this.selectedNumber();  

this.inactiveall();
}else if(res.code==208){
  this.alertService.swalError("Please specify the slots....");
}else if(res.code==300){
  this.alertService.swalError(res.description+"\n"+"Your Time slot is excced !! that's why Your Application form is not choosed!!");
  this.isDisabled=false;
}
else if(res.code==301){
  this.alertService.swalError("Yor are in active user Please connect to Admin!!");
  this.isDisabled=false;
}
else if(res.code==302){
  this.alertService.swalError("You are selected previous slot please select current slot!!");
  this.isDisabled=false;
  this.restslot();
}
   
});
// console.log('last id',this.lastid);

}
   
  }

// lastid = "0";
  public myAngularxQrCode: string = null;
match :any;
matchtime : any;
matchEndtime: any;
leftslot =1;
showamt=false;
disableprint = false;

matcham: any;
bookingPrint(){
  const file = '/home/foo.json'
//  const file = 'E:\\WORK\\tmp\\foo.json'
const obj = { name: 'JP' }
 console.log(this.allresult.content,'this.allresult' );
jsonfile.writeFile(file, this.allresult.content, function (err) {
  if (err) console.error(err)
})

setTimeout(() => {
  this.calljar();
}, 3000);

}

calljar(){
var exec = require('child_process').exec, child;
child = exec('java -jar /home/result.jar',
//child = exec('java -jar E:\\result.jar',
function (error, stdout, stderr){
console.log('stdout: ' + stdout);
console.log('stderr: ' + stderr);
if(error !== null){
  console.log('exec error: ' + error);
}
});

setTimeout(() => {
  this.printPdf();
}, 2000);
// this.printPdf();
this.reset();
this.alertService.swalSuccess("Selection saved");
}
printPdf(){
  ptp
  .print("/home/my_doc.pdf")
  .then(console.log)
  .catch(console.error);
  //.print("E:\\my_doc.pdf")
}
  checktime(){
    // console.log('checktime');
    //this.leftslot= this.countslot;
    this.today;
    let h :any;
    let m :any;
    let s:any;
    var d = new Date();
   h = d.getHours();
   m = d.getMinutes()
   s= d.getSeconds();
  //  console.log('time');
  //  console.log(h);
  //  console.log(m);

  let min: string;
  if(m<10){
    min= "0"+m;
  }else{
min =m
  }
   this.match= h+":"+min;
  //  this.match= "19:00";
let time="21:30";
// console.log(this.match, 'this.match')
// console.log(time, 'this.time')

if(this.match>=time){
 
this.bookingslot=[];
console.log(this.bookingslot, 'this.bookingslot')
}else{

  for(let book of this.bookingslot)
  {
   
    if(this.match>=book.startTime && this.match<=book.endTime)
    {
             this.matchtime=book.startTime;

             this.matchEndtime=book.drawTime;
             this.matcham= book.drawTimeAm;
             
    }
  }
}



  this.restslot();
  
    }

disablebooking =1;
    disableScreen(){
      //this.leftslot= this.countslot;
      this.today;
      let h :any;
      let m :any;
      var d = new Date();
     h = d.getHours();
     m = d.getMinutes()
    //  console.log('time');
    //  console.log(h);
    //  console.log(m);
    let min: string;
    if(m<10){
      min="0"+m;
    }else{
      min=m;
    }
     this.match= h+":"+min;
    //  this.match= "19:00";
let flag=0;


     for(let book of this.bookingslot)
     {
       if(this.match==book.endTime )
       {
               flag=1;
                
       }else if(this.match==book.drawTime){
flag=2;
       }
     }
  if(flag==1){
  //  this.modal4.show();
  this.disablebooking=2;
  this.alertService.swalError("Conclusion is Processing...");
  }else if(flag==2){
    //this.modal4.hide();
    this.disablebooking=1;
    flag=0;
    this.getresult();
  }

    
      }
totalslot: any;
    restslot(){
         this.leftslot= this.countslot;
         this.totalslot= this.countslot;
      for(let book of this.bookingslot)
      {
        if(this.matchtime<=book.startTime)
        {
          this.leftslot--;
          //this.totalslot= this.leftslot;
        }
      } 
    }




    timeLeft: number = 60;
    min: number = 15;
    interval: NodeJS.Timer;
//     startTimer() {
// this.interval = () => {
//   if(this.min > 0) {
    
//   }
// }
//      this.interval = setInterval(() => {
//         if(this.timeLeft > 0) {
//           this.timeLeft--;
//         } else {
//           this.timeLeft = 60;
//         }
//       },1000)
//     }
  //   onPrint(){
  //     window.print();
  // }

//   printToCart(printSectionId: string){
//     let popupWinindow
//     let innerContents = document.getElementById(printSectionId).innerHTML;
//     popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
//     popupWinindow.document.open();
//     popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
//     popupWinindow.document.close();
// }
    countslot: any =1;
    diable(){
      this.countslot= this.bookingslot.length;
    }  
  ticketprint(){
    this.randomnumber= Math.random();
    this.randomnumber= "DestributerID"+"Retailer ID"+" "+this.randomnumber;
this.myAngularxQrCode = this.randomnumber;
 //  console.log(this.myAngularxQrCode);
this.modal2.show();
  }

//   printCart(printSectionId: string){
//     let popupWinindow
//     let innerContents = document.getElementById(printSectionId).innerHTML;
//     popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
//     popupWinindow.document.open();
//     popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
//     popupWinindow.document.close();
// }

  // onPrint(){
     
  //     var innerContents = document.getElementById('#printSectionId').innerHTML;
  //     var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
  //     popupWinindow.document.open();
  //     popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
  //     popupWinindow.document.close();
  //   }

// public print(printEl: HTMLElement) {
//   let printContainer: HTMLElement = document.querySelector('#printSectionId');

//   if (!printContainer) {
//     printContainer = document.createElement('div');
//     printContainer.id = 'printSectionId';
//   } 

//   printContainer.innerHTML = '';

//   let elementCopy = printEl.cloneNode(true);
//   printContainer.appendChild(elementCopy);
//   document.body.appendChild(printContainer);

//   window.print();
// }

printToCart(_printSectionId: string){
  // var printContents = document.getElementById(printSectionId).innerHTML;
	// 		var originalContents = document.body.innerHTML;
	// 		document.body.innerHTML = printContents;
      window.print();
  //    // window.close();
	// 		document.body.innerHTML = originalContents;
}


  public captureScreen() {
    const data = document.getElementById('printSectionId');
    html2canvas(data).then(canvas => {
    const imgWidth = 57;
    const pageHeight = 297;
    const imgHeight = canvas.height * imgWidth / canvas.width;
    const heightLeft = imgHeight;
    const contentDataURL = canvas.toDataURL('image/png');
    //  const pdf = new jspdf('p', 'mm', 'a4'); 
      // pdf.setFontSize(22);
      //   pdf.text(20, 20, 'Example');
      //   pdf.autoPrint();
      //     window.open(pdf.output('bloburl'), '_blank');
    const position = 0;

    // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, pageHeight);
    // //pdf.print();
    // pdf.save('invoice.pdf');

    // pdf.open('invoice.pdf');
    // pdf.output;
   
    });
    //this.billModel=[];
      //  this.totalModel.total=0;
       // this.slots=[];
       // this.modal2.hide();
    }

    ///

luckywinners = [
  {

    "id": 1,
    "gherno" : 22,
    "data" : [
      {
        "time" : "10:15",
         "no"  : 24,
      }   
    ],

  }
]

  // timer-------------
  getLuckyArray: any= [];
  clonearray: any= [];
  getreverseLuckyArray: any= [];
   mainArray : any =[];
  elseArray : any =[];
  getreverse(){
    let value =0;
    this.getreverseLuckyArray= [];
    for(let i=this.getLuckyArray.length-1;i>=0;i--)
    {
    
  this.getreverseLuckyArray.push({
    "gherno" :this.getLuckyArray[i].gherno,
     "luckyno" : this.getLuckyArray[i].luckyno,
     "status" : this.getLuckyArray[i].status,
  })
  
  value=0;
  
    }
  //  console.log('checkreverse',this.getreverseLuckyArray);
    
  }

//   getluckyNumber(gherno: any){
//     let mn =1;
//     let els =2;
// let flag=0;
// for(let ch of this.getLuckyArray){
//   if(gherno.value==ch.gherno){
// flag=1;
// break;
//   }
// }
// if(flag==1){
// for(let sort of this.getLuckyArray){
//   if(gherno.value==sort.gherno){
//     this.mainArray.push({
//       "gherno" : sort.gherno,
//        "luckyno" : sort.luckyno,
//        "status" : mn,
//     })
//   }else{
//     this.elseArray.push({
//       "gherno" : sort.gherno,
//       "luckyno" : sort.luckyno,
//       "status" :  els,
//     })
//   }
// }
// this.getLuckyArray = [];

// for(let els of this.elseArray){
//   this.getLuckyArray.push({
//     "gherno" : els.gherno,
//     "luckyno" : els.luckyno,
//     "status" :  els.status,
//   });
// }
// for(let main of this.mainArray){
//   this.getLuckyArray.push({
//     "gherno" : main.gherno,
//        "luckyno" : main.luckyno,
//        "status" :  main.status,
//   });
// }
// this.mainArray = [];
// this.elseArray = []; 
// }else if(flag==0){

//   for(let res of this.getLuckyArray){
//     if(res.status==1)
//     {
//       res.status=2;
//     }
//     }


//   for(let res of this.resultList){
//     if(gherno.value==res.gherNo){
//     this.getLuckyArray.push({
//       "gherno" : res.gherNo,
//        "luckyno" : res.luckyNumber,
//        "status" : mn,
      
//     });
//     }
//  }
// }
//     console.log('check lucky result',this.getLuckyArray);
//     this.getreverse();
//   }
mainarray=[];
elsearray=[];
  getluckNumbers(){

 //   console.log('getluckygher',this.getluckygher);
 //   console.log('resultList',this.resultListdummy);
    let mn =1;
    let els =2;
let flag=0;
    for(let luck of this.getluckygher){
      for(let get of this.getLuckyArray){
           if(luck.gherno==get.gherno){
                flag=1;
            }
      }
    }  
    if(flag==1){
      this.mainarray=[];
      this.elsearray=[];
      for(let luck of this.getluckygher){
        for(let get of this.getLuckyArray){
             if(luck.gherno==get.gherno){
              this.mainarray.push({
                "gherno" : get.gherno,
                 "luckyno" : get.luckyno,
                 "status" : mn,
                
              }); 
              }
        }
      }
   //   console.log('mainarray',this.mainarray);
      let temp=[];
      let f=0;
// for(let lk of this.getLuckyArray){
//   f=0;
//   for(let get of this.getluckygher){
//     if(lk.gherno==get.gherno){
// f=1
// break;
//     }
//   }
//   if(f==0){
//     temp.push({
//       "gherno" : lk.gherno,
//       "status": 1
//     })
//   }
// }

// this.getluckygher=[];
// this.getluckygher=temp;
//       for(let get of this.getluckygher){
     
//         for(let luck of this.getLuckyArray ){
//              if(get.gherno==luck.gherno){
//               this.elsearray.push({
//                 "gherno" : luck.gherno,
//                  "luckyno" : luck.luckyno,
//                  "status" : els,
                
//               });  
              
//              }
//             }
           
//           }
//           console.log('elsearray',this.elsearray);
//       this.getLuckyArray=[];
//       for(let el of this.elsearray){
//         this.getLuckyArray.push({
//           "gherno" : el.gherno,
//            "luckyno" : el.luckyno,
//            "status" : el.status,
          
//         }); 
//       }
    //  console.log('elsearray getLuckyArray',this.getLuckyArray);
      this.getLuckyArray=[];
      for(let main of this.mainarray){
        this.getLuckyArray.push({
          "gherno" : main.gherno,
           "luckyno" : main.luckyno,
           "status" : main.status,
          
        }); 
      }
   //   console.log('mainarray getLuckyArray',this.getLuckyArray);
    }

 

    if(flag==0){
      this.mainarray=[];
      this.elsearray=[];
      for(let res of this.getLuckyArray){
        if(res.status==1)
        {
          res.status=2;
        }
        }

        let temp=[];
        temp=this.getLuckyArray;
        this.getLuckyArray=[];
      for(let luck of this.getluckygher)
      { 
      for(let res of this.resultList){
        if(luck.gherno==res.gherNo){
        this.getLuckyArray.push({
          "gherno" : res.gherNo,
           "luckyno" : res.luckyNumber,
           "status" : mn,
          
        });
        }
     }
    }
    if(temp==null){

    }else{
      for(let tp of temp){
        this.getLuckyArray.push({
          "gherno" : tp.gherno,
           "luckyno" : tp.luckyno,
           "status" : tp.status,
          
        });
      }
    }
   
    temp=[];
    }
   // console.log('getLuckyArray',this.getLuckyArray);
    this.getluckygher=[];
    this.getluck=1;
    this.getreverseLuckyArray=this.getLuckyArray;
  //this.getreverse();
  }
  //--------------------
  mint =15;
  minTime(){
    if(this.mint>0)
    { 
      // this.mint--;
    // this.countTimer();
    }
  }

  Lefttime: number = 60;
  interval1: NodeJS.Timer;

countTimer() {
    this.interval1 = setInterval(() => {
      if(this.Lefttime > 0) {
        this.Lefttime--;
      } else {
        this.Lefttime = 60;
      }
    },1000)
    this.mint--;
  }
  
// getBalance(){
//     let user= { 
//       "userName": this.userD.user.userName, 
//       "email": this.userD.user.email, 
//       "userId":this.userD.user.id
//      }
//     this.http.post(this.url+'users/balance',user).subscribe(response => {
//       this.balanceInfo = JSON.parse(JSON.stringify(response));
//     });
// }

reset(){
  this.billModel =[];
  this.inactiveall();
  this.selectedNumber(this.evenModel.value);
  this.inactiveall();
  this.totalModel.total=0;
  this.grandtotal=0;
  this.textValue="";
  this.getLuckyArray= [];
  this.getreverseLuckyArray = [];
  this.gherarray=[];
  for(let all of this.gherNumber){
    all.status=1;
  
  }
  this.evenModel.value=-1;
  this.slots=[];
  //this.getslotlist();
  this.getcurrentSlot();
}

tickethistory = [];
canceltickethistory=[];
cancelPreview() {
  if(this.lastid==0){
    this.alertService.swalError('You have not last Selection!!');
  }else{
  let reqMap = {
    bookingDetail: {
      id:this.lastid,
    }
  }
 // console.log(reqMap);
  this.lotteryHttpService.makeRequestApi('post', 'bookingId', reqMap).subscribe((res) => {
    if (res.code == 206) {
      let ticket = res;
      this.tickethistory=res.content;
  //   console.log('tickethistory',res);
  //    console.log('tickethistory',this.tickethistory);
      this.canceltickethistory=[];
      for(let tic of this.tickethistory){
        this.canceltickethistory.push({
          "bookingDate":tic.bookingDetails.bookingDate,
          "createDate":tic.bookingDetails.createDate,
          "id":tic.bookingDetails.id,
          "retailerId":tic.bookingDetails.retailerId,
          "drawTimeAm":tic.bookingDetails.slot.drawTimeAm,
          "ticketId":tic.bookingDetails.ticketId,
          "totalPrice":tic.bookingDetails.totalPrice,
          "totalTicket":tic.bookingDetails.totalTicket,
          "ticketNumbers":tic.ticketNumbers
        })
      }
      this.modal5.show();
    //  console.log('canceltickethistory',this.canceltickethistory); 
    }else{
      this.alertService.swalError('Technical Issue');
    }
  });
}
}

cancel(){
  let input= {
    "user" : {
      "id" : this.retailerID
    },
    "bookingId" : this.lastid
  }
 //  console.log('cancel');
 //  console.log(JSON.stringify(input));
if(this.lastid=="0"){
  this.alertService.swalError("You have no last selection...");
}else{
  this.lotteryHttpService.makeRequestApi('post', 'ticketCancle', input).subscribe((res) => {
  //  console.log(res);
   if(res.code==206){
    this.alertService.swalSuccess("selection Cancel");
    this.lastid="0";
    this.getAmount();
    this.modal5.hide();
   }else if(res.code==209){
    this.alertService.swalError("Technical issue");
   }else if(res.code==208){
    this.alertService.swalError(res.message);
    this.modal5.hide();
   }else if(res.code==301){
    this.alertService.swalError(res.message);
    this.modal5.hide();
   }else if(res.code==302){
    this.alertService.swalError(res.message);
    this.modal5.hide();
   }
  
    });
}


}
previewModal : any = [];
previewslotModal : any = [];
ticpre(){
  this.previewModal= [];
  this.previewModal= this.allresult;
//console.log('this.allresult.....',this.allresult)
  this.storageService.set('preview', this.previewModal);
  this.router.navigate(['/ticket-preview']);
}

previewModel : any= {
  "total":"",
}
preview(){
  
  this.previewModal= [];
  this.previewslotModal= [];
  this.previewModal= this.billModel;
  this.previewslotModal= this.slots;
  let count=this.slots.length;
  this.totalModel.tickettotal= this.totalModel.total;
  this.previewModel.total= this.totalModel.total*count;
  this.storageService.set('preview', this.previewModal);
  this.storageService.set('previewslots', this.previewslotModal);
  this.storageService.set('previewtotalamt', this.totalModel.total);
  this.modal2.show();
}
  
  //----------------------

  ticket: any;
  claimList: any = [];
  requestedId = this.storageService.get('currentUser').user.roleMaster.id
  Disabledis = false;
  openClaimModal(){
    this.modal6.show();
    this.ticket = "";
    this.claimList = [];
   // console.log('Kya ea empty hua',this.claimList);

  }

  selfClaim(_emp: any) {
    this.Disabledis=true;
  //  console.log(this.requestedId);
    let reqMap = {
      id: this.claimList.id,
      loginId: this.storageService.get('currentUser').user.id,
      paymentType: "cash"
    }
    this.lotteryHttpService.makeRequestApi('post', 'settledCliam', reqMap).subscribe((_res) => {
      this.getdetails();
    });
    this.Disabledis=false;
    this.modal8.hide();
    this.alertService.swalSuccess('claim successfully Done');
    this.getdetails();
    this.claimList=[];
  }
  bookingDetails: any = {
    "id": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
  } 


  getdetails() {

  //  console.log(this.ticket);
    let reqMap = {
      id: this.ticket
    }
    id: this.ticket;
    this.lotteryHttpService.makeRequestApi('post', 'searchByNumber', reqMap).subscribe((res) => {
      this.claimList = res;
   //   console.log('claimlist', res);
      this.bookingDetails.startTime = this.claimList.bookingDetail.slot.startTime;
      this.bookingDetails.id = this.claimList.bookingDetail.ticketId;
      this.bookingDetails.totalPrice = this.claimList.bookingDetail.totalPrice;
      this.bookingDetails.bookingDate = this.claimList.bookingDetail.bookingDate;
      this.bookingDetails.status = this.claimList.status.id;
      this.bookingDetails.winningPrice = this.claimList.bookingDetail.winningPrice;

      
    });
    this.claimList = [];
  //  console.log('Kya ea empty hua',this.claimList);

    $('.table').show();
    
  }
  modal(){
this.modal6.hide();
this.claimList= [];
this.ticket= "";
this.modal8.show();
this.bookingDetails.startTime = "";
this.bookingDetails.id = "";
this.bookingDetails.totalPrice = "";
this.bookingDetails.bookingDate = "";
this.bookingDetails.status = "";
this.bookingDetails.winningPrice = "";
  }
  close(){
    this.modal6.hide();
   this.claimList = [];
    this.ticket= "";
    this.bookingDetails.startTime = "";
    this.bookingDetails.id = "";
    this.bookingDetails.totalPrice = "";
    this.bookingDetails.bookingDate = "";
    this.bookingDetails.status = "";
    this.bookingDetails.winningPrice = "";
 //   console.log('Kya ea empty hua',this.claimList);
  }
  valueoftext :any;
  //----------------------
  text(event: any) {
    this.modal6.show();
    this.valueoftext += event.target.value;
 //   console.log('ati kya khandala',this.valueoftext);
    let reqMap = {
      id: this.valueoftext
    }
    id: this.ticket;
    this.lotteryHttpService.makeRequestApi('post', 'searchByNumber', reqMap).subscribe((res) => {
      this.claimList = res;
   //   console.log('claimlist', res);
      this.bookingDetails.startTime = this.claimList.bookingDetail.slot.startTime;
      this.bookingDetails.id = this.claimList.bookingDetail.ticketId;
      this.bookingDetails.totalPrice = this.claimList.bookingDetail.totalPrice;
      this.bookingDetails.bookingDate = this.claimList.bookingDetail.bookingDate;
      this.bookingDetails.status = this.claimList.status.id;
      this.bookingDetails.winningPrice = this.claimList.bookingDetail.winningPrice;

      
    });
    this.claimList = [];
 //   console.log('Kya ea empty hua',this.claimList);


  }
}