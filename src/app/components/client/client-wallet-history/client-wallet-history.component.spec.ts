import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientWalletHistoryComponent } from './client-wallet-history.component';

describe('ClientWalletHistoryComponent', () => {
  let component: ClientWalletHistoryComponent;
  let fixture: ComponentFixture<ClientWalletHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientWalletHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientWalletHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
