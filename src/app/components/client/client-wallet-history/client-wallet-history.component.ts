import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { LotteryStorageService } from '../../../services/lottery-storage.service';

@Component({
  selector: 'app-client-wallet-history',
  templateUrl: './client-wallet-history.component.html',
  styleUrls: ['./client-wallet-history.component.scss']
})
export class ClientWalletHistoryComponent implements OnInit {
  userD = this.storageService.get('currentUser');
  id= this.userD.user.roleMaster.id;
  navModel: any = {
    "wallet": 1,
    "walletHistory": 2,
   
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.wallet = 2;
      this.navModel.walletHistory = 1;
    

    }
    else if (input == 2) {
      this.navModel.wallet = 1;
      this.navModel.walletHistory = 2;
    }

    
  }
  balance: any;
  Balance: any = {
    "amount": "",
  }
  page = 1;
  pageSize = 6;
  today = Date();
  WalletCalenderDate:any;
  walletList:any[]=[];
  filteredWalletList:any[]=[];
  constructor(private router: Router, private sweetAlert: SweetAlertService, private storageService: LotteryStorageService,
    private http: HttpClient, private lotteryService: LotteryHttpService,  private datePipe: DatePipe) { }

  ngOnInit() {
    this.walletBalance();
    this.walletreport();
  }
  walletHistory(){
    this.router.navigate(['/client/wallet-Details']);
  }
  openClientWalletHistory(){
    this.router.navigate(['/client/client-wallet-history']);
  }
  openDistributorWalletHistory(){
    this.router.navigate(['/distributor/distributor-wallet-History']);
  
  }
  walletBalance() {

    let reqMap = {
      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBalance', reqMap).subscribe((res) => {
      this.balance = res;
      console.log(this.balance.content.amount);
      this.Balance.amount = this.balance.content.amount;
    });

  }
  walletreport(){     
    this.date = this.today;
    let reqMap = {
      transactionDate :this.datePipe.transform(this.date, "dd-MM-yyyy"),
      user: {
        id: this.storageService.get('currentUser').user.id,
 
      }
 
    }
    this.lotteryService.makeRequestApi('post', 'retailerWalletHistory', reqMap).subscribe((res) => {
      this.walletList = res.content;
      this.filteredWalletList = this.walletList
  
    });  
    console.log('Histrory',this.filteredWalletList);
    this.date = this.datePipe.transform(this.today, "dd-MM-yyyy")

  }
  date:any;
  walletReportByDate(){
    this.date = this.WalletCalenderDate
    this.lotteryService.makeRequestApi('post', 'retailerWalletHistory', {
      transactionDate :this.datePipe.transform(this.date, "dd-MM-yyyy"),
      user: {
        id: this.storageService.get('currentUser').user.id,
 
      }
      }).subscribe(res => {
      console.log('hello user data', res);
      this.walletList = res.content;
      this.filteredWalletList = this.walletList   
      });
      console.log('Histrory',this.filteredWalletList);

      this.date = this.datePipe.transform(this.WalletCalenderDate, "dd-MM-yyyy")
  
  }
 
}
