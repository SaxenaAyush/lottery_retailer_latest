import { Component, OnInit, ViewChild } from '@angular/core';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataSharingService } from '../../../services/data-sharing.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { Route, Router } from '@angular/router';
// import { BarecodeScannerLivestreamComponent } from 'ngx-barcode-scanner';
import { getDevices } from 'usb-barcode-scanner';
import { UsbScanner } from 'usb-barcode-scanner';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  // @ViewChild(BarecodeScannerLivestreamComponent)
  // barecodeScanner: BarecodeScannerLivestreamComponent;
  
  // barcodeValue;
  
  // ngAfterViewInit() {
  //     this.barecodeScanner.start();
  // }

  // onValueChanges(result){
  //     this.barcodeValue = result.codeResult.code;
  // }
 
  public userForm: FormGroup;
  loggedInUserDetails: any;
  isEdit = false;
  profile: any;
  userP: any;
  currentP: any;
  permanentP: any;
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  public personalDetailsForm: FormGroup
  public bankDetailsForm: FormGroup
  public kycDetailsForm: FormGroup
  constructor(private fb: FormBuilder, private lotteryService: LotteryHttpService, private storageService: LotteryStorageService,
    private httpClient: HttpClient, private dataSharingService: DataSharingService, private alertService: SweetAlertService, private router: Router) { }

  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('currentUser');
    this.loginUser();

   
    console.log(this.loggedInUserDetails);
  }
  userModel: any = {
    "firstName": "",
    "lastName": "",
    "phoneNumber": "",
    "email": "",

  }
  currentAddress: any = {
    "address": "",
    "city": "",
    "state": "",
    "pin": "",
    "country": "",
  }
  permanentAddress: any = {
    "address": "",
    "city": "",
    "state": "",
    "pin": "",
    "country": "",

  }

  userd() {
    this.userModel.firstName = this.userDetails.user.firstName,
      this.userModel.lastName = this.userDetails.user.lastName,
      this.userModel.phoneNumber = this.userDetails.user.phoneNumber,
      this.userModel.email = this.userDetails.user.email
  }

  current() {
    if (this.currentAddress.address == null) {
      this.currentAddress.address = ""
    } else {
      this.currentAddress.address = this.userDetails.currentDetail.address

    } if (this.currentAddress.city == null) {
      this.currentAddress.city = ""

    } else {
      this.currentAddress.city = this.userDetails.currentDetail.city
    }
    if (this.currentAddress.state == null) {
      this.currentAddress.state = ""

    } else {
      this.currentAddress.state = this.userDetails.currentDetail.state
    }
    if (this.currentAddress.pin == null) {
      this.currentAddress.pin = ""

    } else {
      this.currentAddress.pin = this.userDetails.currentDetail.pin

    } if (this.currentAddress.country == null) {
      this.currentAddress.country = ""
    }

    else {

      this.currentAddress.country = this.userDetails.currentDetail.country
    }

  }
  permanent() {
    this.permanentAddress.address = this.userDetails.permanentDetail.address,
      this.permanentAddress.city = this.userDetails.permanentDetail.city,
      this.permanentAddress.state = this.userDetails.permanentDetail.state,
      this.permanentAddress.pin = this.userDetails.permanentDetail.pin,
      this.permanentAddress.country = this.userDetails.permanentDetail.country
  }
  initForm() {

    this.userForm = this.fb.group({
      user: this.initPersonalDetails(),
      permanentDetail: this.initPermanentAddress(),
      currentDetail: this.initCurrentAddress(),
    });

  }
  initPersonalDetails() {
    if (this.loggedInUserDetails.permanentDetail !== null) {
      return this.fb.group({
        firstName: [this.loggedInUserDetails.user.firstName],
        lastName: [this.loggedInUserDetails.user.lastName],
        phoneNumber: [this.loggedInUserDetails.user.phoneNumber],
        email: [this.loggedInUserDetails.user.email],

      });
    } else {
      return this.fb.group({
        firstName: [this.loggedInUserDetails.user.firstName],
        lastName: [this.loggedInUserDetails.user.lastName],
        phoneNumber: [this.loggedInUserDetails.user.phoneNumber],
        email: [this.loggedInUserDetails.user.email],
      });
    }
  }

  initPermanentAddress() {
    if (this.loggedInUserDetails.permanentDetail !== null) {
      return this.fb.group({
        address: [this.loggedInUserDetails.permanentDetail.address],
        city: [this.loggedInUserDetails.permanentDetail.city],
        state: [this.loggedInUserDetails.permanentDetail.state],
        pin: [this.loggedInUserDetails.permanentDetail.pin],
        country: [this.loggedInUserDetails.permanentDetail.country],
        id: [this.loggedInUserDetails.permanentDetail.id],

      });
    } else {
      return this.fb.group({
        address: [''],
        city: [''],
        state: [''],
        pin: [''],
        country: [''],

      });
    }
  }
  initCurrentAddress() {
    if (this.loggedInUserDetails.currentDetail !== null) {
      return this.fb.group({

        address: [this.loggedInUserDetails.currentDetail.address],
        city: [this.loggedInUserDetails.currentDetail.city],
        state: [this.loggedInUserDetails.currentDetail.state],
        pin: [this.loggedInUserDetails.currentDetail.pin],
        country: [this.loggedInUserDetails.permanentDetail.country],
        id: [this.loggedInUserDetails.currentDetail.id],

      });
    } else {
      return this.fb.group({

        address: [''],
        city: [''],
        state: [''],
        pin: [''],
        country: [''],

      });
    }
  }

  // user1={
  //   "firstName": this.loggedInUserDetails.user.firstName,
  //   "lastName":this.loggedInUserDetails.user.lastName,
  //   "phoneNumber": this.loggedInUserDetails.user.phoneNumber,
  //   "email": this.loggedInUserDetails.user.email,
  // }

  userdetail: any;
  formSubmit() {
let per;
let cuc;
    console.log(this.userModel);
    console.log(this.currentAddress);
    console.log(this.permanentAddress);
    console.log(this.loggedInUserDetails);
    if(this.loggedInUserDetails.hasOwnProperty('permanentDetail')){
      console.log('permanentDetail');
per=this.loggedInUserDetails.permanentDetail.id;
   //   this.permanent();
    }else{
      //this.loggedInUserDetails.permanentDetail.id="",
      console.log('not');
      per="";
    }
    if(this.loggedInUserDetails.hasOwnProperty('currentDetail')){
      console.log('this.current();');
    //  this.current();
    cuc=this.loggedInUserDetails.currentDetail.id;
      //this.loggedInUserDetails.currentDetail.id=""
    }else{
      console.log('not');
      cuc=""
    }

    let input = {
      "user": {

        "id": this.loggedInUserDetails.user.id,
        "firstName": this.userModel.firstName,
        "lastName": this.userModel.lastName,
        "phoneNumber": this.userModel.phoneNumber,
        "email": this.userModel.email
      },
      "permanentDetail": {
        "id": per,
        "address": this.permanentAddress.address,
        "city": this.permanentAddress.city,
        "state": this.permanentAddress.state,
        "pin": this.permanentAddress.pin,
        "country": this.permanentAddress.country
      },

      "currentDetail": {

        "id": cuc,
        "address": this.currentAddress.address,
        "city": this.currentAddress.city,
        "state": this.currentAddress.state,
        "pin": this.currentAddress.pin,
        "country": this.currentAddress.country
      }

    }
    console.log(input);
    this.lotteryService.makeRequestApi('post', 'updateEmployee', input).subscribe((res) => {
      if (res.code == 206) {
        this.alertService.swalSuccess(' Successfully Updated');
        this.loggedInUserDetails=res.content;
        console.log(res);
        this.profile = res.content;
        console.log('profile..', this.profile);
        this.userP = this.profile.user;
        this.currentP = this.profile.currentDetail;
        this.permanentP = this.profile.permanentDetail;
        console.log('userp..', this.userP);
        console.log('current..', this.currentP);
        console.log('perma..', this.permanentP);

        this.userModel.firstName = this.userP.firstName;
        this.userModel.lastName = this.userP.lastName;
        this.userModel.phoneNumber = this.userP.phoneNumber;
        this.userModel.email = this.userP.email;

        this.currentAddress.address = this.currentP.address;
        this.currentAddress.city = this.currentP.city;
        this.currentAddress.state = this.currentP.state;
        this.currentAddress.pin = this.currentP.pin;
        this.currentAddress.country = this.currentP.country;

        this.permanentAddress.address = this.permanentP.address,
          this.permanentAddress.city = this.permanentP.city,
          this.permanentAddress.state = this.permanentP.state,
          this.permanentAddress.pin = this.permanentP.pin,
          this.permanentAddress.country = this.permanentP.country

        this.isEdit = false;
      } else if (res.code == 209) {
        this.alertService.swalError(' technical issue');
      }

    }, err => {
      console.log(err);
    });

  }

  userDetails: any;
  loginUser() {

    const reqMap = {
      id: this.userD.user.id
    };
    this.lotteryService.makeRequestApi('post', 'loginId', reqMap).subscribe(res => {
      this.userDetails = res;
      console.log('detailssssss', this.userDetails.user.firstName);
      console.log('detailsdfdfsssss', this.userDetails.user);
      
if(this.userDetails.hasOwnProperty('permanentDetail')){
  console.log('permanentDetail');
  this.permanent();
}else{
  //this.loggedInUserDetails.permanentDetail.id="",
  console.log('not');
}
if(this.userDetails.hasOwnProperty('currentDetail')){
  console.log('this.current();');
  this.current();
  //this.loggedInUserDetails.currentDetail.id=""
}else{
  console.log('not');
}
if(this.userDetails.hasOwnProperty('user')){
  console.log('this.user();');
  this.userd();
}else{
  console.log('not');
}

      // for(let add of this.userDetails.user){
      //   console.log('dcdcd',add);
      //   if(add.hasOwnProperty('address')){
      //     console.log('check');
      //   }else{
      //     console.log('not');
      //   }

      // }
     
    //  this.current();
     // this.permanent();
    });

  }
 
 

}
