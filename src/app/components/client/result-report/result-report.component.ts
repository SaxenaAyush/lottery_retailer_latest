import {Component, OnInit} from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import {LotteryStorageService} from '../../../services/lottery-storage.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-result-report',
  templateUrl: './result-report.component.html',
  styleUrls: ['./result-report.component.scss']
})
export class ResultReportComponent implements OnInit {

  constructor(private lotteryService: LotteryHttpService,
    private storageService: LotteryStorageService,private datePipe:DatePipe) {
  }
  bookingslot: any=[];
  resultList: any = [];
  ngOnInit() {
  this.getSlotList();
  }

getResultByDateAndSlot(slotId:any){
 this.lotteryService.makeRequestApi('post', 'viewResults', {bookingDate: this.datePipe.transform(new Date(),"dd-MM-yyyy"),slotMaster:{id:1 }}).subscribe(
   res => {
         this.resultList=res;
         console.log(this.resultList);
    });
}

 getSlotList(){
     this.lotteryService.makeRequestApi('get', 'slotlist').subscribe(slot => {
       if (slot !=null) {
       this.bookingslot= slot;
       this.getResultByDateAndSlot(this.bookingslot[0].id);
       }
       else{
         console.log('error');
       }

     });
   }
  }



