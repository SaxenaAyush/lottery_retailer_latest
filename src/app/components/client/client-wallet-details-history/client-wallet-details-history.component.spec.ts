import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientWalletDetailsHistoryComponent } from './client-wallet-details-history.component';

describe('ClientWalletDetailsHistoryComponent', () => {
  let component: ClientWalletDetailsHistoryComponent;
  let fixture: ComponentFixture<ClientWalletDetailsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientWalletDetailsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientWalletDetailsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
