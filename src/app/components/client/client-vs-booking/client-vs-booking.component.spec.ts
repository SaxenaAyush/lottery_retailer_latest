import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientVsBookingComponent } from './client-vs-booking.component';

describe('ClientVsBookingComponent', () => {
  let component: ClientVsBookingComponent;
  let fixture: ComponentFixture<ClientVsBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientVsBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientVsBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
