import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as $ from '../../../../../node_modules/jquery';

// declare let jsPDF;
import * as jsPDF from 'jspdf';
// import 'jspdf-autotable';

@Component({
  selector: 'app-client-vs-booking',
  templateUrl: './client-vs-booking.component.html',
  styleUrls: ['./client-vs-booking.component.scss']
})
export class ClientVsBookingComponent implements OnInit {
  navModel: any = {
    "selfReport": 1,
    "claimvsbooking": 2,
   
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.selfReport = 2;
      this.navModel.claimvsbooking = 1;
    

    }
    else if (input == 2) {
      this.navModel.selfReport = 1;
      this.navModel.claimvsbooking = 2;
    }

    
  }
  dailyReportFiltered:any[]=[];
  dailyReport:any[]=[];
  allRetailers:any[]=[];
  allDurationRetailers:any[]=[];
  durationReportFiltered:any[]=[];
  TodayDate:any;
  FromDate:any;
  status = 1;
  PlayerTodayDate:any;
  PlayerFromDate:any
  today = Date();
  page = 1;
  pageSize = 7;
  // page2 = 1;
  // pageSize2 = 7;
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
    this.Dailyreport();
    this.single();
    this.PlayerTodayDate=this.datePipe.transform(this.today, "dd-MM-yyyy") 

  }

  selfReport(){
    this.route.navigate(['/client/daily-report']); 
   }
  //  openCliamVsBooking(){
  //   this.route.navigate(['/client/client-vs-booking']) 

  //  }
  

  single() {
    this.status = 1;
    this.FromDate = "";
    this.TodayDate = "";
    $('.singleForm').show();
    $('.durationForm').hide();
  

  }
  duration() {
    this.status = 2;
    this.FromDate = "";
    this.TodayDate = "";
  
    $('.singleForm').hide();
    $('.durationForm').show();

  }
Dailyreport(){

  this.PlayerTodayDate = this.today

  let reqMap = {
    retailerId:  this.storageService.get('currentUser').user.id,
    bookingDate: this.datePipe.transform(this.PlayerTodayDate, "dd-MM-yyyy") ,    
  }
  this.lotteryHttpService.makeRequestApi('post', 'allUserCreatedById', reqMap).subscribe((res) => {
    this.allRetailers = res.content;
    this.dailyReportFiltered = this.allRetailers

    this.storageService.set('claimvsbookingReport',this.dailyReportFiltered);
    this.storageService.set('date',this.PlayerTodayDate);

    console.log(this.dailyReportFiltered);

  });
  this.PlayerTodayDate=this.datePipe.transform(this.today, "dd-MM-yyyy") 

}
dailyReportByDate(){
  this.PlayerTodayDate = this.TodayDate

  let reqMap = {
    retailerId:  this.storageService.get('currentUser').user.id,
    bookingDate: this.datePipe.transform(this.PlayerTodayDate, "dd-MM-yyyy") ,   
  }
  this.lotteryHttpService.makeRequestApi('post', 'allUserCreatedById', reqMap).subscribe((res) => {
    this.allRetailers = res.content;
    this.dailyReportFiltered = this.allRetailers
    this.storageService.set('claimvsbookingReport',this.dailyReportFiltered);
    this.storageService.set('date',this.PlayerTodayDate);

    console.log('claimvsbookingReport',this.PlayerTodayDate);

  }); 
this.PlayerTodayDate=this.datePipe.transform(this.TodayDate, "dd-MM-yyyy") 
}

durationReportByDate(){
  this.PlayerTodayDate = this.TodayDate
  console.log('From Date',this.PlayerFromDate);
  let reqMap = {
    fromDate:this.datePipe.transform(this.FromDate, "dd-MM-yyyy") ,
	  toDate:this.datePipe.transform(this.PlayerTodayDate, "dd-MM-yyyy") ,
    retailerId:  this.storageService.get('currentUser').user.id,
  }
  this.lotteryHttpService.makeRequestApi('post', 'toFromDateReport', reqMap).subscribe((res) => {
    this.allDurationRetailers = res.content.book;
    this.durationReportFiltered = this.allDurationRetailers,
    console.log('durationReportFiltered',this.durationReportFiltered);
    // this.storageService.set('claimvsbookingReport',this.dailyReportFiltered);
    // this.storageService.set('date',this.PlayerTodayDate);

    console.log('claimvsbookingReport',this.PlayerTodayDate);

  }); 
this.PlayerTodayDate=this.datePipe.transform(this.TodayDate, "dd-MM-yyyy") 
this.PlayerFromDate=this.datePipe.transform(this.FromDate, "dd-MM-yyyy") 

}


slotTime:any
Details(slotId: any,drawTimeAm:any) {
  this.slotTime = drawTimeAm
    this.lotteryHttpService.makeRequestApi('post', 'slotDetails', {
      bookingDate :this.PlayerTodayDate,
      retailerId:this.storageService.get('currentUser').user.id,
      slot: {
        id : slotId
      }
  
    }).subscribe(res => {
    console.log('hello user data', res);
    this.storageService.set('slotsData', res.content);
    this.storageService.set('id', slotId);
    this.storageService.set('drawTime', this.slotTime);
    this.storageService.set('date', res.content.bookingDetail[0].bookingDate);
    this.route.navigate(['/client/claim-vs-booking-details']);
    });
    
}
print(){
  console.log("Function is working")
  this.route.navigate(['/first-claim-vs-booking-print']) 

}
// convert() {

//   var doc = new jsPDF();
//   var col = ["Details", "Values"];
//   var rows = [];

// /* The following array of object as response from the API req  */

// var itemNew = [    
// { id: 'Case Number', name : '101111111' },
// { id: 'Patient Name', name : 'UAT DR' },
// { id: 'Hospital Name', name: 'Dr Abcd' },
// { id: 'Case Number', name : '101111111' },
// { id: 'Patient Name', name : 'UAT DR' },

// ]


// itemNew.forEach(element => {      
//   var temp = [element.id,element.name];
//   rows.push(temp);

// });        

//   doc.autoTable(col, rows);
//   doc.save('doodea.pdf');
// }
}
