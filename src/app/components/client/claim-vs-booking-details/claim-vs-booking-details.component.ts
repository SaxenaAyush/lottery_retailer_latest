import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-claim-vs-booking-details',
  templateUrl: './claim-vs-booking-details.component.html',
  styleUrls: ['./claim-vs-booking-details.component.scss']
})
export class ClaimVsBookingDetailsComponent implements OnInit {
  SlotsDetails:any[]=[];
  slotTime:any;
  TodayDate:any;
  viewSlotsDetails = this.storageService.get('slotsData');
  viewSlotsId = this.storageService.get('id');
  viewSlotsdrawTime = this.storageService.get('drawTime');

  drawTime
  constructor(private fb: FormBuilder, private storageService: LotteryStorageService,
     private route: Router, private alertService: SweetAlertService,private datePipe: DatePipe,
    private lotteryService: LotteryHttpService) { }

  ngOnInit() {
    this.SlotsDetails = this.viewSlotsDetails.bookingDetail
         console.log('National',this.SlotsDetails);
         this.slotTime = this.viewSlotsdrawTime
  }
//  slotsByDate(){
//   this.lotteryService.makeRequestApi('post', 'slotDetails', {
//     bookingDate :this.datePipe.transform(this.TodayDate, "dd-MM-yyyy"),
//     retailerId:this.storageService.get('currentUser').user.id,
//     slot: {
//       id : this.viewSlotsId
//     }

//   }).subscribe(res => {
//     this.SlotsDetails = res.content
//   });
//  }
back(){
  this.route.navigate(['/client/client-vs-booking']) 


}
print(){
  console.log("Function is working")
  this.route.navigate(['/claim-vs-booking-print']) 

}
}
