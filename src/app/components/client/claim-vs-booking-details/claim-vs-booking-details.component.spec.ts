import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimVsBookingDetailsComponent } from './claim-vs-booking-details.component';

describe('ClaimVsBookingDetailsComponent', () => {
  let component: ClaimVsBookingDetailsComponent;
  let fixture: ComponentFixture<ClaimVsBookingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimVsBookingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimVsBookingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
