import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import Swal from 'sweetalert2';
import { getDevices } from 'usb-barcode-scanner';
import { UsbScanner } from 'usb-barcode-scanner';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-claim-transaction',
  templateUrl: './claim-transaction.component.html',
  styleUrls: ['./claim-transaction.component.scss']
})
export class ClaimTransactionComponent implements OnInit {
  scannerValue: any;
  searchText: any;
  searchText2: any;
  searchText3: any;
  searchText4: any;
  searchText5: any;
  isDisabled = false;
  isDisabled1 = false;

  today: number = Date.now();
  tempList: any = [];
  hideClaim = false;
  mprice: any;
  homeClass = "active";
  transactionClass = "inactive";
  dispatchClass = "inactive";
  homeActive = "btn-btn-primary";
  @ViewChild('modal5') modal5: any;
  @ViewChild('modal1') modal1: any;
  @ViewChild('modal4') modal4: any;
  mobile: Number;
  ticket: Number;
  claimList: any = [];
  slotList: any = [];
  cliamDetailsList: any = [];
  claimedList: any = [];
  requestedDetails: any = [];
  dispatchedList: any = [];
  dispatchedDetail: any = [];
  transactionList: any = [];
  allTransactionList: any = [];
  tempMobile: Number;
  tempTicket: Number;
  requestList: any = [];
  requestedList: any = [];
  historyList: any = [];
  ticketHistoryList: any = [];
  status: String;
  page = 1;
  pageSize = 8;
  requestedId = this.storageService.get('currentUser').user.roleMaster.id
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  property = false;
  roleId = this.userD.user.roleMaster.id;
  constructor(private sweetAlert: SweetAlertService, private router: Router, private storageService: LotteryStorageService, private lotteryHttpService: LotteryHttpService, private alertService: SweetAlertService, ) {
  }

  clicked() {
    this.tempMobile = this.mobile;
    this.tempTicket = this.ticket;
  }
  navModel: any = {
    "claim": 2,
    "transcation": 1,
    "dispatch": 1,
    "requested": 1,
    "history": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.transcation = 2;
      this.navModel.claim = 1;
      this.navModel.dispatch = 1;
      this.navModel.history = 1;
      this.navModel.requested = 1;

    }
    else if (input == 2) {
      this.navModel.transcation = 1;
      this.navModel.claim = 2;
      this.navModel.dispatch = 1;
      this.navModel.requested = 1;
      this.navModel.history = 1;
    }
    else if (input == 3) {
      this.navModel.transcation = 1;
      this.navModel.claim = 1;
      this.navModel.dispatch = 2;
      this.navModel.requested = 1;
      this.navModel.history = 1;
    }
    else if (input == 4) {
      this.navModel.transcation = 1;
      this.navModel.claim = 1;
      this.navModel.dispatch = 1;
      this.navModel.requested = 2;
      this.navModel.history = 1;
    }
    else if (input == 5) {
      this.navModel.transcation = 1;
      this.navModel.claim = 1;
      this.navModel.dispatch = 1;
      this.navModel.requested = 1;
      this.navModel.history = 2;
    }
  }
  bookingDetails: any = {
    "id": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
  } 

  getdetails() {

    console.log(this.ticket);
    let reqMap = {
      id: this.ticket,     
	    loginId:this.storageService.get('currentUser').user.id,
    }
    id: this.ticket;
    this.lotteryHttpService.makeRequestApi('post', 'searchByNumber', reqMap).subscribe((res) => {
      if(res.code == 206){
        this.claimList = res.content;
        console.log('claimlist', res);
        this.bookingDetails.startTime = this.claimList.bookingDetail.slot.startTime;
        this.bookingDetails.id = this.claimList.bookingDetail.ticketId;
        this.bookingDetails.totalPrice = this.claimList.bookingDetail.totalPrice;
        this.bookingDetails.bookingDate = this.claimList.bookingDetail.bookingDate;
        this.bookingDetails.status = this.claimList.status.id;
        this.bookingDetails.winningPrice = this.claimList.bookingDetail.winningPrice;
      } else if (res.code == 301){
            this.sweetAlert.swalError(res.message);
      } else{
        this.sweetAlert.swalError(res.message);
      }
  

      
    });
    $('.table').show();
    
  }
  ngOnInit() {

    $(function () {
      $("#Box1").focus();
    });
 
    // this.scannerValue = getDevices();
    // console.log('hthrthr', getDevices());
    // console.log(this.scannerValue);
    // console.log('value', this.scannerValue);
    // console.log(this.requestedId);
    // this.items = Object.keys(this.users[0]);
    // this.persons = Object.keys(this.users);
  }
  allTransactionEntry: any = {
    "id": "",
    "name": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
    "status": "",

  }
  transList: any[] = [];
  filterTransList :any[]=[];
  openTransactionForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').show();
    $('.dispatched-Form').hide();
    $('.requested-Form').hide();
    $('.history-Form').hide();

    $('.search3-Form').hide();
    $('.search1-Form').hide();
    $('.search2-Form').show();
    $('.search4-Form').hide();
    $('.search5-Form').hide();

    let reqMap = {

     
        loginId: this.storageService.get('currentUser').user.id,
      
    }

    this.lotteryHttpService.makeRequestApi('post', 'viewAllTransactionClaim', reqMap).subscribe((res) => {
      this.transactionList = res;

      this.transList = this.transactionList.content;
      this.filterTransList = this.transList;
      console.log(this.transList);
      for (var j = 0; j < this.transList.length; j++) {
        this.allTransactionList = this.transList[j]

        console.log(this.allTransactionList.claim);
        if (this.allTransactionList.claim != null) {
          console.log('hello');
          this.allTransactionEntry.startTime = this.allTransactionList.claim.bookingDetail.slot.startTime;
          this.allTransactionEntry.id = this.allTransactionList.claim.bookingDetail.id;
          this.allTransactionEntry.totalPrice = this.allTransactionList.claim.bookingDetail.totalPrice;
          this.allTransactionEntry.bookingDate = this.allTransactionList.claim.bookingDetail.bookingDate;
          this.allTransactionEntry.status = this.allTransactionList.status.name;
          this.allTransactionEntry.name = this.allTransactionList.requestBy.firstName;
        }
      }
    });
  }

  openCliamForm() {
    $('.transaction-Form').hide();
    $('.dispatched-Form').hide();
    $('.claim-Form').show();
    $('.requested-Form').hide();
    $('.history-Form').hide();
    $('.search1-Form').show();
    $('.search2-Form').hide();
    $('.search3-Form').hide();
    $('.search4-Form').hide();
    $('.search5-Form').hide();
  }
  dispatchedEntry: any = {
    "id": "",
    "name": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
    "status": "",

  }
  newList: any = [];
  openDispatchedForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').hide();
    $('.dispatched-Form').show();
    $('.requested-Form').hide();
    $('.history-Form').hide();
    $('.search3-Form').show();
    $('.search2-Form').hide();
    $('.search1-Form').hide();
    $('.search4-Form').hide();
    $('.search5-Form').hide();


    let reqMap = {

      loginId: this.storageService.get('currentUser').user.id,
    }

    this.lotteryHttpService.makeRequestApi('post', 'viewDispatchClaim', reqMap).subscribe((res) => {

      this.newList = res.content;

      for (var j = 0; j < this.newList.length; j++) {
        this.dispatchedList = this.newList[j]
      }
      console.log(this.dispatchedList);

    });
  }
  requestedEntry: any = {
    "id": "",
    "name": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
    "status": "",

  }
  openRequestedForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').hide();
    $('.dispatched-Form').hide();
    $('.requested-Form').show();
    $('.history-Form').hide();

    $('.search3-Form').hide();
    $('.search2-Form').hide();
    $('.search1-Form').hide();
    $('.search4-Form').show();
    $('.search5-Form').hide();


    let reqMap = {

      loginId: this.storageService.get('currentUser').user.id,
    }
    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'viewRequestClaim', reqMap).subscribe((res) => {
      this.requestList = res.content;

      for (var j = 0; j < this.requestList.length; j++) {
        this.requestedList = this.requestList[j]
      }
      console.log(this.requestedList);
    });

  }

  selfClaim(emp) {
    this.isDisabled=true;
    this.isDisabled1 = true;

    console.log(this.requestedId);
    let reqMap = {
      id: this.claimList.id,
      loginId: this.storageService.get('currentUser').user.id,
      paymentType: "cash"
    }

    this.lotteryHttpService.makeRequestApi('post', 'settledCliam', reqMap).subscribe((res) => {
    
      this.getdetails();
    });
    this.isDisabled=false;
    this.isDisabled1=false;

    this.modal1.hide();
    this.sweetAlert.swalSuccess('claim successfully Done');
    this.getdetails();
  }

  ClaimModal(){
    this.modal1.show();
  }
  openHistoryForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').hide();
    $('.dispatched-Form').hide();
    $('.requested-Form').hide();
    $('.history-Form').show();
    $('search3-Form').hide();
    $('.search2-Form').hide();
    $('.search1-Form').hide();
    $('.search4-Form').hide();
    $('.search5-Form').show();

    let reqMap = {

      id: this.storageService.get('currentUser').user.id,

    }
    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'ticketHistory', reqMap).subscribe((res) => {
      this.historyList = res;

      // for (var j = 0; j < this.historyList.length; j++) {
      //   this.ticketHistoryList = this.historyList[j]
      // }
      this.filteredhistoryList = this.historyList.content;
      console.log(this.historyList);

    });
  }
  ticketEntry: any = {
    "ticketid": "",
    "ticketamount": "",
    "ticketdate": "",
    "transactionMode": "",

  }
  historyList2: any;
  ticketHistoryList2: any;
  openModalTicket(value) {
    this.modal5.show();
    let reqMap = {
      user: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'ticketHistory', reqMap).subscribe((res) => {
      this.historyList2 = res.content;
      for (var j = 0; j < this.historyList.length; j++) {
        this.ticketHistoryList2 = this.historyList2[j]
      }
      console.log(this.historyList2);
      // this.ticketEntry.ticketid = this.ticketHistoryList2.bookingId,
      //   this.ticketEntry.ticketdate = this.ticketHistoryList2.transactionDate,
      //   this.ticketEntry.ticketamount = this.ticketHistoryList2.transactionAmount
      // this.ticketEntry.transactionMode = this.ticketHistoryList2.transactionMode
      console.log('tabtab', this.ticketEntry.ticketamount);
    });
  }
  bookingdate: any;
  slottime: any;
  slots: any;
  ticketDeatils: any;
  number: any;
  gher: any;
  amount: any;
  totalprice: any;
  previewModal: any = [];
  TicketpreviewModal:any=[];
  PreviewticketDeatils:any;
  ticketBookingDate:any;
  ticketSlotTime:any;
  ticketTotalPrice:any;
  totaltickets: any = [];
  ticketcount: any = [];
  sepratedata() {
    
  
  }
  previewticketModal(userId) {
    let reqMap = {
      bookingDetail: {
        id: userId,
      }
    }

    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'bookingId', reqMap).subscribe((res) => {
      if (res.code == 206) {
        
        this.PreviewticketDeatils = res.content[0];
        this.ticketBookingDate = this.PreviewticketDeatils.bookingDetails.bookingDate;
        this.ticketSlotTime = this.PreviewticketDeatils.bookingDetails.slot.drawTime;
        this.ticketTotalPrice = this.PreviewticketDeatils.bookingDetails.totalPrice;
        console.log('PreviewticketDeatils', this.PreviewticketDeatils);    

       }      
       for (let demo of this.PreviewticketDeatils) {
      
        this.TicketpreviewModal.push({
          "gherno": demo.gherno,
          "ticketNo": demo.bookingDetail.totalTicket,
          "totalticketAmt": demo.bookingDetail.totalPrice,
          "numbersSelected": demo.numbersSelected,

        })
        

      }
      for (let bill of this.PreviewticketDeatils) {
        
        this.ticketcount.push({
          "id": bill.bookingDetails.id,
          "ticketId": parseInt(bill.bookingDetails.ticketId)
        });
  
        this.totaltickets.push({
          "id": bill.bookingDetails.id,
          "retailerId": bill.bookingDetails.retailerId,
          "bookingDate": bill.bookingDetails.bookingDate,
          "createDate": bill.bookingDetails.createDate,
          "totalPrice": bill.bookingDetails.totalPrice,
          "slot": bill.bookingDetails.slot.drawTimeAm,
          "totalTicket": bill.bookingDetails.totalTicket,
          "ticketNumbers": bill.ticketNumbers,
          "ticketId": parseInt(bill.bookingDetails.ticketId)
        })
        //   }
        // }
  
      }

        this.modal5.show();
      
    });
  }
  ticketModal(userId) {
    let reqMap = {
      bookingDetail: {
        id: userId,
      }
    }

    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'bookingId', reqMap).subscribe((res) => {
      if (res.code == 206) {
        let ticket = res;
        this.ticketDeatils = res.content;
        // console.log('ticketDeatils......', this.ticketDeatils);
        this.storageService.set('reprint', ticket);
        this.router.navigate(['/claim-reprint']);
        this.previewModal = [];
        // this.slots = this.ticketDeatils[0].bookingDetail.slot.drawTimeAm;
        // this.bookingdate = this.ticketDeatils[0].bookingDetail.createDate;
        // console.log('slots', this.slots);
        // this.slottime = this.slots.drawTimeAm;
        // console.log('slotsssssssss', this.slottime);
        // this.totalprice = this.ticketDeatils[0].bookingDetail.totalPrice;
        // console.log('sdcdsc', this.totalprice);
        for (let demo of this.ticketDeatils) {
          // for(let d of demo.bookingDetail){
          //   this.totalprice=d.totalPrice;
          // }
          this.previewModal.push({
            "gherno": demo.gherno,
            "ticketNo": demo.ticketNo,
            "totalticketAmt": demo.totalticketAmt,
            "numbersSelected": demo.numbersSelected,

          })
          // for(let i of this.previewModal.numbersSelected){

          //}

        }
        for (let p of this.previewModal) {
          console.log('abc', p.numbersSelected);
          this.bac = p.numbersSelected;
          //let a= this.splt(this.bac);
          //console.log(a);
        }

      }



      console.log('houseno', this.previewModal);
    });
  }

  farwordClaim(emp) {
    console.log(this.ticket);
    let reqMap = {
      id: this.claimList.id,
      loginId: this.storageService.get('currentUser').user.id,
      requestTo: {
        id: this.requestedId,
      }
    }
    console.log();
    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'farwordClaim', reqMap).subscribe((res) => {
      this.getdetails();

    });
    this.modal4.hide();
    this.sweetAlert.swalSuccess('claim successfully Farworded');

  }
  bac: any;
  backup: any = [];
  //   splt(string : any) {
  //     var array = string.split(",");

  //     for(let i = 0; i < array.length; i++){
  //     let a=array[i].split(",");
  //     // var array1 = a.split("[");
  //     // for(let i = 0; i < array1.length; i++){
  //     //   let b=array[i].split("[");
  //     //   console.log(b);
  //     // }

  //     console.log(a);
  //    // console.log(b);
  //     //console.log(this.backup);
  //     }
  // }
  //   refresh(): void {
  //     window.location.reload();
  // }
  items: any = [];
  persons: any = [];
 
  code: any;
  cancleTicket(emp) {
    console.log('emp', emp)
    Swal.fire({
      html: '<h3 class="mt-5 mb-4 mx-2">Are you sure you want to Cancle this Ticket?</h3>',
      animation: true,
      width: 548,
      padding: '-1.9em',
      showCancelButton: true,
      focusConfirm: false,
      cancelButtonText: 'No',
      cancelButtonColor: '#17A2B8',
      cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
      confirmButtonColor: '#DD6B55',
      confirmButtonClass: 'btn btn-danger btn-pill px-4',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {

        this.lotteryHttpService.makeRequestApi('post', 'ticketCancle', {
          user: {
            id: this.storageService.get('currentUser').user.id,
          },
          bookingId: emp
        }).subscribe(res => {
          if (res.code == 206) {
            this.sweetAlert.swalSuccess('Ticket Cancled');
          }
          else {
            this.sweetAlert.swalError(res.message);
          }
          if (!res.isError) {
            this.openHistoryForm();
          }
          this.code = res.code;
        });

      }
    });

  }
  filteredhistoryList: any

  search1() {
    console.log(this.searchText);
    for (let item of this.historyList.content) {
      if (this.searchText == item.id) {
        this.filteredhistoryList = [];
        this.filteredhistoryList.push(item)

        this.searchText = "";
      }
      // console.log('this.resultList', this.resultList);

    }
  }
  search4() {
    console.log(this.searchText4);
    for (let item of this.requestedList) {
      if (this.searchText4 == item.id) {
        this.requestedList = [];
        this.requestedList.push(item)

        this.searchText = "";
      }
      // console.log('this.resultList', this.resultList);

    }
  }
  search3() {
    console.log(this.searchText3);
    for (let item of this.newList) {
      if (this.searchText3 == item.id) {
        this.newList = [];
        this.newList.push(item)

        this.searchText = "";
      }
      // console.log('this.resultList', this.resultList);

    }
  }
  search5() {
    console.log(this.searchText5);
    for (let item of this.historyList.content) {
     
      if (this.searchText5 == item.ticketId) {
        this.filteredhistoryList = [];
        this.filteredhistoryList.push(item)

        // this.searchText5 = "";
      }
      if(this.searchText5 == ""){
        this.openHistoryForm();
      }
      // console.log('this.resultList', this.resultList);

    }
  }
  search2() {
    console.log(this.searchText2);
    for (let item of this.transList) {
      if (this.searchText2 == item.id) {
        this.transList = [];
        this.transList.push(item)

        this.searchText = "";
      }
      // console.log('this.resultList', this.resultList);

    }
  }
  searchDataActivate: any[] = [];
  onSearchFilterActivate(searchData) {
      if (searchData) {
        this.filterTransList = this.transList.filter(item => {
          const filter = Object.keys(item);
          return filter.some(
            key => {  
              if ((key === 'createDate'  ) && item[key] != null) {
                return item[key].toLowerCase().trim().indexOf(searchData.toLowerCase().trim()) !== -1;
              }
              // else if ((key === 'totalPrice' || key === 'totalTicket' ) && item[key] != null) {
              //   return item[key].toString().trim().indexOf(searchData.toString().trim()) !== -1;
              // }
            }
          );
        });
      } else {
        this.filterTransList = this.transList;
      }
    }
}
