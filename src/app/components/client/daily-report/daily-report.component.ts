import { Component, OnInit } from '@angular/core';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { Router } from '@angular/router';
import { NgForm, FormBuilder } from '@angular/forms';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { DatePipe } from '@angular/common';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import * as $ from '../../../../../node_modules/jquery';

@Component({
  selector: 'app-daily-report',
  templateUrl: './daily-report.component.html',
  styleUrls: ['./daily-report.component.scss']
})
export class DailyReportComponent implements OnInit {
  navModel: any = {
    "selfReport": 2,
    "claimvsbooking": 1,
   
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.selfReport = 2;
      this.navModel.claimvsbooking = 1;
    

    }
    else if (input == 2) {
      this.navModel.selfReport = 1;
      this.navModel.claimvsbooking = 2;
    }

    
  }
  bookingslot: any;
  SlotstartTime: any;
  SlotEndTime: any;
  slots: any;
  status = 1;
  FromDate: any;
  ToDate: any;
  FromSlot: any;
  ToSlot: any;
  detailsList: any;
  Claim: any;
  ClaimBonus: any;
  Commission: any;
  TicketBooked: any;
  WalletBalance:any;
  Profit:any;
  page = 1;
  pageSize = 4;
  List :any = [];
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
    this.getslotlist();
    this.getDetailsToday();
  }
  getslotlist() {
    this.lotteryHttpService.makeRequestApi('get', 'slotlist').subscribe((res) => {
      this.slots = res;
      console.log('booking slot', this.slots);
      this.SlotstartTime = res.startTime;
      this.SlotEndTime = res.drawTimeAm;
      console.log('Startslot ali', this.SlotstartTime);
      console.log('Endslot ali', this.SlotEndTime);

    });
  }

  slotModel = {
    "slot_from_id": 0,
    "slot_to_id": 0
  };
  single() {
    this.status = 1;
    this.FromDate = "";
    this.ToDate = "";
    this.List = "";
  

  }
  duration() {
    this.status = 2;
    this.FromDate = "";
    this.ToDate = "";
    this.List = "";
    

  }
  printReport() {
    this.route.navigate(['/report-print']);
  }
  durationPrintReport(){
    this.route.navigate(['/duration-report-print']);
    this.storageService.set('durationDate', this.durationDetailsList);
    console.log('list',this.durationDetailsList);
  }
  claimAmount:any;
  currentBalance:any;
  name:any;
  Date = Date();
  today= this.datePipe.transform(this.Date, "dd-MM-yyyy");
  payToCompany:any;
  profit:any;
  ticketBooked:any;
  totalWalletRecharge:any;
  cashInDrawer:any;
  reportDate:any;
  closingBalance:any;
  openingBalance:any;
  durationclaimAmount:any;
  durationcurrentBalance:any;
  durationname:any;
  durationpayToCompany:any;
  durationprofit:any;
  durationticketBooked:any;
  durationtotalWalletRecharge:any;
  durationcashInDrawer:any;
  durationreportDate:any;
  durationclosingBalance:any;
  durationopeningBalance:any;
  durationdailyRecharge:any[]=[];
  dailyRecharge:any = [];
  getDetails() {
    let reqMap = {
      userId: this.storageService.get('currentUser').user.id,
      reportDate: this.datePipe.transform(this.ToDate, "dd-MM-yyyy"),
      // toDate: this.datePipe.transform(this.ToDate, "yyyy-MM-dd"),
      // tempDate: this.datePipe.transform(this.ToDate, "dd-MM-yyyy"),

      // formSlot: this.slotModel.slot_from_id,
      // toSlot: this.slotModel.slot_to_id
      // fromSlot:1,
      // toSlot:48,
    }
 
    this.lotteryHttpService.makeRequestApi('post', 'dailyReport', reqMap).subscribe(res => {
      this.storageService.set('reportDetails', res);
      this.detailsList = res.content;
      this.storageService.set('reportDetails', this.detailsList);
      this.claimAmount = this.detailsList.claimAmount,
      this.currentBalance = this.detailsList.currentBalance,
      this.payToCompany = this.detailsList.payToCompany,
      this.ticketBooked = this.detailsList.ticketBooked 
      this.cashInDrawer = this.detailsList.cashAmount
      this.profit = this.detailsList.profit,
      this.totalWalletRecharge = this.detailsList.totalWalletRecharge,
      this.reportDate = this.detailsList.reportDate,
      this.name = this.detailsList.name,
      this.dailyRecharge = this.detailsList.dailyRecharge,
      this.closingBalance = this.detailsList.closingBalance,
      this.openingBalance = this.detailsList.openingBalance
      console.log('Paisaaaa',this.dailyRecharge);
      // this.List.push({ Claim : this.detailsList.Claim,
      //   ClaimBonus : this.detailsList.ClaimBonus,
      //   Commission : this.detailsList.Commission,
      //   TicketBooked : this.detailsList.TicketBooked });

    });
    }
    getDetailsToday() {
      let reqMap = {
        userId: this.storageService.get('currentUser').user.id,
        reportDate: this.datePipe.transform(this.Date, "dd-MM-yyyy"),
        // toDate: this.datePipe.transform(this.ToDate, "yyyy-MM-dd"),
        // tempDate: this.datePipe.transform(this.ToDate, "dd-MM-yyyy"),
  
        // formSlot: this.slotModel.slot_from_id,
        // toSlot: this.slotModel.slot_to_id
        // fromSlot:1,
        // toSlot:48,
      }
   
      this.lotteryHttpService.makeRequestApi('post', 'dailyReport', reqMap).subscribe(res => {
        this.storageService.set('reportDetails', res);
        this.detailsList = res.content;
        this.storageService.set('reportDetails', this.detailsList);
        this.claimAmount = this.detailsList.claimAmount,
        this.currentBalance = this.detailsList.currentBalance,
        this.payToCompany = this.detailsList.payToCompany,
        this.ticketBooked = this.detailsList.ticketBooked 
        this.cashInDrawer = this.detailsList.cashAmount
        this.profit = this.detailsList.profit,
        this.totalWalletRecharge = this.detailsList.totalWalletRecharge,
        this.reportDate = this.detailsList.reportDate,
        this.name = this.detailsList.name,
        this.dailyRecharge = this.detailsList.dailyRecharge,
        this.closingBalance = this.detailsList.closingBalance,
        this.openingBalance = this.detailsList.openingBalance
        console.log('Paisaaaa',this.dailyRecharge);
        // this.List.push({ Claim : this.detailsList.Claim,
        //   ClaimBonus : this.detailsList.ClaimBonus,
        //   Commission : this.detailsList.Commission,
        //   TicketBooked : this.detailsList.TicketBooked });
  
      });
      }
    durationDetailsListTotal:any;
      durationDetailsList:any[]=[];
    durationDetailsListTotal1:any;
    durationToDate:any;
    getDurationDetails(){
      $('.loader').show();
      let reqMap = {
        userId: this.storageService.get('currentUser').user.id,
        toDate: this.datePipe.transform(this.ToDate, "dd-MM-yyyy"),      
        reportDate : this.datePipe.transform(this.FromDate, "dd-MM-yyyy"), 
        
      }
      this.lotteryHttpService.makeRequestApi('post', 'reportDatewise', reqMap).subscribe(res => {
        // this.storageService.set('durationreportDetails', res);
        console.log('durationReport',res)
        this.durationDetailsList = res.content.DateWise;
        this.durationDetailsListTotal = res.content.Total;
        // this.durationDetailsListTotal1 = this.durationDetailsListTotal;
        $('.loader').hide();

        this.storageService.set('durationreportDetails', this.durationDetailsList);
        this.durationclaimAmount = this.durationDetailsListTotal.claimAmount,
        this.durationcurrentBalance = this.durationDetailsListTotal.currentBalance,
        this.durationpayToCompany = this.durationDetailsListTotal.payToCompany,
        this.durationticketBooked = this.durationDetailsListTotal.ticketBooked, 
        this.durationcashInDrawer = this.durationDetailsListTotal.cashAmount,
        this.durationprofit = this.durationDetailsListTotal.profit,
        this.durationtotalWalletRecharge = this.durationDetailsListTotal.totalWalletRecharge,
        this.durationreportDate = this.durationDetailsListTotal.reportDate,
        this.durationToDate = this.durationDetailsListTotal.toDate,
        this.durationname = this.durationDetailsListTotal.name,
        this.durationdailyRecharge = this.durationDetailsListTotal.dailyRecharge,
        this.durationclosingBalance = this.durationDetailsListTotal.closingBalance,
        this.durationopeningBalance = this.durationDetailsListTotal.openingBalance,
                console.log('Paisaaaa',this.durationDetailsListTotal);
      });
    }
   selfReport(){
    this.route.navigate(['/client/daily-report']) 
   }
   openCliamVsBooking(){
    this.route.navigate(['/client/client-vs-booking']) 

   }
durationList= [
{
"Date" : "13/01/2020",
"walletOpeningBalance": 999922,
"ticketBooked": 456,
"Claim": 44434,
"Profit": 333,
"cashAmount": 999,
"walletClosingBalance": 322323, 
},
{
  "Date" : "13/01/2020",
  "walletOpeningBalance": 999922,
  "ticketBooked": 456,
  "Claim": 44434,
  "Profit": 333,
  "cashAmount": 999,
  "walletClosingBalance": 322323, 
  },
  {
    "Date" : "13/01/2020",
    "walletOpeningBalance": 999922,
    "ticketBooked": 456,
    "Claim": 44434,
    "Profit": 333,
    "cashAmount": 999,
    "walletClosingBalance": 322323, 
    },
    {
      "Date" : "14/01/2020",
      "walletOpeningBalance": 999922,
      "ticketBooked": 456,
      "Claim": 44434,
      "Profit": 333,
      "cashAmount": 999,
      "walletClosingBalance": 322323, 
      },
      {
        "Date" : "15/01/2020",
        "walletOpeningBalance": 999922,
        "ticketBooked": 456,
        "Claim": 44434,
        "Profit": 333,
        "cashAmount": 999,
        "walletClosingBalance": 322323, 
        },
        {
          "Date" : "16/01/2020",
          "walletOpeningBalance": 999922,
          "ticketBooked": 456,
          "Claim": 44434,
          "Profit": 333,
          "cashAmount": 999,
          "walletClosingBalance": 322323, 
          },
          {
            "Date" : "17/01/2020",
            "walletOpeningBalance": 999922,
            "ticketBooked": 456,
            "Claim": 44434,
            "Profit": 333,
            "cashAmount": 999,
            "walletClosingBalance": 322323, 
            },
            {
              "Date" : "17/01/2020",
              "walletOpeningBalance": 999922,
              "ticketBooked": 456,
              "Claim": 44434,
              "Profit": 333,
              "cashAmount": 999,
              "walletClosingBalance": 322323, 
              }
]



}
