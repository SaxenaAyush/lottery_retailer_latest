import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { Router, ActivatedRoute } from '@angular/router';
import { LotteryStorageService } from '../../services/lottery-storage.service';
import { SweetAlertService } from '../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../services/lottery-http.service';
import * as $ from '../../../../node_modules/jquery';

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss']
})
export class CertificateComponent implements OnInit {
  public clientDetailsForm: FormGroup
  public bankDetailsForm: FormGroup
  public kycDetailsForm: FormGroup
  public uploader: FileUploader = new FileUploader({});
  imageUrl: string | ArrayBuffer;
  imagePK: any;
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  isEdit = false;
  loggedInUserDetails = this.storageService.get('currentUser');
  userid = this.loggedInUserDetails.user.id;
  constructor(private storageService: LotteryStorageService, private alertService: SweetAlertService,
    private router: Router, public route: ActivatedRoute, private fb: FormBuilder,
    private lotteryService: LotteryHttpService) {
  }
  navModel: any = {
    "Credit": 1,
    "Debit": 1,
    "ShowTransaction": 2,
    "BonusClaim": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.Credit = 2;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.BonusClaim = 1;

      
    }
    else if (input == 2) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 2;
      this.navModel.ShowTransaction = 1;
      this.navModel.BonusClaim = 1;

     
    }
     else if (input == 5) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 2;
      this.navModel.BonusClaim = 1;

     
    }else if (input == 4) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.BonusClaim = 2;
      
    }
  }

  ngOnInit() {


  }certificateOne(){
    $('.certificateOneForm').show();
    $('.certificateTwoForm').hide();
    $('.certificateThreeForm').hide();
    $('.certificateFourForm').hide();

    console.log("one");
  }
  certificateTwo(){
    $('.certificateOneForm').hide();
    $('.certificateTwoForm').show();
    $('.certificateThreeForm').hide();
    $('.certificateFourForm').hide();

    console.log("Two");

  }
  certificateThree(){
    $('.certificateOneForm').hide();
    $('.certificateTwoForm').hide();
    $('.certificateThreeForm').show();
    $('.certificateFourForm').hide();

    console.log("Three");

  }
  certificateFour(){
    $('.certificateOneForm').hide();
    $('.certificateTwoForm').hide();
    $('.certificateThreeForm').hide();
    $('.certificateFourForm').show();

    console.log("Three");

  }

}
