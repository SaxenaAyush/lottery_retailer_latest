import {Component, OnInit} from '@angular/core';
import {AppComponent} from '../../app.component';
import {FormGroup} from "@angular/forms";
import {LotteryHttpService} from "../../services/lottery-http.service";
import {LotteryStorageService} from "../../services/lottery-storage.service";
import {version} from '../../../../package.json';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit{

  loggedInUserDetails: any;
  userD = this.storageService.get('currentUser');
 id= this.userD.user.roleMaster.id;
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
appVersion:any =' '+ version;
  constructor(private storageService: LotteryStorageService, private curryLeavesHttpService: LotteryHttpService) {
    console.log('userD', this.userD);
    console.log('userD role id', this.id);
  }

  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('currentUser');
// console.log(this.loggedInUserDetails);
  }


}

