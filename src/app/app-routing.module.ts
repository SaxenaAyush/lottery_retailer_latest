import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { BookingComponent } from './components/client/booking/booking.component';
import { WalletDetailsComponent } from './components/client/wallet-details/wallet-details.component';
import { ClaimTransactionComponent } from './components/client/claim-transaction/claim-transaction.component';
import { AddDetailsComponent } from './components/client/add-details/add-details.component';
import { AuthGuard } from './components/guard/auth.guard';
import { ResultViewComponent } from './components/client/result-view/result-view.component';
import { UserProfileComponent } from './components/client/user-profile/user-profile.component';
import { SettingComponent } from './components/client/setting/setting.component';
import { ResultReportComponent } from './components/client/result-report/result-report.component';
import { CollapsingComponent } from './components/collapsing/collapsing.component';
import { AboutUsComponent } from './components/aboutUs/aboutUs.component';
import { CertificateComponent } from './components/certificate/certificate.component';
import { TicketpreviewComponent } from './ticketpreview/ticketpreview.component';
import { ReprintticketComponent } from './reprintticket/reprintticket.component';
import { PrintresultComponent } from './printresult/printresult.component';
import { TicketHistoryComponent } from './components/client/ticket-history/ticket-history.component';
import { DailyReportComponent } from './components/client/daily-report/daily-report.component';
import { ReportPrintComponent } from './report-print/report-print.component';
import { CalimReprintticketComponent } from './claim-reprint/claim-reprintticket.component';
import { ClientWalletHistoryComponent } from './components/client/client-wallet-history/client-wallet-history.component';
import { ClientWalletDetailsHistoryComponent } from './components/client/client-wallet-details-history/client-wallet-details-history.component';
import { ClientVsBookingComponent } from './components/client/client-vs-booking/client-vs-booking.component';
import { ClaimVsBookingDetailsComponent } from './components/client/claim-vs-booking-details/claim-vs-booking-details.component';
import { PrintClaimVsBookingComponent } from './print-claim-vs-booking/print-claim-vs-booking.component';
import { FirstClaimBookingPrintComponent } from './first-claim-booking-print/first-claim-booking-print.component';
import { DurationReportPrintComponent } from './duration-report-print/duration-report-print.component';
const routes: Routes = [

  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },


    ]
  },
  {
    path: 'client',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'booking',
        component: BookingComponent
      },
      {
        path: 'wallet-Details',
        component: WalletDetailsComponent
      },
      {
        path: 'claim-transaction',
        component: ClaimTransactionComponent
      },
      {
        path: 'result-view',
        component: ResultViewComponent
      },
      {
        path: 'add-details',
        component: AddDetailsComponent
      },
      {
        path: 'user-profile',
        component: UserProfileComponent
      },
      {
        path: 'setting',
        component: SettingComponent
      },
      {
        path: 'result-report',
        component: ResultReportComponent
      },
     
      {
        path: 'ticket-history',
        component: TicketHistoryComponent
      },
       
      {
        path: 'daily-report',
        component: DailyReportComponent
      },
       
      {
        path: 'client-wallet-history',
        component: ClientWalletHistoryComponent
      },
       
      {
        path: 'client-wallet-history-details',
        component: ClientWalletDetailsHistoryComponent
      },
      {
        path: 'client-vs-booking',
        component: ClientVsBookingComponent
      },
      {
        path: 'claim-vs-booking-details',
        component: ClaimVsBookingDetailsComponent
      },
    ]
  },
{
path: 'collapsing',
component: CollapsingComponent
},
{
  path: 'aboutUs',
  component: AboutUsComponent
  },
  {
    path: 'certificate',
    component: CertificateComponent
    },
    {
      path: 'ticket-preview',
      component: TicketpreviewComponent,
      },
      {
        path: 'reprint-ticket',
        component: ReprintticketComponent,
        },
        {
          path: 'report-print',
          component: ReportPrintComponent
        },
        {
          path: 'result-print',
          component: PrintresultComponent
        },
        {
          path: 'claim-reprint',
          component: CalimReprintticketComponent
        },
        {
          path: 'claim-vs-booking-print',
          component: PrintClaimVsBookingComponent
        },
        {
          path: 'first-claim-vs-booking-print',
          component: FirstClaimBookingPrintComponent
        },
        {
          path: 'duration-report-print',
          component: DurationReportPrintComponent
        },
  { path: '**', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}