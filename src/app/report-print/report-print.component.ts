import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LotteryStorageService } from '../services/lottery-storage.service';

@Component({
  selector: 'app-report-print',
  templateUrl: './report-print.component.html',
  styleUrls: ['./report-print.component.scss']
})
export class ReportPrintComponent implements OnInit {
  ReportDetails: any;
  billModel1: any = [];
  userD = this.storageService.get('currentUser');
  firstName = this.userD.user.firstName;
  lastName = this.userD.user.lastName;
  retailerID = this.userD.user.userCode;
  claimAmount:any;
  currentBalance:any;
  name:any;
  payToCompany:any;
  profit:any;
  ticketBooked:any;
  totalWalletRecharge:any;
  cashInDrawer:any;
  reportDate:any;
  dailyRecharge:any = [];
  Claim: any;
  ClaimBonus: any;
  Commission: any;
  TicketBooked: any;
  FromDate:any;
  ToDate:any;
  Profit: any;
  closingBalance:any;
  openingBalance:any;
  WalletBalance:any;
  constructor(private route: Router, private storageService: LotteryStorageService) { }

  ngOnInit() {
    this.ReportDetails = this.storageService.get('reportDetails');
    console.log('bill model', this.ReportDetails);
    this.claimAmount = this.ReportDetails.claimAmount,
      this.currentBalance = this.ReportDetails.currentBalance,
      this.payToCompany = this.ReportDetails.payToCompany,
      this.ticketBooked = this.ReportDetails.ticketBooked 
      this.cashInDrawer = this.ReportDetails.cashAmount
      this.profit = this.ReportDetails.profit,
      this.totalWalletRecharge = this.ReportDetails.totalWalletRecharge,
      this.reportDate = this.ReportDetails.reportDate,
      this.name = this.ReportDetails.name,
      this.dailyRecharge = this.ReportDetails.dailyRecharge,
      this.closingBalance = this.ReportDetails.closingBalance,
      this.openingBalance = this.ReportDetails.openingBalance
      setTimeout(() => {
        console.log('Test');
        window.print();
        this.back();
    }, 1000/60);  
  }
  back() {
    this.route.navigate(['/client/daily-report']);

  }
}
