import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalimReprintticketComponent } from './claim-reprintticket.component';

describe('ReprintticketComponent', () => {
  let component: CalimReprintticketComponent;
  let fixture: ComponentFixture<CalimReprintticketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalimReprintticketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalimReprintticketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
